
<div class="col-xs-12" id="map" style="display: none"></div>
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <h4>Historial</h4>
                    </div>
                </div>

            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">

                            <table width="100%" class="table table-striped  table-hover data_table_historial_todos">
                                <thead>
                                    <th>Id</th><th>Promotor</th><th>Fecha</th><th>Ubicación</th><th>Foto</th><th>Estatus</th><th>Cadena</th><th>Sucursal</th><th>Cliente</th><th>Marca</th><?=($metodo=="Vendedor" || $metodo=="Promotor")?"
                                    <th>Producto</th><th>Lugar</th><th>Posición</th><th>Cantidad</th>":""?><th>Comentario</th>
                                </thead>
                                <tbody class="cuerpo_tabla_historial">

                                </tbody>
                            </table>


                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


<?php  $this->load->view('pie'); ?>

<script type="text/javascript" src="<?=base_url()?>assets/js/historial.js?v=2"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/guardo_direcciones.js?v=3"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRKyryCiEefdr8ewLMKhnBrAZsENlp0iw&callback=initMap"
        async defer></script>

</body>
</html>
