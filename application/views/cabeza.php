<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <!-- META SECTION -->
    <title><?=$titulo?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <link rel="icon" href="<?=base_url()?>assets/img/vd.png"  />
    <!-- END META SECTION -->

    <!-- CSS INCLUDE -->
    <link rel="stylesheet" type="text/css" id="theme" href="<?=base_url()?>plantilla/css/theme-default.css"/>
    <!-- EOF CSS INCLUDE -->

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">


    <script type="text/javascript" src="<?=base_url()?>assets/js/base_url.js"></script>
    <script type="text/javascript" src="<?=base_url()?>assets/js/libreria_js_mau.js"></script>
    <link rel="stylesheet" href="<?=base_url(); ?>assets/css/mapvd.css?v=2">
    <link rel="stylesheet" href="<?=base_url(); ?>assets/css/configuracion.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

</head>
<body>
<!-- START PAGE CONTAINER -->
<div class="page-container">

    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="index.html"><?=$privilegio?></a>
                <a href="#" class="x-navigation-control"></a>
            </li>
            <li class="xn-profile">
                <a href="#" class="profile-mini">
                    <img src="<?=$foto_perfil?>" alt="sin imagen"/>
                </a>
                <div class="profile">
                    <div class="profile-image">
                        <img src="<?=$foto_perfil?>" alt="sin imagen"/>
                    </div>
                    <div class="profile-data">
                            <div class="profile-data-name"><?=$usuario?></div>
                        <input id="id_usuario" type="hidden" value="<?=$this->session->userdata('id')?>">
                    </div>
                    <!--
                    <div class="profile-controls">
                        <a href="#" class="profile-control-left"><span class="fa fa-info"></span></a>
                    </div>
                    -->
                </div>
            </li>


            <?php

           $class_ac_usuarios='';
           $class_ac_historial='';
           $class_ac_estatus='';
           $class_ac_mapa='';
           $class_ac_cliente='';
           $class_ac_sucursal='';
           $class_ac_cliente_promotor='';
           $class_ac_cadena='';
           $class_ac_producto='';
           $class_ac_lugar='';
           $class_ac_posicion='';
           $class_ac_promotor_vendedor_h='';
           $class_ac_promotor_h='';
            $class_ac_promotor_vendedor_m='';
            $class_ac_promotor_m='';
            $class_ac_vendedor_h='';
            $class_ac_vendedor_m='';

            $class_ac_config='active';
            $class_ac_historial='';
            $class_ac_mapa='';

                $class_active='active';
                switch ($titulo){
                    case 'Usuarios':
                        $class_ac_usuarios='active';
                        break;
                    case 'Cadena':
                        $class_ac_cadena='active';
                        break;
                    case 'Sucursal':
                        $class_ac_sucursal='active';
                        break;
                    case 'Cliente':
                        $class_ac_cliente='active';
                        break;
                    case 'ClientePromotor':
                        $class_ac_cliente_promotor='active';
                        break;
                    case 'Estatus':
                        $class_ac_estatus='active';
                        break;
                    case 'Producto':
                        $class_ac_producto='active';
                        break;
                    case 'Lugar':
                        $class_ac_lugar='active';
                        break;

                    case 'Posición':
                        $class_ac_posicion='active';
                        break;

                    case 'Promotor':


                        if($m_titulo=="Historial") {
                            $class_ac_historial = 'active';
                            $class_ac_promotor_h='active';
                        }else {
                            $class_ac_mapa = 'active';
                            $class_ac_promotor_m='active';
                        }


                        $class_ac_config='';

                        break;
                    case 'Promotor-Vendedor':


                        if($m_titulo=="Historial") {
                            $class_ac_historial = 'active';
                            $class_ac_promotor_vendedor_h='active';
                        }else {
                            $class_ac_mapa = 'active';
                            $class_ac_promotor_vendedor_m='active';
                        }
                        $class_ac_config='';

                        break;
                    case 'Vendedor':


                        if($m_titulo=="Historial") {
                            $class_ac_historial = 'active';
                            $class_ac_vendedor_h='active';
                        }else {
                            $class_ac_mapa = 'active';
                            $class_ac_vendedor_m='active';
                        }
                        $class_ac_config='';

                        break;
                }


                if($id_privilegio==1){ // admin


                  echo '  <li class="xn-openable '.$class_ac_config.'">
                        <a href="#"><span class="fa fa-cogs"></span> <span class="xn-text">Configuración</span></a>
                        <ul>
                            <li class="'.$class_ac_usuarios.'">
                               <a href="'.base_url().'configuracion/usuarios" ><span class="fa fa-users"> </span><span class="xn-text"> Usuarios</span></a>
                               </li>
                             <li class="'.$class_ac_cadena.'">
                                <a href="'.base_url().'configuracion/cadena" ><span class="fa fa-list"></span><span class="xn-text"> Cadena</span> </a>
                            </li>
                             <li class="'.$class_ac_sucursal.'">
                                <a href="'.base_url().'configuracion/sucursal" ><span class="fa fa-list"></span><span class="xn-text"> Sucursal</span> </a>
                            </li>
                             <li class="'.$class_ac_cliente.'">
                                <a href="'.base_url().'configuracion/cliente" ><span class="fa fa-list"></span><span class="xn-text"> Cliente</span> </a>
                            </li>
                             <li class="'.$class_ac_cliente_promotor.'">
                                <a href="'.base_url().'configuracion/cliente-promotor" ><span class="fa fa-list"></span><span class="xn-text"> Cliente-Promotor</span> </a>
                            </li>
                             <li class="'.$class_ac_estatus.'">
                                <a href="'.base_url().'configuracion/estatus" ><span class="fa fa-list"></span><span class="xn-text"> Estatus</span> </a>
                            </li>
                              <li class="'.$class_ac_producto.'">
                                <a href="'.base_url().'configuracion/producto" ><span class="fa fa-list"></span><span class="xn-text"> Producto</span> </a>
                            </li>
                             </li>
                              <li class="'.$class_ac_lugar.'">
                                <a href="'.base_url().'configuracion/lugar" ><span class="fa fa-list"></span><span class="xn-text"> Lugar</span> </a>
                            </li>
                              <li class="'.$class_ac_posicion.'">
                                <a href="'.base_url().'configuracion/posicion" ><span class="fa fa-list"></span><span class="xn-text"> Posición</span> </a>
                            </li>
                        </ul>
                    </li>';
                }

            echo '  <li class="xn-openable '.$class_ac_historial.'">
                        <a href="#"><span class="fa fa-database"></span> <span class="xn-text">Historial</span></a>
                        <ul>
                            <li class="'.$class_ac_promotor_h.'">
                               <a href="'.base_url().'historial/promotor" ><span class="fa fa-list"> </span><span class="xn-text"> Promotor</span></a>
                             </li>
                             <li class="'.$class_ac_promotor_vendedor_h.'">
                                <a href="'.base_url().'historial/promotor-vendedor" ><span class="fa fa-list"></span><span class="xn-text"> Promotor-Vendedor</span> </a>
                            </li>
                             <li class="'.$class_ac_vendedor_h.'">
                                <a href="'.base_url().'historial/vendedor" ><span class="fa fa-list"></span><span class="xn-text"> Vendedor</span> </a>
                            </li>
                        </ul>
                    </li>';

            echo '  <li class="xn-openable '.$class_ac_mapa.'"> 
                        <a href="#"><span class="fa fa-database"></span> <span class="xn-text">Mapa</span></a>
                        <ul>
                            <li class="'.$class_ac_promotor_m.'">
                               <a href="'.base_url().'mapa/promotor" ><span class="fa fa-list"> </span><span class="xn-text"> Promotor</span></a>
                             </li>
                             <li class="'.$class_ac_promotor_vendedor_m.'">
                                <a href="'.base_url().'mapa/promotor-vendedor" ><span class="fa fa-list"></span><span class="xn-text"> Promotor-Vendedor</span> </a>
                            </li>
                              <li class="'.$class_ac_vendedor_m.'">
                                <a href="'.base_url().'mapa/vendedor" ><span class="fa fa-list"></span><span class="xn-text"> Vendedor</span> </a>
                            </li>
                           
                        </ul>
                    </li>';

            ?>


        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">

        <!-- START X-NAVIGATION VERTICAL -->
        <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
            <!-- TOGGLE NAVIGATION -->
            <li class="xn-icon-button">
                <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
            </li>
            <!-- END TOGGLE NAVIGATION -->
            <!-- SEARCH
            <li class="xn-search">
                <form role="form">
                    <input type="text" name="search" placeholder="Search..."/>
                </form>
            </li>
            <!-- END SEARCH -->
            <!-- SIGN OUT -->
            <li class="xn-icon-button pull-right">
                <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span></a>
            </li>
            <!-- END SIGN OUT -->
            <!-- MESSAGES -->
            <!-- END TASKS -->
        </ul>
        <!-- END X-NAVIGATION VERTICAL -->

        <!-- START BREADCRUMB -->
        <ul class="breadcrumb">
            <li><a href="#"><?=$m_titulo?></a></li>
            <li class="active"><?=$titulo?></li>
        </ul>
        <!-- END BREADCRUMB -->

        <!-- PAGE CONTENT WRAPPER -->
        <div class="page-content-wrap"  style="background-color: background-color: rgb(249, 249, 249);">