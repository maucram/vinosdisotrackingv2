<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html style="height:100%">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Acceso</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="icon" href="<?=base_url()?>assets/img/vd.png" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mapvd.css">
</head>
<body >
<div id="fondo_login"></div>
<section class="login">
    <div class="container-fluid">
        <div class="row justify-content-center" >
            <div class="col-xs-12 col-md-8 col-lg-6" id="imagen_logo_vd">

                <img width="50%" src="<?=base_url(); ?>assets/img/vdprovensa.png">
            </div>
        </div>
        <div class="row justify-content-center" >
            <div class="col-xs-12 col-md-6 col-lg-4">
                <div class="row justify-content-center" id="form-ingreso">
                    <form action="<?=base_url(); ?>login/acceso" method="post" accept-charset="utf-8" id="cuerpo_login">
                        <div class="form-group">
                            <div class="input-group agrupo_inp_ico">
                                <i class="material-icons icon_ticket iconos_login" >account_circle</i>
                                <input type="text" class="form-control" placeholder="Usuario" required name="usuario">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group agrupo_inp_ico">
                                <i class="material-icons icon_ticket iconos_login">lock</i>
                                <input type="password" class="form-control" placeholder="Contraseña" required name="pass">
                            </div>
                        </div>
                        <?php if(isset($error)):?>
                            <div class="alert alert-danger" role="alert">
                                Corrreo o contraseña incorrectos
                            </div>
                        <?php endif ?>
                        <?php if(isset($error_pri)):?>
                            <div class="alert alert-warning" role="alert">
                                Perfil de usuario deshabilitado
                            </div>
                        <?php endif ?>
                        <button type="submit"  name="acceso" class="boton_mapvd">Acceder</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</section>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>