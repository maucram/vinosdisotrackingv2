
<!--
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">


            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label for="foto">Foto</label>
                        <input class="form-control" type="file">
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label for="foto">Estatus</label>
                        <?=$select_estatus
                        ?>

                    </div>
                    <div class="col-xs-12 col-md-4" style="margin-top: 10px" align="center">
                       <button class="btn boton_mapvd_modals">Reportar</button>
                    </div>
                </div><br>

            </div>

        </div>

    </div>

</div>
-->



<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label for="foto"><?=$titulo?></label>
                        <select class="form-control" id="mapa_usuario"></select>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <label for="foto">Fecha</label>
                        <input type="date" value="<?=date("Y-m-d")?>" class="form-control" id="mapa_fecha" required>

                    </div>
                    <div class="col-xs-12 col-md-4" style="margin-top: 10px" align="center">
                        <button class="btn boton_mapvd_modals" id="buscar_ruta">Buscar</button>

                    </div>
                </div><br>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


<div class="row" id="foco_mapa">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-12">
                        <h4><?=$titulo?></h4>
                    </div>
                    <div class="col-xs-12" id="map_status">

                    </div>
                </div>

            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-12 col-md-6" id="map">

                    </div>
                    <div class="col-sm-12 col-md-6 text-center" id="grafica"></div>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <h4>Historial</h4>
                    </div>
                </div>

            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12">

                            <table width="100%" class="table table-striped  table-hover data_table_historial">
                                <thead>
                                <th>Id</th><th><?=$titulo?></th><th>Fecha</th><th>Ubicación</th><th>Foto</th><th>Estatus</th><th>Cadena</th><th>Sucursal</th><th>Cliente</th><th>Marca</th><?=($metodo=="Vendedor" || $metodo=="Promotor")?"
                                    <th>Producto</th><th>Lugar</th><th>Posición</th><th>Cantidad</th>":""?><th>Comentario</th>
                                </thead>
                                <tbody class="cuerpo_tabla_historial">

                                </tbody>
                            </table>

                    </div>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel --> 
    </div>
    <!-- /.col-lg-12 -->
</div>
<?php  $this->load->view('pie'); ?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/agrego_edito.js?v=2"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/historial.js?v=2"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/mapaAPI.js?v=2"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/guardo_direcciones.js?v=4"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRKyryCiEefdr8ewLMKhnBrAZsENlp0iw&callback=initMap"
        async defer></script>
</body>
</html>
