<div class="row">
    <div class="col-xs-12">
            <h5 class="text-right"><a href="#"><i title="Agregar" data-toggle="modal" data-target="#Modal_agregar" class="fa fa-plus-circle fa-2x"></i></a></h5>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-6">
                        <h4><?=$titulo?> </h4>
                    </div>
                    <?php
                    if($titulo=='Tickets'){
                        echo ' <div class="col-xs-6">
                                <h4 class="text-right"><i title="Descargar" class="fa fa-file-excel-o descargar_excel_tickets"> Descargar</i></h4>
                            </div>';
                    }
                    ?>
                    <!-- /.col-lg-12 -->
                </div>

            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                   <table width="100%" class="table table-striped  table-hover data_table_configuracion">
                        <thead>
                            <?=$cabeza_tabla?>
                        </thead>
                        <tbody class="cuerpo_tabla_configuracion">

                        </tbody>
                    </table>

                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>


<!-- The Modal -->
<div class="modal fade" id="Modal_agregar">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">AGREGAR <?=mb_strtoupper ($titulo_con_espacios)?></h4>
            </div>

          <form method="post" data-tabla="<?=$titulo?>"  enctype="multipart/form-data" id="form-agregar" class="form_agregar" accept-charset="utf-8">

            <!-- Modal body -->
            <div class="modal-body">
                <?=$modal_body_agregar?>
            </div>
            <div class="modal-close" style="padding: 30px;margin-top: 20px" align="center">
                <div class="row">
                    <div class="form-group col-xs-12">
                        <button type="submit" class="btn boton_mapvd_modals"  id="boton_guardar">Guardar</button>
                        <button type="button" class="btn boton_mapvd_modals" data-dismiss="modal">Cancelar</button><br>
                    </div>
                </div>

            </div>
            <!-- Modal footer -->
            <div id="status-general" align="center"></div>
          </form>

        </div>
    </div>
</div>

<!-- EDITAR IVA ---------------->

<!-- The Modal -->
<div class="modal fade" id="Modal_editar">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">EDITAR <?=mb_strtoupper ($titulo_con_espacios)?></h4>
            </div>

            <form onsubmit="return false" method="post" data-tabla_e="<?=$titulo?>"  enctype="multipart/form-data" class="form_editar" id="form-editar" accept-charset="utf-8">

            <!-- Modal body -->
            <div class="modal-body editar_body">
                <?=$modal_body_editar?>
                <!-- imprimo archivo modales_body_edito.js -->
            </div>

           <div class="modal-close" style="padding: 30px;margin-top: 20px" align="center">
               <div class="row">
                   <div class="form-group col-xs-12">
                <button type="submit" class="btn boton_mapvd_modals"  id="boton_guardar_e">Editar</button>
                <button type="button" class="btn boton_mapvd_modals" data-dismiss="modal">Cancelar</button>
                   </div>
                       <div class="form-group col-xs-12" align="right">
                            <button type="button" class="btn btn-danger"  id="boton_eliminar">Eliminar</button>
                       </div>
               </div>
            </div>
            <!-- Modal footer -->
            <div id="status-general_e" align="center"></div>
            </form>
            <!-- FIN EDITAR IVA ---------------->
        </div>
    </div>
</div>
<?php
$this->load->view('pie'); ?>
<script type="text/javascript" src="<?=base_url()?>assets/js/agrego_edito.js?v=2"></script>

</body>
</html>
