<footer class="footer">
    <img width="200vw"  src="<?=base_url()?>assets/img/vdprovensa.png"/>

</footer>




</div>
<!-- END PAGE CONTENT WRAPPER -->
</div>
</div>
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
    <div class="mb-container">
        <div class="mb-middle">
            <div class="mb-title"><span class="fa fa-sign-out"></span> Cerrar sesión</div>
            <div class="mb-content">
                <p>¿Estás seguro que deseas cerrar sesión?</p>
            </div>
            <div class="mb-footer">
                <div class="pull-right">
                    <a href="<?=base_url()?>mapa/cerrar_sesion" class="btn btn-info btn-lg">Si</a>
                    <button class="btn btn-default btn-lg mb-control-close">No</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MESSAGE BOX-->

<!-- START PRELOADS
<audio id="audio-alert" src="<?=base_url()?>plantilla/audio/alert.mp3" preload="auto"></audio>
<audio id="audio-fail" src="<?=base_url()?>plantilla/audio/fail.mp3" preload="auto"></audio>
<!-- END PRELOADS -->

<!-- START SCRIPTS -->
<!-- START PLUGINS -->
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/jquery/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/bootstrap/bootstrap.min.js"></script>
<!-- END PLUGINS -->

<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.0/dist/jquery.validate.min.js"></script>
<script>
    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número entero válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        accept: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
        range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
        max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
        min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
    });
</script>
<!-- START THIS PAGE PLUGINS-->
<script type='text/javascript' src='<?=base_url()?>plantilla/js/plugins/icheck/icheck.min.js'></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/scrolltotop/scrolltopcontrol.js"></script>

<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/morris/raphael-min.js"></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/rickshaw/d3.v3.js"></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/rickshaw/rickshaw.min.js"></script>
<script type='text/javascript' src='<?=base_url()?>plantilla/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
<script type='text/javascript' src='<?=base_url()?>plantilla/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>
<script type='text/javascript' src='<?=base_url()?>plantilla/js/plugins/bootstrap/bootstrap-datepicker.js'></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/owl/owl.carousel.min.js"></script>

<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/moment.min.js"></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins/daterangepicker/daterangepicker.js"></script>
<!-- END THIS PAGE PLUGINS-->



<!-- START TEMPLATE -->
<script type="text/javascript" src="<?=base_url()?>plantilla/js/settings.js"></script>

<script type="text/javascript" src="<?=base_url()?>plantilla/js/plugins.js"></script>
<script type="text/javascript" src="<?=base_url()?>plantilla/js/actions.js"></script>


<!-- END TEMPLATE -->
<!-- END SCRIPTS -->
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<!-- botones exportar data table -->

<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
<script type="text/javascript" language="javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>

<!-- fin botones -->
<script type="text/javascript" src="<?=base_url()?>assets/js/block_config.js"></script>


<?php
    echo '<script>
                    var g_titulo="'.$titulo.'";
               
            </script>';
?>




<script type="text/javascript" src="<?=base_url()?>assets/js/datatables_ini.js"></script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

