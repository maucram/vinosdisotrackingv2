<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class LoginModel extends CI_Model {

    public function acceso($usuario)
    {

        $posibleUsuario = $this->db->where('usuario', $usuario['usuario'])->where('(privilegios=1 OR privilegios=2)')->get('usuarios')->row();

        if($posibleUsuario) {

            if ($usuario['pass']==$posibleUsuario->pass) {
                if ($posibleUsuario->status) {
                    return $posibleUsuario->id;
                }else
                    return false;
            }

        }else
            return false;
    }

        public function obtengo_datos_usuario($id)
        {
            return $this->db->where('id', $id)->where("status","1")->get("usuarios")->row();
        }

}
?>