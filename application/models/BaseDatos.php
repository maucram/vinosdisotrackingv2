<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class BaseDatos extends CI_Model { 

    public function acceso($usuario)
    {

        $posibleUsuario = $this->db->where('usuario', $usuario['usuario'])->get('usuarios')->row();

        if($posibleUsuario) {

            if ($usuario['pass']==$posibleUsuario->pass) {
                if ($posibleUsuario->status) {
                    return $posibleUsuario->id;
                }else
                    return false;
            }

        }else
            return false;
    }


    public function obtengo_max_id($tabla){
        $resultado=$this->db->select("id")->order_by("id","desc")->get($tabla,1)->row();

        if($resultado)
            return $resultado->id;
        else
            return false;
    }

    /*############################################################## configuracion ##############################################################*/

    public function obtengo_usuario($activo=false)
    {
        if($activo) {
            $this->db->where('status', '1');
        }

        return $this->db->order_by("usuario")->get("usuarios")->result();
    }


    public function agregar($tabla,$datos)
    {
        switch ($tabla) {
            case 'usuarios':

                $base_to_php = explode(',', $datos["foto"]);


                if (isset($base_to_php[1])) {

                    $data = base64_decode($base_to_php[1]);

                    $div1 = explode("/", $base_to_php[0]);

                    $div2 = explode(";", $div1[1]);
                    $extension = $div2[0];
                    $nombre_foto = date("Ymdhis") . "." . $extension;


                    $datos["foto_perfil"]=$nombre_foto;
                    unset($datos["foto"]);

                    $query = $this->db->insert("usuarios",$datos);

                    if ($query) {

                        $id_usuario=$this->obtengo_max_id("usuarios");

                        $carpeta = "./fotos_perfil/" . $id_usuario . '/';

                        if (!file_exists($carpeta)) {
                            mkdir($carpeta, 0777, true);
                        }



                        $filepath = $carpeta . "/" . $nombre_foto;


                        if (file_put_contents($filepath, $data)) {

                            $respuesta=$this->ultima_actualizacion("usuarios");
                            if($respuesta)
                                return array('status' => 200,"id"=>$id_usuario,"foto"=>$datos["foto_perfil"]);
                            else
                                return array("status" => 400, "mensaje" => "No se pudo guardar fecha actualización");

                        } else {
                            return array("status" => 400, "mensaje" => "No se pudo guardar la imagen");
                        }
                    } else {
                        return array('status' => 400, "mensaje" => "Error al insertar datos");
                    }

                } else
                    return json_encode(array("status" => 400, "Error en formato de imagen o al obtener id usuario"));

                break;
            default:
                $resultado=$this->db->insert($tabla,$datos);
                if($resultado) {
                    $id=$this->obtengo_max_id($tabla);
                    $respuesta=$this->ultima_actualizacion($tabla);
                    if($respuesta)
                    return array('status' => 200, "id" => $id);
                    else
                        return array("status" => 400, "mensaje" => "No se pudo guardar fecha actualización");
                }else
                    return array("status" => 400, "mensaje" => "Error al intentar guardar");


        }
    }

    public function ultima_actualizacion($tabla){
        $query='UPDATE ultima_actualizacion SET tabla="'.$tabla.'", hash="'.substr(md5(time()), 0, 10).'" WHERE id=1';
        $this->db->query($query);
        if($this->db->affected_rows())
            return true;
        else
            return false;
    }

    public function editar($tabla,$datos)
    {
        switch ($tabla) {
            case 'usuarios':

                if (!empty($datos["foto"])) {

                    $elimino_img = $this->elimino_imagen("./fotos_perfil/".$datos["id"]."/","usuarios", $datos["id"],"foto_perfil");

                    $base_to_php = explode(',', $datos["foto"]);
                    if (isset($base_to_php[1]) && $elimino_img["status"] == 200) {
                        $data = base64_decode($base_to_php[1]);

                        $div1 = explode("/", $base_to_php[0]);

                        $div2 = explode(";", $div1[1]);
                        $extension = $div2[0];

                        $carpeta = "./fotos_perfil/".$datos["id"].'/';

                        if (!file_exists($carpeta)) {
                            mkdir($carpeta, 0777, true);
                        }
                        $nombre_foto = date("Ymdhis") . "." . $extension;

                        $filepath = $carpeta . "/" . $nombre_foto;

                        $datos["foto_perfil"] = $nombre_foto;
                        unset($datos["foto"]);

                        if (file_put_contents($filepath, $data)) {
                            $this->db->where("id", $datos["id"]);
                            $query = $this->db->set($datos)->update("usuarios");

                            if ($query) {
                                $respuesta=$this->ultima_actualizacion($tabla);
                                if($respuesta)
                                     return array('status' => 200);
                                else
                                    return array("status" => 400, "mensaje" => "No se pudo guardar fecha actualización");

                            } else {
                                return array('status' => 400, "mensaje" => "Error al editar datos");
                            }
                        } else {
                            return array("status" => 400, "mensaje" => "No se pudo editar la imagen");
                        }

                    } else {
                        return array("status" => 400, "mensaje" => "Error al intentar editar imagen");
                    }
                } else {
                    unset($datos["foto"]);
                    $this->db->where("id", $datos["id"]);
                    $query = $this->db->set($datos)->update("usuarios");

                    if ($query) {
                        $respuesta=$this->ultima_actualizacion($tabla);
                        if($respuesta)
                            return array('status' => 200);
                        else
                            return array("status" => 400, "mensaje" => "No se pudo guardar fecha actualización");
                    } else {
                        return array('status' => 400,"mensaje" => "Error al intentar editar datos");
                    }


                }

                break;

            default:
                $this->db->where("id", $datos["id"]);
                $resultado=$this->db->set($datos)->update($tabla);
                if($resultado) {
                    $respuesta=$this->ultima_actualizacion($tabla);
                    if($respuesta)
                        return array("status" => 200);
                    else
                        return array("status" => 400, "mensaje" => "No se pudo guardar fecha actualización");
                }else
                    return array("status" => 400, "mensaje" => "Error al intentar editar");

        }
    }

    public function eliminar($datos,$ruta=null,$nombre_campo=null)
    {


        $resultado =(is_null($ruta) && is_null($nombre_campo))?array("status"=>200):$this->elimino_imagen($ruta, $datos["tabla"], $datos["id"], $nombre_campo,true);


        if ($resultado["status"] == 200) {
            $this->db->where("id", $datos["id"]);
            $respuesta = $this->db->delete($datos["tabla"]);
            if ($respuesta) {
                $respuesta=$this->ultima_actualizacion($datos["tabla"]);
                if($respuesta)
                    return json_encode(array("status" => 200));
                else
                    return array("status" => 400, "mensaje" => "No se pudo guardar fecha actualización");
            } else {
                return json_encode(array("status" => 400, "mensaje" => "Error al tratar de eliminar el registro"));
            }
        }else{
            return json_encode($resultado);
        }



    }


    private function elimino_imagen($ruta,$tabla,$id,$nombre_campo,$elimino_directorio=false){
        $query = $this->db->where("id",$id)->select($nombre_campo)->get($tabla);

        if($query->num_rows() > 0) {
            $resultado = $query->row_array();
            $imagen = $resultado[$nombre_campo];

            $url_imagen = $ruta . $imagen;

           // return  array('status' => 400,"mensaje"=>$url_imagen);
         //   exit();
            if (is_file($url_imagen)) {
                if (unlink($url_imagen)) {
                    if ($elimino_directorio) {
                        if (rmdir($ruta))
                            return array('status' => 200);
                        else
                            return array('status' => 400,"mensaje"=>"Error al intentar eliminar directorio");
                    } else
                        return array('status' => 200);
                }else
                    return array('status' => 400,"Error al intentar eliminar imagen");
            } else {
                return array('status' => 200);
            }
        }else{
            return array('status' => 200);
        }

    }




    public function key_exists($new_key)
    {
            $this->db->where('api_key', $new_key);
        return $this->db->get("usuarios")->result();
    }





    /*############################################################## /configuracion ##############################################################*/


    /*********************************************************** mapa y historial *******************************************************************/

    public function obtengo_usuarios_text($id_rol)
    {

        //  $this->db->like('usuario',$palabra);
        $this->db->where('privilegios',$id_rol);
        $this->db->where('status', '1');

        $result=$this->db->select("id, usuario as text")->order_by("usuario")->get("usuarios")->result();
        return $result;
    }


    public function obtengo_promotores($id_cliente=0,$metodo)
    {

        if($id_cliente!=0){

            $query='SELECT  (SELECT usuarios.id FROM usuarios WHERE cliente_promotor.id_promotor=usuarios.id) as id ,
                      (SELECT usuarios.usuario FROM usuarios WHERE cliente_promotor.id_promotor=usuarios.id) as promotor FROM usuarios
                      JOIN cliente ON cliente.id_usuario=usuarios.id 
                      JOIN cliente_promotor ON cliente_promotor.id_cliente=cliente.id 
                      WHERE  usuarios.status = "1" AND usuarios.id = '.$id_cliente.'
                      AND cliente.status = "1" AND cliente_promotor.status = "1"  
                      GROUP BY (SELECT usuarios.id FROM usuarios WHERE cliente_promotor.id_promotor=usuarios.id) ORDER BY usuarios.usuario ASC';

            $resultado=$this->db->query($query)->result();
        }else{
            if($metodo=="vendedor")
                $id_rol=5;
            elseif ($metodo=="promotor")
             $id_rol=4;
            else
                $id_rol=0;


            $this->db->where('privilegios',$id_rol);
            $this->db->where('status', '1');
            $resultado=$this->db->select("id, usuario as promotor")->order_by("usuario")->get("usuarios")->result();
        }
        if($resultado)
            return json_encode(array("status"=>200,"data"=>$resultado));
        else
            return json_encode(array("status"=>404, "data"=>array(),"mensaje"=>"no se encontraron resultados"));
    }


    public function verificar_usuario($nom_usuario){
        $this->db->where("usuario",$nom_usuario);
        return $this->db->get("usuarios")->row();
    }



    public function busco_direcciones_sin_procesar($tabla){
        $resultado=$this->db->select("id, ubic_lat as lat, ubic_lon as lon")
            ->where("ubic_lat!=",0)->where("ubic_lon!=",0)->where("direccion","")->get($tabla)->result();
        if($resultado)
            return json_encode($resultado);
        else
            return false;
    }

    public function guardo_direcciones($direcciones,$tabla){
        foreach ($direcciones as $direccion){

            $this->db->where("id",$direccion["id"])->set("direccion",$direccion["direccion"])->update($tabla);
        }
    }


    public function MaxID($tabla)
    {
        $row=$this->db->select("id")->order_by("id","desc")->get($tabla,1,0)->row();

        if($row)
            return $row->id;
        else return 0;

    }

/********************** cargo datatables de todas las tablas *************************************/

    public function cargo_datatable($tabla,$id=0,$id_rol=null){
// CONCAT(historial.ubic_lat,',',historial.ubic_lon) as latlng

        switch ($tabla){
            case 'usuarios':
                $resultado=$this->db->select('*,DATE_FORMAT(fecha_alta,\'%d/%m/%Y\') as fecha_alta,fecha_alta as fecha_alta_orig')->get($tabla)->result();
                break;

            case 'sucursal':
                $resultado=$this->db->select("sucursal.id, sucursal.sucursal, empresa.id as id_empresa, empresa.empresa, sucursal.status, empresa.status as status_empresa")
                    ->from("sucursal")
                    ->join("empresa","empresa.id=sucursal.id_empresa")
                    ->order_by("sucursal.id","desc")->get()->result();
                break;
            case 'cliente':
                $resultado=$this->db->select("cliente.id, cliente.cliente, cliente.id_usuario, usuarios.usuario, cliente.status, usuarios.status as status_usuario")
                    ->from("cliente")
                    ->join("usuarios","usuarios.id=cliente.id_usuario")
                    ->order_by("cliente.id","desc")->get()->result();
                break;

            case 'cliente_promotor':

               $where=(!is_null($id_rol))?" WHERE cliente_promotor.id_rol=".$id_rol:"";



                $query='SELECT cliente_promotor.id,
                        (SELECT usuarios.usuario FROM usuarios WHERE cliente.id_usuario=usuarios.id) as usuario_cliente, usuarios.status as status_usuario,
                        (SELECT usuarios.usuario FROM usuarios WHERE cliente_promotor.id_promotor=usuarios.id) as promotor, cliente_promotor.id_cliente, cliente_promotor.id_rol,
                          cliente.cliente, cliente.status as status_cliente, usuarios.status as status_promotor, cliente_promotor.id_promotor, cliente.id_usuario as id_usuario_cliente,
                            cliente_promotor.status FROM usuarios
                      JOIN cliente ON cliente.id_usuario=usuarios.id 
                      JOIN cliente_promotor ON cliente_promotor.id_cliente=cliente.id  '.$where.'
                       ORDER BY usuarios.usuario ASC';

                $resultado=$this->db->query($query)->result();
                break;


            case 'producto':
                $resultado=$this->db->select("productos.id, productos.producto, usuarios.id as id_cliente, usuarios.usuario as cliente, cliente.cliente as marca,usuarios.status as status_usuario,
                 cliente.id as id_marca,productos.status")
                    ->from("productos")
                    ->join("cliente","cliente.id=productos.id_cliente")
                    ->join("usuarios","usuarios.id=cliente.id_usuario")
                    ->order_by("productos.id","desc")->get()->result();
                break;

            case 'posicion':
                $resultado=$this->db->select("posicion.id, posicion.posicion, lugar.id as id_lugar, lugar.lugar, posicion.status, lugar.status as status_lugar")
                    ->from("posicion")
                    ->join("lugar","lugar.id=posicion.id_lugar")
                    ->order_by("posicion.id","desc")->get()->result();
                break;

            case 'historial_promotor':

                if($id!=0)
                    $where=' cliente.id_usuario='.$id.' ';
                else
                    $where=' 1=1';

                $query='SELECT historial_pv.id, DATE_FORMAT(historial_pv.fecha,\'%d/%m/%Y %H:%i:%s\') as fecha, historial_pv.foto, usuarios.id as id_usuario, 
                      historial_pv.ubic_lat as lat,historial_pv.direccion as latlng, historial_pv.ubic_lon as lng, IFNULL(estatus.estatus,"--") as estatus,
                      historial_pv.comentario, IFNULL(usuarios.usuario,"--") as promotor, historial_pv.cantidad,posicion.posicion,lugar.lugar,productos.producto,
                       cliente.cliente as marca, (SELECT usuario FROM usuarios WHERE usuarios.id=cliente.id_usuario ) as cliente,
                    
                        IFNULL(empresa.empresa,"--") as empresa, IFNULL(sucursal.sucursal,"--") as sucursal FROM historial_pv

                       
                        LEFT JOIN usuarios ON usuarios.id=historial_pv.id_usuario 
                        LEFT JOIN estatus ON estatus.id=historial_pv.id_estatus 
                        LEFT JOIN productos ON productos.id=historial_pv.id_producto
                         LEFT JOIN cliente ON cliente.id=productos.id_cliente

                          JOIN posicion ON posicion.id=historial_pv.id_posicion
                           JOIN lugar ON lugar.id=posicion.id_lugar
                          
                        LEFT JOIN sucursal ON sucursal.id=historial_pv.id_sucursal 
                        LEFT JOIN empresa ON empresa.id=sucursal.id_empresa 
                        
                        WHERE '.$where.'  GROUP by historial_pv.id ORDER BY historial_pv.id DESC';

                /*
                 * CASE WHEN usuarios.status IS NULL THEN 1=1 ELSE usuarios.status="1" END
                        AND CASE WHEN estatus.status IS NULL THEN 1=1 ELSE estatus.status="1" END
                        AND CASE WHEN empresa.status IS NULL THEN 1=1 ELSE empresa.status="1" END
                        AND CASE WHEN sucursal.status IS NULL THEN 1=1 ELSE sucursal.status="1" END
                 */
                $resultado=$this->db->query($query)->result();

                break;

            case 'historial_vendedor':

                if($id!=0)
                    $where=' cliente.id_usuario='.$id.' ';
                else
                    $where=' 1=1';

                $query='SELECT historial_v.id, DATE_FORMAT(historial_v.fecha,\'%d/%m/%Y %H:%i:%s\') as fecha, historial_v.foto, usuarios.id as id_usuario, 
                      historial_v.ubic_lat as lat,historial_v.direccion as latlng, historial_v.ubic_lon as lng, IFNULL(estatus.estatus,"--") as estatus,
                      historial_v.comentario, IFNULL(usuarios.usuario,"--") as promotor, historial_v.cantidad,posicion.posicion,lugar.lugar,productos.producto,
                       cliente.cliente as marca, (SELECT usuario FROM usuarios WHERE usuarios.id=cliente.id_usuario ) as cliente,
                    
                        IFNULL(empresa.empresa,"--") as empresa, IFNULL(sucursal.sucursal,"--") as sucursal FROM historial_v

                       
                        LEFT JOIN usuarios ON usuarios.id=historial_v.id_usuario 
                        LEFT JOIN estatus ON estatus.id=historial_v.id_estatus 
                        LEFT JOIN productos ON productos.id=historial_v.id_producto
                         LEFT JOIN cliente ON cliente.id=productos.id_cliente

                          JOIN posicion ON posicion.id=historial_v.id_posicion
                           JOIN lugar ON lugar.id=posicion.id_lugar
                          
                        LEFT JOIN sucursal ON sucursal.id=historial_v.id_sucursal 
                        LEFT JOIN empresa ON empresa.id=sucursal.id_empresa 
                        
                        WHERE '.$where.'  GROUP by historial_v.id ORDER BY historial_v.id DESC';

                $resultado=$this->db->query($query)->result();

                break;

            case 'historial_promotor_vendedor':


                if($id!=0)
                    $where=' cliente.id_usuario='.$id.' ';
                else
                    $where='1=1';

                $query='SELECT historial.id, DATE_FORMAT(historial.fecha,\'%d/%m/%Y %H:%i:%s\') as fecha, historial.foto, usuarios.id as id_usuario, 
                      historial.ubic_lat as lat, historial.direccion as latlng, historial.ubic_lon as lng, IFNULL(estatus.estatus,"--") as estatus,
                       historial.comentario, IFNULL(usuarios.usuario,"--") as promotor_vendedor,   IFNULL((SELECT usuario FROM usuarios WHERE usuarios.id=cliente.id_usuario ),"--") as cliente,
                       IFNULL(cliente.cliente,"--")  as marca,
                    
                        IFNULL(empresa.empresa,historial.empresa) as empresa, IFNULL(sucursal.sucursal,"--") as sucursal FROM historial 

                        LEFT JOIN usuarios ON usuarios.id=historial.id_usuario 
                        LEFT JOIN estatus ON estatus.id=historial.id_estatus 
                       
                        LEFT JOIN cliente ON cliente.id=historial.id_cliente 
                        LEFT JOIN sucursal ON sucursal.id=historial.id_sucursal 
                        LEFT JOIN empresa ON empresa.id=sucursal.id_empresa 
                        
                        WHERE '.$where.'  GROUP by historial.id ORDER BY historial.id DESC';

                $resultado=$this->db->query($query)->result();

                break;
            default:
                $resultado=$this->db->get($tabla)->result();

        }

        if($resultado){

            return json_encode(array("status"=>"200","data"=>$resultado));

        }else return json_encode(array("status"=>"404","data"=>array(),"mensaje"=>"No se encontraron registros en la base de datos"));
    }

    public function cargo_combo($tabla,$order,$id=0){

        if($tabla=="usuarios") {

                $this->db->where("privilegios", $order);
            $order='usuario';
        }

        if($id!=0){

            if($tabla=='cliente')
                $this->db->where("id_usuario",$id);
        }

        $this->db->where("status","1");

        return $this->db->order_by($order,"asc")->get($tabla)->result();
    }
    public function cargo_combo_query($tabla,$id_cliente=null){

        if($tabla=="usuarios") {
            $query = 'SELECT usuarios.id, usuarios.usuario  FROM usuarios 
                           JOIN cliente ON cliente.id_usuario=usuarios.id
                          JOIN cliente_promotor ON cliente_promotor.id_cliente=cliente.id
                          WHERE usuarios.privilegios=2 AND (cliente_promotor.id_rol=4 OR cliente_promotor.id_rol=5)  GROUP BY usuarios.id
                          ORDER BY usuarios.usuario ASC';
        }else if($tabla=="cliente") {

            $query = 'SELECT cliente.id, cliente.cliente as marca  FROM usuarios
                           JOIN cliente ON cliente.id_usuario=usuarios.id
                          JOIN cliente_promotor ON cliente_promotor.id_cliente=cliente.id
                          WHERE usuarios.privilegios=2 AND usuarios.id='.$id_cliente.' AND (cliente_promotor.id_rol=4 OR cliente_promotor.id_rol=5)  GROUP BY cliente.id
                          ORDER BY cliente.cliente ASC';
        }

        return $this->db->query($query)->result();
    }




    public function cargo_datos_ubicaciones($datos,$id_rol)
    {
//return $id_rol;

        if($id_rol==4){// promotor
            $query = 'SELECT historial_pv.id,  DATE_FORMAT(historial_pv.fecha,\'%d/%m/%Y %H:%i:%s\') as fecha, historial_pv.foto, usuarios.id as id_usuario,
                          historial_pv.ubic_lat as lat, historial_pv.direccion, historial_pv.ubic_lon as lng,  IFNULL(estatus.estatus,"--") as estatus, historial_pv.comentario,
                          IFNULL(usuarios.usuario,"--") as promotor, historial_pv.cantidad,posicion.posicion,lugar.lugar,productos.producto,
                       cliente.cliente as marca, (SELECT usuario FROM usuarios WHERE usuarios.id=cliente.id_usuario ) as cliente,
                         IFNULL(empresa.empresa,"--") as empresa,  IFNULL(sucursal.sucursal,"--") as sucursal  FROM historial_pv
                      
                      
                        LEFT JOIN usuarios ON usuarios.id=historial_pv.id_usuario 
                        LEFT JOIN estatus ON estatus.id=historial_pv.id_estatus 
                        LEFT JOIN productos ON productos.id=historial_pv.id_producto
                         LEFT JOIN cliente ON cliente.id=productos.id_cliente

                          JOIN posicion ON posicion.id=historial_pv.id_posicion
                           JOIN lugar ON lugar.id=posicion.id_lugar
                         
                        LEFT JOIN sucursal ON sucursal.id=historial_pv.id_sucursal 
                        LEFT JOIN empresa ON empresa.id=sucursal.id_empresa 
                       
                       
                       WHERE  usuarios.privilegios=4 AND
                        historial_pv.id_usuario=' . $datos["id_persona"] . ' AND historial_pv.fecha LIKE "%' . $datos["fecha"] . '%" 
                       ORDER BY historial_pv.id ASC';

        }else if($id_rol==5)
        {
            $query = 'SELECT historial_v.id,  DATE_FORMAT(historial_v.fecha,\'%d/%m/%Y %H:%i:%s\') as fecha, historial_v.foto, usuarios.id as id_usuario,
                          historial_v.ubic_lat as lat, historial_v.direccion, historial_v.ubic_lon as lng,  IFNULL(estatus.estatus,"--") as estatus, historial_v.comentario,
                          IFNULL(usuarios.usuario,"--") as promotor, historial_v.cantidad,posicion.posicion,lugar.lugar,productos.producto,
                       cliente.cliente as marca, (SELECT usuario FROM usuarios WHERE usuarios.id=cliente.id_usuario ) as cliente,
                         IFNULL(empresa.empresa,"--") as empresa,  IFNULL(sucursal.sucursal,"--") as sucursal  FROM historial_v
                      
                      
                        LEFT JOIN usuarios ON usuarios.id=historial_v.id_usuario 
                        LEFT JOIN estatus ON estatus.id=historial_v.id_estatus 
                        LEFT JOIN productos ON productos.id=historial_v.id_producto
                         LEFT JOIN cliente ON cliente.id=productos.id_cliente

                          JOIN posicion ON posicion.id=historial_v.id_posicion
                           JOIN lugar ON lugar.id=posicion.id_lugar
                         
                        LEFT JOIN sucursal ON sucursal.id=historial_v.id_sucursal 
                        LEFT JOIN empresa ON empresa.id=sucursal.id_empresa 
                       
                       
                       WHERE  usuarios.privilegios=5 AND
                        historial_v.id_usuario=' . $datos["id_persona"] . ' AND historial_v.fecha LIKE "%' . $datos["fecha"] . '%" 
                       ORDER BY historial_v.id ASC';
        }else
        {


            $query = 'SELECT historial.id, DATE_FORMAT(historial.fecha,\'%d/%m/%Y %H:%i:%s\') as fecha, historial.foto, usuarios.id as id_usuario, 
                      historial.ubic_lat as lat, historial.direccion, historial.ubic_lon as lng, IFNULL(estatus.estatus,"--") as estatus,
                       historial.comentario, IFNULL(usuarios.usuario,"--") as promotor_vendedor,   IFNULL((SELECT usuario FROM usuarios WHERE usuarios.id=cliente.id_usuario ),"--") as cliente,
                       IFNULL(cliente.cliente,"--")  as marca,
                    
                        IFNULL(empresa.empresa,historial.empresa) as empresa, IFNULL(sucursal.sucursal,"--") as sucursal FROM historial 
                        
                          LEFT JOIN usuarios ON usuarios.id=historial.id_usuario
                          LEFT JOIN estatus ON estatus.id=historial.id_estatus
                        LEFT JOIN cliente ON cliente.id=historial.id_cliente
                  
                       LEFT JOIN sucursal ON sucursal.id=historial.id_sucursal
                       
                       LEFT JOIN empresa ON empresa.id=sucursal.id_empresa WHERE  usuarios.privilegios=0 AND
                        historial.id_usuario=' . $datos["id_persona"] . ' AND historial.fecha LIKE "%' . $datos["fecha"] . '%" 
                       ORDER BY historial.id ASC';




            /*
             * AND
                                    CASE WHEN usuarios.status IS NULL THEN 1=1 ELSE usuarios.status="1" END AND
                                    CASE WHEN estatus.status IS NULL THEN 1=1 ELSE estatus.status="1" END AND
                                    CASE WHEN empresa.status IS NULL THEN 1=1 ELSE empresa.status="1" END AND
                                    CASE WHEN sucursal.status IS NULL THEN 1=1 ELSE sucursal.status="1" END
             */
        }


        $result=$this->db->query($query)->result();

        return $result;
    }

    public function obtengo_datos_grafica($post,$id_rol){

        $fecha_elegida=$post["fecha"];
        $fecha_menos_30=date("Y-m-d",strtotime($fecha_elegida."- 30 days"));


        if($id_rol==0) // promotor vendedor
                    $from="historial";
        else if($id_rol==5)
            $from="historial_v";
        else
            $from="historial_pv"; //promotor

        $query='SELECT DATE_FORMAT(fecha,\'%Y/%m/%d\') as   fecha, COUNT(DATE_FORMAT(fecha,\'%Y/%m/%d\')) as visitas FROM '.$from.'
            WHERE id_usuario='.$post["id_promotor"].' AND DATE_FORMAT(fecha,\'%Y-%m-%d\')<="'.$fecha_elegida.'"  AND DATE_FORMAT(fecha,\'%Y-%m-%d\')>="'.$fecha_menos_30.'" GROUP BY DATE_FORMAT(fecha,\'%Y/%m/%d\') ORDER BY fecha';

        $result=$this->db->query($query)->result();

        if($result){
            return json_encode(array("status"=>200,"data"=>$result));
        }else{
            return json_encode(array("status"=>404,"data"=>"no se encontraron registros"));
        }
    }

}
?>