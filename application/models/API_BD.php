<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class API_BD extends CI_Model
{

    public function acceso_api($usuario)
    {

        $posibleUsuario = $this->db->select("id,usuario,api_key,privilegios as id_rol")
            ->where('pass',$usuario["pass"])->where('usuario',$usuario["usuario"])->where('status', "1")->where('(privilegios=0 OR privilegios=4 OR privilegios=5)')->get('usuarios')->row();

        if ($posibleUsuario) {
                return $posibleUsuario;
        } else
            return false;
    }

    /*********************************************************** usuarios *******************************************************************/
    public function obtengo_usuario_api($id)
    {
        if ($id != 'todos')
            $this->db->where('id', $id);

        $this->db->where('status', '1');
        $this->db->where('privilegios', '0')->select("id,usuario,pass,api_key");
        if ($id != 'todos')
                return $this->db->get("usuarios")->row();
        else
            return $this->db->get("usuarios")->result();
    }


    public function key_exists($new_key,$id_usuario)
    {
        $this->db->where('api_key', $new_key)->where('id', $id_usuario);
        return $this->db->get("usuarios")->result();
    }




    /*********************************************************** datos necesarios para app  *******************************************************************/

    public function obtengo_historial_api($id)
    {

        if ($id != 'todos') {
            $this->db->where('id', $id);
            return $this->db->get("historial")->row();
        }else
            return $this->db->get("historial")->result();
    }

    public function obtengo_estatus_api($id)
    {

        $this->db->select("id,estatus");
        $this->db->where('status', "1");
        if ($id != 'todos') {
            $this->db->where('id', $id);
            return $this->db->get("estatus")->row();
        }else
            return $this->db->get("estatus")->result();
    }
    public function obtengo_empresa_sucursal_api($id)
    {

        $this->db->select("id,empresa");
        $this->db->where('status', "1");
        if ($id != 'todos') {
            $this->db->where('id', $id);

            return $this->db->get("estatus")->row();
        }else {
            $emp=$this->db->get("empresa")->result_array();

            for($i=0;$i<count($emp);$i++){
                $emp[$i]["sucursal"]=array();

                $suc=$this->db->where("id_empresa",$emp[$i]["id"])->where("status","1")->select("id,sucursal")->get("sucursal")->result_array();
                for($j=0;$j<count($suc);$j++){
                    $emp[$i]["sucursal"][$j]=array();
                    $emp[$i]["sucursal"][$j]["id"]=$suc[$j]["id"];
                    $emp[$i]["sucursal"][$j]["sucursal"]=$suc[$j]["sucursal"];
                }


            }

            return $emp;

        }
    }

    public function existe_actualizacion($ultima_actualizacion){
        $query='SELECT id FROM ultima_actualizacion WHERE ultima_actualizacion!="'.$ultima_actualizacion.'"';
        $consulta=$this->db->query($query);
        if($consulta->num_rows())
            return true;
        else
            return false;
    }
    public function obtengo_ultima_actualizacion(){
        $query='SELECT ultima_actualizacion FROM ultima_actualizacion WHERE id=1';
        $consulta=$this->db->query($query);
        if($consulta->num_rows()>0) {
            $consulta=$consulta->row();
            return $consulta->ultima_actualizacion;
        }else
            return null;
    }

    public function obtengo_cliente_api($id,$id_rol)
    {


        $query='SELECT cliente.id, cliente.cliente FROM cliente 
                      JOIN cliente_promotor ON cliente_promotor.id_cliente=cliente.id WHERE cliente_promotor.id_promotor='.$id.' AND
                      cliente_promotor.id_rol='.$id_rol.' AND cliente.status=1 AND cliente_promotor.status=1
                       ORDER BY cliente.cliente ASC';

        $cliente=$this->db->query($query)->result_array();


        if($id_rol==4 || $id_rol==5) {// promotor vendedor
            for ($i = 0; $i < count($cliente); $i++) {
                $consul = $this->db->select("id,producto")->where('status', "1")->where("id_cliente", $cliente[$i]["id"])->get("productos")->result_array();
                $cliente[$i]["producto"] = array();
                if ($consul) {
                    $cliente[$i]["producto"] = $consul;
                }
            }
        }

        return $cliente;
    }


    public function obtengo_lugar_api()
    {


        $query='SELECT id, lugar FROM lugar where status=1
                       ORDER BY lugar ASC';

       // $puerta=true;

        $lugar=$this->db->query($query)->result_array();

        if($lugar) {


            for ($i = 0; $i < count($lugar); $i++) {
                $consul = $this->db->select("id,posicion")->where('status', "1")->where("id_lugar", $lugar[$i]["id"])->get("posicion")->result_array();
                $lugar[$i]["posicion"] = array();
                if ($consul) {
                    $lugar[$i]["posicion"] = $consul;
                }
            }

                return $lugar;


        }else return false;



    }



    public function historial_put($tabla,$put)
    {
            return $this->db->insert($tabla,$put);
    }



    public function existe_estatus($id_estatus)
    {
        $this->db->where('id', $id_estatus);
        return $this->db->order_by("estatus","ASC")->get("estatus")->row();
    }

    public function obtener($param){

        switch ($param["obtengo"]){
            case "empresa":
                $result["empresas"]=$this->db->order_by("empresa")->select("id,empresa")->where("status","1")->get("empresa")->result_array();
                break;
            case "sucursal":
                $result["sucursales"]=$this->db->order_by("sucursal")->select("id,sucursal")->where("status","1")->where("id_empresa",$param["id_empresa"])->get("sucursal")->result_array();
                break;
            case "cliente":
                 $query='SELECT cliente.id, cliente.cliente FROM cliente 
                      JOIN cliente_promotor ON cliente_promotor.id_cliente=cliente.id WHERE cliente_promotor.id_promotor='.$param["id_usuario"].' AND
                      cliente_promotor.id_rol='.$this->obtengo_rol($param["id_usuario"]).' AND cliente.status=1 AND cliente_promotor.status=1
                       ORDER BY cliente.cliente ASC';
                $result["clientes"]= $this->db->query($query)->result_array();
                break;
            case "estatus":
                $result["estatus"]=$this->db->order_by("estatus")->select("id,estatus")->where("status","1")->get("estatus")->result_array();
                break;

            case "lugar":
                $result["lugares"]=$this->db->order_by("lugar")->select("id,lugar")->where("status","1")->get("lugar")->result_array();
                break;

            case "posicion":
                $result["posiciones"]=$this->db->order_by("posicion")->select("id,posicion")->where("status","1")->where("id_lugar",$param["id_lugar"])->get("posicion")->result_array();
                break;

            case "producto":
                $result["productos"]=$this->db->order_by("producto")->select("id,producto")->where("status","1")->where("id_cliente",$param["id_cliente"])->get("productos")->result_array();
                break; 



        }
        $result["status"]=400;
        if($result)
            $result["status"]=200;

        return $result;

    }


    public function obtengo_rol($id_usuario)
    {

            $this->db->where('id', $id_usuario);

        $this->db->where('status', '1');
        $this->db->select("privilegios as id_rol");

            $result=$this->db->get("usuarios")->row();

            if($result)
                return $result->id_rol;
            else
                return -1;
    }



}