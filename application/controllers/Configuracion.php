<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracion extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            $this->load->model("BaseDatos");

        }

    public function _remap($metodo, $parametros = array())
    {
        if($this->session->userdata('acceso_mapvd')) {

              if($this->session->userdata('id_privilegio')==1) { // super usuario

                    switch ($metodo) {
                        case 'usuarios':
                            $this->usuarios($parametros);
                            break;
                        case 'cadena':
                            $this->cadena($parametros);
                            break;
                        case 'sucursal':
                            $this->sucursal($parametros);
                            break;
                        case 'cliente':
                            $this->cliente($parametros);
                            break;
                        case 'cliente-promotor':
                            $this->cliente_promotor($parametros);
                            break;
                        case 'estatus':
                            $this->estatus($parametros);
                            break;
                        case 'verificar_usuario':
                            $this->verificar_usuario();
                            break;

                        case 'cargo_combo':
                            $this->cargo_combo($parametros);
                            break;
                        case 'producto':
                            $this->producto($parametros);
                            break;
                        case 'lugar':
                            $this->lugar($parametros);
                            break;

                        case 'posicion':
                            $this->posicion($parametros);
                            break;
                        case 'obtengo_promotores':

                            echo $this->obtengo_promotores(0,$parametros);

                            break;

                        default:
                            redirect(base_url());
                    }
              } else if($this->session->userdata('id_privilegio')==2) { // cliente
                  switch ($metodo) {

                      case 'obtengo_promotores':

                          echo $this->obtengo_promotores($this->session->userdata('id'),$parametros);

                          break;

                      default:
                          redirect(base_url());
                  }
              }else
                  redirect(base_url().'mapa');
        }else
            $this->load->view('loginView');
    }


    public function obtengo_promotores($id_usuario=0,$parametro){

            if($id_usuario==0){
                $resultado=$this->BaseDatos->obtengo_promotores(0,$parametro[0]);
            }else{
                $resultado=$this->BaseDatos->obtengo_promotores($id_usuario,$parametro[0]);
            }

            echo $resultado;
    }
    /*   -------------------------------------------   USUARIOS  ------------------------------------------- */

    /**
     *
     */


    public function usuarios($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('usuarios');

                        break;
                    case 'agregar':
                        $_POST["api_key"]=$this->generate_key();
                        $resultado=$this->BaseDatos->agregar('usuarios',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('usuarios',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $resultado=$this->BaseDatos->eliminar($_POST,"./fotos_perfil/".$_POST["id"]."/","foto_perfil");

                        echo json_encode($resultado);

                        break;
                    default:
                          redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Usuarios';
            $vista["titulo_con_espacios"]='Usuarios';
            $vista["titulo"]=$cabeza["titulo"];

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Foto</th><th>Usuario</th><th>Correo</th><th>Contraseña</th><th>Teléfono</th><th>Dirección</th><th>Tarjeta de dispersión</th>
                                    <th>Fecha alta</th><th>Sueldo</th><th>Rol</th>';

            $vista["modal_body_agregar"]=' <div class="row center-block">
                                            <div class="col-sm-12">
                                             <label for="formapagos">Foto</label>
                                                 <input type="file" class="form-control" accept="image/*" id="usuarios_foto_input" autocomplete="off" required>
                                                 <div id="usuarios_muestro_foto" class="hidden"><img width="50%"></div>
                                                 <input type="hidden" id="usuarios_foto" name="foto">
                                            </div>
                                        </div> 

                                <br><div class="form-row">
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Usuario</label>
                                             <input type="text" class="form-control" id="usuarios_usuario" name="usuario" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Correo</label>
                                             <input type="email" class="form-control" id="usuarios_correo" name="correo" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Contraseña</label>
                                             <input type="text" class="form-control" id="usuarios_pass" name="pass" minlength="5" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Teléfono</label>
                                             <input type="number" class="form-control" id="usuarios_telefono" name="telefono" minlength="10" maxlength="12" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Dirección</label>
                                             <textarea class="form-control" id="usuarios_direccion" maxlength="300" name="direccion" autocomplete="off" required></textarea>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Tarjeta de dispersión</label>
                                             <input type="text" class="form-control" id="usuarios_tarjeta_dispersion" name="tarjeta_dispersion" autocomplete="off" required>
                                        </div>
                                        <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Fecha alta</label>
                                             <input type="date" class="form-control" id="fecha_alta" name="fecha_alta" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Sueldo</label>
                                             <input type="text" class="form-control" id="usuarios_sueldo" name="sueldo" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Rol</label>
                                             <select type="text" class="form-control" id="usuarios_privilegio" name="privilegios" required><option value="">Elige una opción...</option>
                                                <option value="1">Administrador</option><option value="2">Cliente</option><option value="4">Promotor</option><option value="0">Promotor-Vendedor</option>
                                                <option value="5">Vendedor</option></select>
                                        </div>
                                       
                                        <br><br>
                                    </div>';

            $vista["modal_body_editar"]=' <div class="row center-block">
                                            <div class="col-sm-12">
                                             <label for="formapagos">Foto</label>
                                                 <input type="file" class="form-control" accept="image/*" id="usuarios_foto_input_e" autocomplete="off">
                                                 <div id="usuarios_muestro_foto_e" class="hidden"><img width="50%"></div>
                                                 <input type="hidden" id="usuarios_foto_e" name="foto">
                                            </div>
                                        </div> 

                                <br><div class="form-row">
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Usuario</label> 
                                             <input type="text" class="form-control"  data-edito="e" id="usuarios_usuario_e" name="usuario" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Correo</label>
                                             <input type="email" class="form-control" id="usuarios_correo_e" name="correo" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Contraseña</label>
                                             <input type="text" class="form-control" id="usuarios_pass_e" name="pass" minlength="5" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Teléfono</label>
                                             <input type="number" class="form-control" id="usuarios_telefono_e" name="telefono" minlength="10" maxlength="12" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Dirección</label>
                                             <textarea class="form-control" id="usuarios_direccion_e" maxlength="300" name="direccion" autocomplete="off" required></textarea>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Tarjeta de dispersión</label>
                                             <input type="text" class="form-control" id="usuarios_tarjeta_dispersion_e" name="tarjeta_dispersion" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Fecha alta</label>  
                                             <input type="date" class="form-control" id="fecha_alta_e" name="fecha_alta" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Sueldo</label>
                                             <input type="text" class="form-control" id="usuarios_sueldo_e" name="sueldo" autocomplete="off" required>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Rol</label>
                                             <select type="text" class="form-control" id="usuarios_privilegio_e" name="privilegios" required><option value="">Elige una opción...</option>
                                                <option value="1">Administrador</option><option value="2">Cliente</option><option value="4">Promotor</option><option value="0">Promotor-Vendedor</option>
                                                <option value="5">Vendedor</option></select>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" id="usuarios_id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }

    private function generate_key()
    {
        do
        {
            // Generate a random salt
            $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

            // If an error occurred, then fall back to the previous method
            if ($salt === FALSE)
            {
                $salt = hash('sha256', time() . mt_rand());
            }

            $new_key = substr($salt, 0, 40);
        }
        while ($this->key_exists($new_key));

        return $new_key;
    }


    private function key_exists($new_key)
    {
       $resultado=$this->BaseDatos->key_exists($new_key);
       if($resultado)
           return true;
       else
           return false;
    }


    public function buscoUsuarios()
    {
        if($this->input->is_ajax_request()) {
            $respuesta = $this->PanelModel->buscoUsuario($_POST);
            echo json_encode($respuesta);
        }else{
            echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));
        }
    }

    public function cargo_combo($parametros)
    {
        if($this->input->is_ajax_request()) {

            switch ($parametros[0]){
                case 'cliente':
                    $respuesta=$this->BaseDatos->cargo_combo("cliente","cliente",$_POST["id_usuario_cliente"]);
                    break;
            }
            if($respuesta)
            echo json_encode(array("status"=>200,"data"=>$respuesta));
            else
                echo json_encode(array("status"=>404,"data"=>array()));
        }else{
            echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));
        }
    }

    public function verificar_usuario(){

        if($_POST["usuario"]){
           $resultado= $datos=$this->BaseDatos->verificar_usuario($_POST["usuario"]);
           if($resultado)
               echo true;
           else echo false;
        } else echo true;
    }

    // cadena ======


    public function cadena($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('empresa');

                        break;
                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('empresa',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('empresa',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Cadena';
            $vista["titulo_con_espacios"]='Cadena';
            $vista["titulo"]=$cabeza["titulo"];

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Cadena</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Cadena</label>
                                             <input type="text" class="form-control" id="empresa" name="empresa" autocomplete="off" required>
                                        </div>
                                        <br><br>
                                    </div>';

            $vista["modal_body_editar"]='<div class="form-row">
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Cadena</label>
                                             <input type="text" class="form-control" id="empresa_e" name="empresa" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" id="empresa_id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }


    // sucursal ======


    public function sucursal($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('sucursal');

                        break;
                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('sucursal',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('sucursal',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Sucursal';
            $vista["titulo_con_espacios"]='Sucursal';
            $vista["titulo"]=$cabeza["titulo"];


            $empresas=$this->BaseDatos->cargo_combo("empresa","empresa");

            $combo='<select name="id_empresa" class="form-control"  id="id_empresa" required>';
            $combo_aux='<option value="">Elige una opción...</option>';
            $combo_e='<select name="id_empresa" class="form-control"  id="id_empresa_e" required>';

            foreach ($empresas as $empresa){
                $combo_aux.='<option value="'.$empresa->id.'">'.$empresa->empresa.'</option>';
            }

            $combo_aux.='</select>';

            $combo.=$combo_aux;
            $combo_e.=$combo_aux;

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Cadena</th><th>Sucursal</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cadena</label>
                                             '.$combo.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Sucursal</label>
                                             <input type="text" class="form-control" id="sucursal" name="sucursal" autocomplete="off" required>
                                        </div>
                                        <br><br>
                                    </div>';

            $vista["modal_body_editar"]='<div class="form-row">
                                            <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cadena</label>
                                             '.$combo_e.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Sucursal</label>
                                             <input type="text" class="form-control" id="sucursal_e" name="sucursal" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" id="empresa_id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }


    // cliente ======


    public function cliente($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('cliente');

                        break;
                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('cliente',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('cliente',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Cliente';
            $vista["titulo_con_espacios"]='Cliente';
            $vista["titulo"]=$cabeza["titulo"];


            $clientes=$this->BaseDatos->cargo_combo("usuarios",2);

            $combo='<select name="id_usuario" class="form-control"  id="id_usuario" required>';
            $combo_aux='<option value="">Elige una opción...</option>';
            $combo_e='<select name="id_usuario" class="form-control"  id="id_usuario_e" required>';

            foreach ($clientes as $cliente){
                $combo_aux.='<option value="'.$cliente->id.'">'.$cliente->usuario.'</option>';
            }

            $combo_aux.='</select>';

            $combo.=$combo_aux;
            $combo_e.=$combo_aux;

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Cliente</th><th>Marca</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cliente</label>
                                             '.$combo.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Marca</label>
                                             <input type="text" class="form-control" maxlength="100" id="cliente" name="cliente" autocomplete="off" required>
                                        </div>
                                        <br><br>
                                    </div>';


            $vista["modal_body_editar"]='<div class="form-row">
                                            <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cliente</label>
                                             '.$combo_e.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Marca</label>
                                            <input type="text" class="form-control" maxlength="100" id="cliente_e" name="cliente" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }


    // cliente-promotor ======


    public function cliente_promotor($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('cliente_promotor');

                        break;

                    case 'obtengo_usuarios_rol_seleccionado':


                       echo $this->obtengo_usuarios_rol_seleccionado($_POST["id_rol"]);
                        break;

                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('cliente_promotor',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('cliente_promotor',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $_POST["tabla"]='cliente_promotor';
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='ClientePromotor';
            $vista["titulo_con_espacios"]='Cliente-Promotor';
            $vista["titulo"]=$cabeza["titulo"];


            $clientes=$this->BaseDatos->cargo_combo("usuarios",2);
            //$promotores=$this->BaseDatos->cargo_combo("usuarios",0);

            $combo='<select  class="form-control"  id="id_usuario_cliente" required>';
            $combo_aux='<option value="">Elige una opción...</option>';
            $combo_e='<select  class="form-control"  id="id_usuario_cliente_e" required>';


            $combo2='<select name="id_promotor" class="form-control"  id="id_promotor" required>';
            $combo_aux2='<option value="">Elige una opción...</option>';
            $combo_e2='<select name="id_promotor" class="form-control"  id="id_promotor_e" required>';

            foreach ($clientes as $cliente){
                $combo_aux.='<option value="'.$cliente->id.'">'.$cliente->usuario.'</option>';
            }


            $combo_aux.='</select>';

            $combo.=$combo_aux;
            $combo_e.=$combo_aux;

            $combo_aux2.='</select>';

            $combo2.=$combo_aux2;
            $combo_e2.=$combo_aux2;

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Cliente</th><th>Marca</th><th>Rol</th><th>Usuario</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                     <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cliente</label>
                                         '.$combo.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Marca</label>
                                            <select class="form-control" name="id_cliente" id="id_cliente" required><option value="">Elige una opción...</option></select>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Rol</label>
                                            <select class="form-control id_rol" name="id_rol" id="id_rol" required><option value="">Elige una opción...</option><option value="4">Promotor</option><option value="0">Promotor-Vendedor</option>
                                            <option value="5">Vendedor</option></select>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Usuario</label>
                                             '.$combo2.'
                                        </div>
                                        <br><br>
                                    </div>';


            $vista["modal_body_editar"]='<div class="form-row">
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cliente</label>
                                         '.$combo_e.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Marca</label>
                                            <select class="form-control" name="id_cliente" id="id_cliente_e" required></select>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Rol</label>
                                            <select class="form-control id_rol" data-edito="true" name="id_rol" id="id_rol_e" required><option value="">Elige una opción...</option><option value="4">Promotor</option><option value="0">Promotor-Vendedor</option>
                                            <option value="5">Vendedor</option></select>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Usuario</label>
                                             '.$combo_e2.'
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }

    private function obtengo_usuarios_rol_seleccionado($rol){

        $usuarios=$this->BaseDatos->cargo_combo("usuarios",$rol);
        if($usuarios)
        return json_encode(array("status"=>200,"data"=>$usuarios));
        else
            return json_encode(array("status"=>400,"mensaje"=>"Error al consultar base de datos"));
    }

    // estatus ======


    public function estatus($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('estatus');

                        break;
                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('estatus',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('estatus',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Estatus';
            $vista["titulo_con_espacios"]='Estatus';
            $vista["titulo"]=$cabeza["titulo"];

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Estatus</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Estatus</label>
                                             <input type="text" class="form-control" id="estatus" name="estatus" autocomplete="off" required>
                                        </div>
                                        <br><br>
                                    </div>';

            $vista["modal_body_editar"]='<div class="form-row">
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Estatus</label>
                                             <input type="text" class="form-control" id="estatus_e" name="estatus" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }


    // producto ======


    public function producto($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('producto');

                        break;
                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('productos',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'obtengo_marcas':
                           $this->obtengo_marcas($_POST["id_cliente"]);

                        break;

                    case 'editar':
                        $resultado=$this->BaseDatos->editar('productos',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $_POST["tabla"]="productos";
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Producto';
            $vista["titulo_con_espacios"]='Producto';
            $vista["titulo"]=$cabeza["titulo"];


           $clientes=$this->BaseDatos->cargo_combo_query("usuarios");

            $combo='<select class="form-control"  id="id_cliente" required>';
            $combo_aux='<option value="">Elige una opción...</option>';
            $combo_e='<select  class="form-control"  id="id_cliente_e" data-edito="true" required>';

            foreach ($clientes as $cliente){
                $combo_aux.='<option value="'.$cliente->id.'">'.$cliente->usuario.'</option>';
            }

            $combo_aux.='</select>';

            $combo.=$combo_aux;
            $combo_e.=$combo_aux;

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Cliente</th><th>Marca</th><th>Producto</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cliente</label>
                                             '.$combo.'
                                        </div>
                                           <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Marca</label>
                                             <select class="form-control" name="id_cliente" id="id_marca" required><option value="">Elige una opción...</option></select>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Producto</label>
                                             <input type="text" class="form-control" id="producto" name="producto" autocomplete="off" required>
                                        </div>
                                        <br><br>
                                    </div>';

            $vista["modal_body_editar"]='<div class="form-row">
                                            <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Cliente</label>
                                             '.$combo_e.'
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Marca</label>
                                             <select class="form-control" name="id_cliente" id="id_marca_e" required><option value="">Elige una opción...</option></select>
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Producto</label>
                                             <input type="text" class="form-control" id="producto_e" name="producto" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" id="empresa_id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }

    private function obtengo_marcas($id_cliente){

        $usuarios=$this->BaseDatos->cargo_combo_query("cliente",$id_cliente);
        if($usuarios)
            echo json_encode(array("status"=>200,"data"=>$usuarios));
        else
            echo json_encode(array("status"=>400,"mensaje"=>"Error al consultar base de datos"));
    }



    // lugar ======


    public function lugar($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('lugar');

                        break;
                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('lugar',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('lugar',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Lugar';
            $vista["titulo_con_espacios"]='Lugar';
            $vista["titulo"]=$cabeza["titulo"];

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Lugar</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Lugar</label>
                                             <input type="text" class="form-control" id="lugar" name="lugar" maxlength="150" autocomplete="off" required>
                                        </div>
                                        <br><br>
                                    </div>';

            $vista["modal_body_editar"]='<div class="form-row">
                                         <div class="form-group col-sm-12">
                                         <label for="formapagos">Lugar</label> 
                                             <input type="text" class="form-control" id="lugar_e" name="lugar" maxlength="150" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id" required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }


    // posicion ======


    public function posicion($parametros)
    {

        if(isset($parametros[0])){

            if($this->input->is_ajax_request()) {
                switch ($parametros[0]) {

                    case 'cargo_datatable':

                        echo $this->cargo_datatable('posicion');

                        break;
                    case 'agregar':
                        $resultado=$this->BaseDatos->agregar('posicion',$_POST);

                        echo json_encode($resultado);

                        break;
                    case 'editar':
                        $resultado=$this->BaseDatos->editar('posicion',$_POST);

                        echo json_encode($resultado);

                        break;

                    case 'eliminar':
                        $resultado=$this->BaseDatos->eliminar($_POST);

                        echo json_encode($resultado);

                        break;
                    default:
                        redirect(base_url());
                        exit();
                        break;
                }

            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

        }else{
            $cabeza["usuario"]=$this->session->userdata('usuario');
            $cabeza["privilegio"]=$this->session->userdata('privilegio');
            $cabeza["id_privilegio"]=$this->session->userdata('id_privilegio');
            $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
                :base_url().'plantilla/assets/images/users/avatar.png';
            $cabeza["id"]=$this->session->userdata('id');
            $cabeza["m_titulo"]='Configuración';
            $cabeza["titulo"]='Posición';
            $vista["titulo_con_espacios"]='Posición';
            $vista["titulo"]=$cabeza["titulo"];


            $lugares=$this->BaseDatos->cargo_combo("lugar","lugar");

            $combo='<select name="id_lugar" class="form-control"  id="id_lugar" required>';
            $combo_aux='<option value="">Elige una opción...</option>';
            $combo_e='<select name="id_lugar" class="form-control"  id="id_lugar_e" required>';

            foreach ($lugares as $lugar){
                $combo_aux.='<option value="'.$lugar->id.'">'.$lugar->lugar.'</option>';
            }

            $combo_aux.='</select>';

            $combo.=$combo_aux;
            $combo_e.=$combo_aux;

            $vista["cabeza_tabla"]='<tr><th>Id</th><th>Lugar</th><th>Posición</th>';

            $vista["modal_body_agregar"]='<div class="form-row">
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Lugar</label>
                                             '.$combo.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Posición</label>
                                             <input type="text" class="form-control" id="posicion" name="posicion" autocomplete="off" required>
                                        </div>
                                        <br><br>
                                    </div>';

            $vista["modal_body_editar"]='<div class="form-row">
                                            <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Lugar</label>
                                             '.$combo_e.'
                                        </div>
                                         <div class="form-group col-sm-12 col-md-6">
                                         <label for="formapagos">Posición</label>
                                             <input type="text" class="form-control" id="posicion_e" name="posicion" autocomplete="off" required>
                                        </div>
                                          <div class="form-group col-sm-12 col-md-6">
                                     <label for="formapagos">Status</label>
                                         <br><input type="radio"  name="status" value="1" required> Activo<br>
                                         <input type="radio"   name="status" value="0" required> Deshabilitado
                                         <input type="hidden"   name="id"  required>
                                    </div><br><br>
                            </div>';

            $vista["cabeza_tabla"].='<th>Editar</th></tr>';
            $this->load->view('cabeza',$cabeza);
            $this->load->view('plantilla_configuracion',$vista);
        }
    }



    // para todos
    public function cargo_datatable($tabla){

        return $this->BaseDatos->cargo_datatable($tabla);

    }


}
?>
