<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("API_BD");
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }


    public function login_post()
    {

        $datos["usuario"]=htmlspecialchars($this->post("usuario"));
        $datos["pass"]=htmlspecialchars($this->post("pass"));

            $usuario=$this->API_BD->acceso_api($datos);


            // Check if the users data store contains users (in case the database result returns NULL)
            if ($usuario) {



                    if (($this->post("ultima_actualizacion") && $this->existe_actualizacion($this->post("ultima_actualizacion"))) || !$this->post("ultima_actualizacion")){



                        $estatus = $this->API_BD->obtengo_estatus_api('todos');
                    $cliente = $this->API_BD->obtengo_cliente_api($usuario->id, $usuario->id_rol);


                    $empresa_sucursal = $this->API_BD->obtengo_empresa_sucursal_api('todos');


                    if ($estatus && $empresa_sucursal && $cliente) {
                        // Set the response and exit
                        $login_datos["status"] = 200;
                        $login_datos["ultima_actualizacion"] = $this->API_BD->obtengo_ultima_actualizacion();

                        //cambio los roles
                        $id_rol_cambio=($usuario->id_rol==4 || $usuario->id_rol==5)?0:4;

                        $login_datos["usuario"] = $usuario;
                        $login_datos["estatus"] = $estatus;
                        $login_datos["empresa"] = $empresa_sucursal;
                        $login_datos["cliente"] = $cliente;



                        if ($usuario->id_rol == 4 || $usuario->id_rol == 5) {// promotor-vendedor
                            $lugar = $this->API_BD->obtengo_lugar_api();
                            if ($lugar) {
                                $login_datos["lugar"] = $lugar;
                            } else {
                                // Set the response and exit
                                $this->response([
                                    'status' => 404,
                                    'message' => 'El campo lugar esta vacío'
                                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                            }
                        }

                        $login_datos["usuario"]->id_rol=$id_rol_cambio;
                        $this->response($login_datos, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                    } else {
                        // Set the response and exit
                        $this->response([
                            'status' => 404,
                            'message' => 'Uno o más de los campos están vacíos'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }
                }else
                    {
                        // Set the response and exit
                        $this->response([
                            'status' => 204,
                            'message' => 'No hay actualizaciones'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => 401,
                    'message' => 'usuario o contraseña incorrectos'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
    }

    public function existe_actualizacion($ultima_actualizacion){
        return $this->API_BD->existe_actualizacion($ultima_actualizacion);
    }
    public function agregar_post()
    {
        // Users from a data store e.g. database

        $api_key=$this->post("api_key");

       // $agregar = $this->get('agregar');

            $id_usuario=$this->post("id_usuario");
            if($this->key_exists($api_key,$id_usuario)) {


                    if (!isset($_FILES["foto"]["size"]))
                        $_FILES["foto"]["size"] = 0;

                    $put["id_usuario"] = $this->post("id_usuario");
                    $put["id_estatus"] = $this->post("id_estatus");
                    $put["fecha"] = $this->post("fecha");
                    $put["ubic_lat"] = $this->post("ubic_lat");
                    $put["ubic_lon"] = $this->post("ubic_lon");
                    $put["foto"] = $this->post("foto");
                    $put["comentario"] = $this->post("comentario");
                    //$put["empresa"] = $this->post("empresa");

                    $put["id_sucursal"] = $this->post("id_sucursal");

                    $id_rol=$this->API_BD->obtengo_rol($id_usuario);


                    if($id_rol==4 || $id_rol==5){// promotor-vendedor
                        $put["id_producto"] = $this->post("id_producto");
                        $put["id_posicion"] = $this->post("id_posicion");
                        $put["cantidad"] = $this->post("cantidad");

                        if (!isset($_POST["id_producto"]) || $put["id_producto"] == '' || !isset($_POST["id_posicion"]) || $put["id_posicion"] == '' || !isset($_POST["cantidad"]) || $put["cantidad"]=='') {
                            $this->response([
                                'status' => 401,
                                'message' => 'Los campos son obligatorios (excepto comentario)'
                            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
                        }

                    }else{
                        $put["id_cliente"] = $this->post("id_cliente");
                        if(!isset($_POST["id_cliente"]) || $_POST["id_cliente"]==''){
                            $this->response([
                                'status' => 401,

                                'message' => 'Los campos son obligatorios (excepto comentario)'
                            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
                        }

                    }

                if($this->existe_estatus($put["id_estatus"])) {

                    if ($put["id_usuario"] != '' && $put["fecha"] != '' && $put["ubic_lat"] != '' && $put["ubic_lon"] != '' && $put["id_sucursal"]!='') {


                        if ($_FILES["foto"]["size"] != 0) {


                            $vowels = array("-", " ", ":");
                            $fecha_concat = str_replace($vowels, "", $put["fecha"]);

                            $div=explode(".",$_FILES["foto"]["name"]);

                            $_FILES["foto"]["name"] = $fecha_concat.'.'.$div[count($div)-1];

                            $ruta = './carga_fotos/' . $put["id_usuario"];
                            if(!file_exists($ruta))
                                mkdir($ruta, 0777, true);
                            /*
                            if (mkdir($ruta, 0777, true)) {

                            }else {
                                // Set the response and exit
                                $this->response([
                                    'status' => 400,
                                    'message' => 'Error carpeta para url foto ya existe'
                                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
                            }
*/
                            $this->load->helper(array('form', 'url'));
                            $config['upload_path'] = $ruta;
                            $config['allowed_types'] = '*';
                            $config['overwrite'] = TRUE;
                            $config['max_size'] = '5000';
                            $config['max_width'] = '5024';
                            $config['max_height'] = '5768';

                            $this->load->library('upload', $config);

                            if (!$this->upload->do_upload('foto')) {
                                $error = array('error' => $this->upload->display_errors());
                                print_r($error);
                            } else {
                                //$file_data = $this->upload->data();

                                $put['foto'] = $_FILES["foto"]["name"];

                            }

                        } else {
                            $put['foto'] = '';
                        }


                        if($id_rol==4)// promotor
                            $tabla="historial_pv";
                        else  if($id_rol==5)// vendedor
                            $tabla="historial_v";
                        else
                            $tabla="historial"; // promotor-vendedor

                            $resultado = $this->API_BD->historial_put($tabla,$put);

                        if ($resultado) {
                            // Set the response and exit
                            $this->response(['status' => 201,"tabla"=>$tabla,
                                'message' => 'Datos Guardados Correctamente',
                                'Datos' => $put
                            ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                        } else {
                            // Set the response and exit
                            $this->response([
                                'status' => 400,
                                'message' => 'Error al intentar guardar'
                            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
                        }

                    } else {
                        $this->response([
                            'status' => 401,
                            'message' => 'Los campos son obligatorios (excepto comentario)'
                        ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

                    }
                }else{
                    $this->response([
                        'status' => 401,
                        'message' => 'El estatus no existe'
                    ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

                }

            }else{
                $this->response([
                    'status' => 401,
                    'message' => 'El api key ingresado no coincide con el de usuario'
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

            }
            /*
        }else{
            $this->response([
                'status' => 404,
                'message' => 'Método desconocido'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

        }
            */
    }

    public function obtener_post()
    {
        // Users from a data store e.g. database

        $api_key=$this->post("api_key");

        // $agregar = $this->get('agregar');

        $id_usuario=$this->post("id_usuario");


        if($this->key_exists($api_key,$id_usuario)) {


                if ($this->post("obtengo")) {

                  //  $param["obtengo"]=$this->post("obtengo");
                    $respuesta=$this->API_BD->obtener($_POST);


                    if ($respuesta["status"]==200) {
                        $respuesta["message"]="Consulta satisfactoria";
                        // Set the response and exit
                        $this->response($respuesta, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                    } else {
                        // Set the response and exit
                        $this->response([
                            'status' => 400,
                            'message' => 'Error al intentar obtener datos'
                        ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
                    }

                } else {
                    $this->response([
                        'status' => 401,
                        'message' => 'No se encontraron todos los parámetros'
                    ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

                }

        }else{
            $this->response([
                'status' => 401,
                'message' => 'El api key ingresado no coincide con el de usuario'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

        }
        /*
    }else{
        $this->response([
            'status' => 404,
            'message' => 'Método desconocido'
        ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

    }
        */
    }


    private function key_exists($api_key,$id_usuario)
    {
        $resultado=$this->API_BD->key_exists($api_key,$id_usuario);
        if($resultado)
            return true;
        else
            return false;
    }

    private function existe_estatus($id_estatus)
    {
        $resultado=$this->API_BD->existe_estatus($id_estatus);
        if($resultado)
            return true;
        else
            return false;
    }


}
