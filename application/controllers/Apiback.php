<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model("API_BD");
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
    }
/*
    public function users_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['id' => 1, 'name' => 'John', 'email' => 'john@example.com', 'fact' => 'Loves coding'],
            ['id' => 2, 'name' => 'Jim', 'email' => 'jim@example.com', 'fact' => 'Developed on CodeIgniter'],
            ['id' => 3, 'name' => 'Jane', 'email' => 'jane@example.com', 'fact' => 'Lives in the USA', ['hobbies' => ['guitar', 'cycling']]],
        ];

        $id = $this->get('id');

        // If the id parameter doesn't exist return all the users

        if ($id === NULL)
        {
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($users)
            {
                // Set the response and exit
                $this->response($users, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No users were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }

        // Find and return a single record for a particular user.

        $id = (int) $id;

        // Validate the id.
        if ($id <= 0)
        {
            // Invalid id, set the response and exit.
            $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
        }

        // Get the user from the array, using the id as key for retrieval.
        // Usually a model is to be used for this.

        $user = NULL;

        if (!empty($users))
        {
            foreach ($users as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $user = $value;
                }
            }
        }

        if (!empty($user))
        {
            $this->set_response($user, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            $this->set_response([
                'status' => FALSE,
                'message' => 'User could not be found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
*/

   // private $api_key_master='ru41eYPqmBRdaOjPYcOF3oAf0fQQ3uIvExCHqPST';


    public function login_post()
    {

        $datos["usuario"]=htmlspecialchars($this->post("usuario"));
        $datos["pass"]=htmlspecialchars($this->post("pass"));

            $usuario=$this->API_BD->acceso_api($datos);
            // Check if the users data store contains users (in case the database result returns NULL)
            if ($usuario)
            {
                $estatus = $this->API_BD->obtengo_estatus_api('todos');
                if ($estatus) {
                    // Set the response and exit
                    $login_datos["usuario"]=$usuario;
                    $login_datos["estatus"]=$estatus;
                    $this->response($login_datos, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                }
                else
                {
                    // Set the response and exit
                    $this->response([
                        'status' => 500,
                        'message' => 'Error del servidor, intenta nuevamente'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => 401,
                    'message' => 'usuario o contraseña incorrectos'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
    }
/*
    public function usuarios_post()
    {
        // Users from a data store e.g. database

        $api_key=$this->post("api_key");

        if($api_key==$this->api_key_master) {

            $id = $this->get('id');


            // If the id parameter doesn't exist return all the users

            if ($id == null) {
                $usuarios = $this->API_BD->obtengo_usuario_api('todos');
                // Check if the users data store contains users (in case the database result returns NULL)
                if ($usuarios) {
                    // Set the response and exit
                    $this->response($usuarios, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron usuarios'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            } else {

                if ($id <= 0) {
                    // Invalid id, set the response and exit.
                    $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                } else {

                    $id = (int)$id;
                    $usuario = $this->API_BD->obtengo_usuario_api($id);

                    if ($usuario) {
                        // Set the response and exit
                        $this->response($usuario, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                    } else {
                        // Set the response and exit
                        $this->response([
                            'status' => FALSE,
                            'message' => 'El usuario no existe'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }

                }

            }

        }else{
            $this->response([
                'status' => 401,
                'message' => 'Api key no coincide'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

        }
    }


    public function estatus_post()
    {
        // Users from a data store e.g. database

        $api_key=$this->post("api_key");

        if($api_key==$this->api_key_master) {

            $id = $this->get('id');

            // If the id parameter doesn't exist return all the users

            if ($id == null) {
                $estatus = $this->API_BD->obtengo_estatus_api('todos');
                // Check if the users data store contains users (in case the database result returns NULL)
                if ($estatus) {
                    // Set the response and exit
                    $this->response($estatus, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                } else {
                    // Set the response and exit
                    $this->response([
                        'status' => FALSE,
                        'message' => 'No se encontraron datos'
                    ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                }
            } else {

                if ($id <= 0) {
                    // Invalid id, set the response and exit.
                    $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                } else {

                    $id = (int)$id;
                    $estatus = $this->API_BD->obtengo_estatus_api($id);

                    if ($estatus) {
                        // Set the response and exit
                        $this->response($estatus, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                    } else {
                        // Set the response and exit
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Estatus no existe'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }

                }

            }

        }else{
            $this->response([
                'status' => 401,
                'message' => 'Api key no coincide'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

        }
    }

*/
    public function agregar_post()
    {
        // Users from a data store e.g. database

        $api_key=$this->post("api_key");

       // $agregar = $this->get('agregar');
/*
        if($agregar==null) {
            if ($api_key == $this->api_key_master) {

                $id = $this->get('id');

                // If the id parameter doesn't exist return all the users

                if ($id == null) {
                    $historial = $this->API_BD->obtengo_historial_api('todos');
                    // Check if the users data store contains users (in case the database result returns NULL)
                    if ($historial) {
                        // Set the response and exit
                        $this->response($historial, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                    } else {
                        // Set the response and exit
                        $this->response([
                            'status' => FALSE,
                            'message' => 'No se encontraron datos'
                        ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                    }
                } else {

                    if ($id <= 0) {
                        // Invalid id, set the response and exit.
                        $this->response(NULL, REST_Controller::HTTP_BAD_REQUEST); // BAD_REQUEST (400) being the HTTP response code
                    } else {

                        $id = (int)$id;
                        $historial = $this->API_BD->obtengo_historial_api($id);

                        if ($historial) {
                            // Set the response and exit
                            $this->response($historial, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                        } else {
                            // Set the response and exit
                            $this->response([
                                'status' => FALSE,
                                'message' => 'El id no existe'
                            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
                        }

                    }

                }

            } else {
                $this->response([
                    'status' => 401,
                    'message' => 'Api key no coincide'
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

            }
        }elseif($agregar=='post'){
*/
            $id_usuario=$this->post("id_usuario");
            if($this->key_exists($api_key,$id_usuario)) {


                    if (!isset($_FILES["foto"]["size"]))
                        $_FILES["foto"]["size"] = 0;

                    $put["id_usuario"] = $this->post("id_usuario");
                    $put["id_estatus"] = $this->post("id_estatus");
                    $put["fecha"] = $this->post("fecha");
                    $put["ubic_lat"] = $this->post("ubic_lat");
                    $put["ubic_lon"] = $this->post("ubic_lon");
                    $put["foto"] = $this->post("foto");
                    $put["comentario"] = $this->post("comentario");
                    $put["empresa"] = $this->post("empresa");

                if($this->existe_estatus($put["id_estatus"])) {

                    if ($put["id_usuario"] != '' && $put["fecha"] != '' && $put["ubic_lat"] != '' && $put["ubic_lon"] != '' && $put["empresa"] != '') {


                        if ($_FILES["foto"]["size"] != 0) {


                            $vowels = array("-", " ", ":");
                            $fecha_concat = str_replace($vowels, "", $put["fecha"]);

                            $div=explode(".",$_FILES["foto"]["name"]);

                            $_FILES["foto"]["name"] = $fecha_concat.'.'.$div[count($div)-1];

                            $ruta = './carga_fotos/' . $put["id_usuario"];
                            if(!file_exists($ruta))
                                mkdir($ruta, 0777, true);
                            /*
                            if (mkdir($ruta, 0777, true)) {

                            }else {
                                // Set the response and exit
                                $this->response([
                                    'status' => 400,
                                    'message' => 'Error carpeta para url foto ya existe'
                                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
                            }
*/
                            $this->load->helper(array('form', 'url'));
                            $config['upload_path'] = $ruta;
                            $config['allowed_types'] = '*';
                            $config['overwrite'] = TRUE;
                            $config['max_size'] = '5000';
                            $config['max_width'] = '5024';
                            $config['max_height'] = '5768';

                            $this->load->library('upload', $config);

                            if (!$this->upload->do_upload('foto')) {
                                $error = array('error' => $this->upload->display_errors());
                                print_r($error);
                            } else {
                                //$file_data = $this->upload->data();

                                $put['foto'] = $_FILES["foto"]["name"];

                            }

                        } else {
                            $put['foto'] = '';
                        }
                        $resultado = $this->API_BD->historial_put($put);

                        if ($resultado) {
                            // Set the response and exit
                            $this->response(['status' => 201,
                                'message' => 'Datos Guardados Correctamente',
                                'Datos' => $put
                            ], REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
                        } else {
                            // Set the response and exit
                            $this->response([
                                'status' => 400,
                                'message' => 'Error al intentar guardar'
                            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code
                        }

                    } else {
                        $this->response([
                            'status' => 401,
                            'message' => 'Los campos son obligatorios (excepto foto y comentario)'
                        ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

                    }
                }else{
                    $this->response([
                        'status' => 401,
                        'message' => 'El estatus no existe'
                    ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

                }

            }else{
                $this->response([
                    'status' => 401,
                    'message' => 'El api key ingresado no coincide con el de usuario'
                ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

            }
            /*
        }else{
            $this->response([
                'status' => 404,
                'message' => 'Método desconocido'
            ], REST_Controller::HTTP_BAD_REQUEST); // NOT_FOUND (404) being the HTTP response code

        }
            */
    }




    private function key_exists($api_key,$id_usuario)
    {
        $resultado=$this->API_BD->key_exists($api_key,$id_usuario);
        if($resultado)
            return true;
        else
            return false;
    }

    private function existe_estatus($id_estatus)
    {
        $resultado=$this->API_BD->existe_estatus($id_estatus);
        if($resultado)
            return true;
        else
            return false;
    }


}
