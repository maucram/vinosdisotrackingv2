<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Historial extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("BaseDatos");

    }

    public function _remap($metodo, $parametros = array())
    {
        if($this->session->userdata('acceso_mapvd') &&  ($this->session->userdata('id_privilegio')==1 || $this->session->userdata('id_privilegio')==2) ) { // cliente y super usuario) {

            if($metodo=="promotor"){
                $this->promotor($parametros);
            }else if($metodo=="promotor-vendedor"){
                $this->promotor_vendedor($parametros);
            }else if($metodo=="vendedor"){
                $this->vendedor($parametros);
            }else{
                $this->promotor($parametros);
            }

        }else
            $this->load->view('loginView');
    }

    /*   -------------------------------------------   Historial  ------------------------------------------- */

    /**
     *
     */

    private function index($m_titulo){

        $cabeza["usuario"] = $this->session->userdata('usuario');
        $cabeza["privilegio"] = $this->session->userdata('privilegio');
        $cabeza["id_privilegio"] = $this->session->userdata('id_privilegio');
        $cabeza["foto_perfil"] = ($this->session->userdata('foto_perfil') != '') ? base_url() . 'fotos_perfil/' . $this->session->userdata('id') . '/' . $this->session->userdata('foto_perfil')
            : base_url() . 'plantilla/assets/images/users/avatar.png';
        $cabeza["id"] = $this->session->userdata('id');
        $cabeza["titulo"] = $m_titulo;
        $cabeza["m_titulo"] = 'Historial';

        $vista["metodo"] = $m_titulo;

        $this->load->view('cabeza', $cabeza);
        $this->load->view('historialView', $vista);
    }

    public function promotor($parametro){

        if(isset($parametro[0])) {
            switch ($parametro[0]) {
                case 'obtengo_usuarios':
                    $this->obtengo_usuarios(4);
                    break;
                case 'cargo_datatable':
                    $this->cargo_datatable('historial_promotor');
                    break;
                case 'busco_direcciones_sin_procesar':
                    $this->busco_direcciones_sin_procesar("historial_pv");
                    break;
                case 'guardo_direcciones':
                    $this->guardo_direcciones("historial_pv");
                    break;
                default:
                $this->index("Promotor");
            }
        }else{
            $this->index("Promotor");
        }
    }

    public function promotor_vendedor($parametro){

        if(isset($parametro[0])) {
            switch ($parametro[0]) {
                case 'obtengo_usuarios':
                    $this->obtengo_usuarios(0);
                    break;
                case 'cargo_datatable':
                    $this->cargo_datatable('historial_promotor_vendedor');
                    break;
                case 'busco_direcciones_sin_procesar':
                    $this->busco_direcciones_sin_procesar("historial");
                    break;
                case 'guardo_direcciones':
                    $this->guardo_direcciones("historial");
                    break;
                default:
                    $this->index("Promotor-Vendedor");
            }
        }else{
            $this->index("Promotor-Vendedor");
        }
    }


    public function vendedor($parametro){

        if(isset($parametro[0])) {
            switch ($parametro[0]) {
                case 'obtengo_usuarios':
                    $this->obtengo_usuarios(0);
                    break;
                case 'cargo_datatable':
                    $this->cargo_datatable('historial_vendedor');
                    break;
                case 'busco_direcciones_sin_procesar':
                    $this->busco_direcciones_sin_procesar("historial_v");
                    break;
                case 'guardo_direcciones':
                    $this->guardo_direcciones("historial_v");
                    break;
                default:
                    $this->index("Vendedor");
            }
        }else{
            $this->index("Vendedor");
        }
    }

    public function obtengo_usuarios($id_rol){
        if($_POST["search"]){
           echo json_encode($this->BaseDatos->obtengo_usuarios_text($id_rol));
        }
    }


    public function cargo_datatable($tabla){

        if($this->session->userdata('id_privilegio')==2)
            $id=$this->session->userdata('id');
        else
            $id=0;

        echo $this->BaseDatos->cargo_datatable($tabla,$id);

    }


    public function busco_direcciones_sin_procesar($tabla){
        if ($this->input->is_ajax_request()) {
            echo $this->BaseDatos->busco_direcciones_sin_procesar($tabla);
        }else echo false;
    }

    public function guardo_direcciones($tabla){
        if($_POST["direcciones"])
           $this->BaseDatos->guardo_direcciones($_POST["direcciones"],$tabla);
    }
}
?>
