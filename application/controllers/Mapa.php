<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapa extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("BaseDatos");

    }

    public function _remap($metodo, $parametros = array())
    {
        if ($this->session->userdata('acceso_mapvd')) {
            switch ($metodo) {

                case 'promotor':
                    $this->promotor($parametros);
                    break;
                case 'promotor-vendedor':
                    $this->promotor_vendedor($parametros);
                    break;
                case 'vendedor':
                    $this->vendedor($parametros);
                    break;
                case 'cerrar_sesion':
                    $this->cerrar_sesion();
                    break;

                default:
                    $this->promotor($parametros);
            }
        } else
            $this->load->view('loginView');
    }

    /*   -------------------------------------------   mapas ------------------------------------------- */

    /**
     *
     */

    public function index($titulo)
    {
        $cabeza["usuario"] = $this->session->userdata('usuario');
        $cabeza["privilegio"] = $this->session->userdata('privilegio');
        $cabeza["id_privilegio"] = $this->session->userdata('id_privilegio');
        $cabeza["foto_perfil"]=($this->session->userdata('foto_perfil')!='')?base_url().'fotos_perfil/'.$this->session->userdata('id').'/'.$this->session->userdata('foto_perfil')
            :base_url().'plantilla/assets/images/users/avatar.png';
        $cabeza["id"] = $this->session->userdata('id');
        $cabeza["m_titulo"] = 'Mapa';
        $cabeza["titulo"] = $titulo;

        $vista["metodo"] = $titulo;
        $this->load->view('cabeza', $cabeza);
        $this->load->view('mapaView',$vista);

    }


    public function promotor($parametros){

        if(isset($parametros[0])) {
            switch ($parametros[0]) {

                case 'cargo_datos_ubicaciones':
                    $this->cargo_datos_ubicaciones(4);
                    break;
                case 'obtengo_grafica':
                    $this->obtengo_grafica(4);
                    break;
                default:
                    $this->index("Promotor");
            }
        }else
            $this->index("Promotor");
    }


    public function promotor_vendedor($parametros){
          if(isset($parametros[0])) {
                switch ($parametros[0]) {

                    case 'cargo_datos_ubicaciones':
                        $this->cargo_datos_ubicaciones(0);
                        break;
                    case 'obtengo_grafica':
                        $this->obtengo_grafica(0);
                        break;
                    default:
                        $this->index("Promotor-Vendedor");
                }
          }else
              $this->index("Promotor-Vendedor");
    }



    public function vendedor($parametros){
        if(isset($parametros[0])) {
            switch ($parametros[0]) {

                case 'cargo_datos_ubicaciones':
                    $this->cargo_datos_ubicaciones(5);
                    break;
                case 'obtengo_grafica':
                    $this->obtengo_grafica(5);
                    break;
                default:
                    $this->index("Vendedor");
            }
        }else
            $this->index("Vendedor");
    }


    public function obtengo_grafica($id_rol)
    {
           if($this->input->is_ajax_request()) {
               $resultado = $this->BaseDatos->obtengo_datos_grafica($_POST,$id_rol);

               echo $resultado;
            }else
                echo json_encode(array("status"=>400,"mensaje"=>"No se utilizó ajax"));

    }



    public function cargo_datos_ubicaciones($id_rol)
    {
        if ($_POST["id_persona"]) {
            $resultado = $this->BaseDatos->cargo_datos_ubicaciones($_POST,$id_rol);
            if ($resultado)
                echo json_encode($resultado);
            else
                echo -1;
        }
    }

    public function cerrar_sesion()
    {
        $this->session->sess_destroy();
        header('Location:' . base_url() . 'login');
    }


    public function descargar_excel_tickets($id_tickets)
    {
        if (count($id_tickets)>0) {

            $this->load->library('PHPExcel');

            $objPHPExcel = new PHPExcel();
            $objSheet = $objPHPExcel->setActiveSheetIndex(0);
            $objPHPExcel->getActiveSheet()->setTitle("Tickets");
            $fila=4;

            $estilo_head = array(
                'font'  => array(
                    'bold'  => false,
                    'size'  => 13,
                    'name'  => 'Verdana',
                    'color' => array('rgb' => 'ffffff')
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '2980B9')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )

            );
            $estilo_head2 = array(
                'font'  => array(
                    'bold'  => false,
                    'size'  => 13,
                    'name'  => 'Verdana',
                    'color' => array('rgb' => 'ffffff')
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '656d78')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )

            );

            $estilo_tickets = array(
                'font'  => array(
                    'bold'  => true,
                    'size'  => 15,
                    'name'  => 'Verdana',
                )

            );
            $estilo_fecha = array(
                'font'  => array(
                    'bold'  => true,
                    'size'  => 13,
                    'name'  => 'Calibri',
                )

            );

            $estilo_color1 = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'EAF2F8')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )

            );
            $estilo_color2 = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'BADDF4')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )

            );

            $estilo_color3 = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'EAEDED')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )

            );
            $estilo_color4 = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'D5DBDB')
                ),
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )

            );
            $gate_color=true;

            $objPHPExcel->getActiveSheet()->getStyle("B3:J3")->applyFromArray($estilo_head);
            $objPHPExcel->getActiveSheet()->getStyle("K3:N3")->applyFromArray($estilo_head2);
            $objPHPExcel->getActiveSheet()->getStyle("A1")->applyFromArray($estilo_tickets);
            $objPHPExcel->getActiveSheet()->getStyle("A2")->applyFromArray($estilo_fecha);
            $objSheet->setCellValue("A1", "Tickets");
            $objSheet->setCellValue("A2", "Fecha descarga: ".date("d/m/Y"));
            $objSheet->setCellValue("B3", "Id");
            $objSheet->setCellValue("C3", "Fecha");
            $objSheet->setCellValue("D3", "Usuario");
            $objSheet->setCellValue("E3", "Proyecto");
            $objSheet->setCellValue("F3", "Menú");
            $objSheet->setCellValue("G3", "Módulo");
            $objSheet->setCellValue("H3", "Concepto");
            $objSheet->setCellValue("I3", "Descripción");
            $objSheet->setCellValue("J3", "Estatus");

            $objSheet->setCellValue("K3", "Fecha");
            $objSheet->setCellValue("L3", "Estatus");
            $objSheet->setCellValue("M3", "Usuario");
            $objSheet->setCellValue("N3", "Comentarios");

                for($i=0;$i<count($id_tickets);$i++){

                    $ticket= $this->BaseDatos->obtengo_ticketId($id_tickets[$i]);

                    if ($gate_color) {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $fila . ":J" . $fila)->applyFromArray($estilo_color1);
                        $objPHPExcel->getActiveSheet()->getStyle("K" . $fila . ":N" . $fila)->applyFromArray($estilo_color3);
                        $gate_color = false;
                    } else {
                        $objPHPExcel->getActiveSheet()->getStyle("B" . $fila . ":J" . $fila)->applyFromArray($estilo_color2);
                        $objPHPExcel->getActiveSheet()->getStyle("K" . $fila . ":N" . $fila)->applyFromArray($estilo_color4);
                        $gate_color = true;
                    }

                   // $fecha_aux = new DateTime( $fila);
                    //$fecha = $fecha_aux->format("d/m/Y");

                    $objSheet->setCellValue("B" . $fila, $ticket->id);
                    $objSheet->setCellValue("C" . $fila,$ticket->fecha );
                    $objSheet->setCellValue("D" . $fila,$ticket->nom_usuario );
                    $objSheet->setCellValue("E" . $fila,$ticket->proyecto );
                    $objSheet->setCellValue("F" . $fila,$ticket->menu );
                    $objSheet->setCellValue("G" . $fila, $ticket->modulo );
                    $objSheet->setCellValue("H" . $fila,$ticket->concepto );
                    $objSheet->setCellValue("I" . $fila,$ticket->descripcion );
                    $objSheet->setCellValue("J" . $fila, $ticket->estatus);


                    $estatus_tickets = $this->BaseDatos->obtengo_datos_estatus_ticket($id_tickets[$i]);

                    $salto_primera=false;
                    foreach ($estatus_tickets as $est_ticket){


                        if($salto_primera){
                            $objSheet->setCellValue("B" . $fila, $ticket->id);
                            $objSheet->setCellValue("C" . $fila,$ticket->fecha );
                            $objSheet->setCellValue("D" . $fila,$ticket->nom_usuario );
                            $objSheet->setCellValue("E" . $fila,$ticket->proyecto );
                            $objSheet->setCellValue("F" . $fila,$ticket->menu );
                            $objSheet->setCellValue("G" . $fila, $ticket->modulo );
                            $objSheet->setCellValue("H" . $fila,$ticket->concepto );
                            $objSheet->setCellValue("I" . $fila,$ticket->descripcion );
                            $objSheet->setCellValue("J" . $fila, $ticket->estatus);

                            if ($gate_color) {
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $fila . ":J" . $fila)->applyFromArray($estilo_color1);
                                $objPHPExcel->getActiveSheet()->getStyle("K" . $fila . ":N" . $fila)->applyFromArray($estilo_color3);
                                $gate_color = false;
                            } else {
                                $objPHPExcel->getActiveSheet()->getStyle("B" . $fila . ":J" . $fila)->applyFromArray($estilo_color2);
                                $objPHPExcel->getActiveSheet()->getStyle("K" . $fila . ":N" . $fila)->applyFromArray($estilo_color4);
                                $gate_color = true;
                            }

                        }

                        $fecha_aux = new DateTime( $est_ticket->fecha);
                        $fecha = $fecha_aux->format("d/m/Y");
                        $objSheet->setCellValue("K" . $fila, $fecha);
                        $objSheet->setCellValue("L" . $fila,$est_ticket->estatus );
                        $objSheet->setCellValue("M" . $fila,$est_ticket->nom_usuario );
                        $objSheet->setCellValue("N" . $fila,$est_ticket->comentarios );

                        $fila++;
                        $salto_primera=true;
                    }
                    if(!$estatus_tickets)
                        $fila++;
                }

            for ($col = 'A'; $col <= 'N'; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
            }

            $fileName = 'Tickets' . date('d/m/Y'); //save our workbook as this file name

            // Redirect output to a client’s web browser (Excel5)
            // header('Content-Type: application/vnd.ms-excel;charset=utf-8');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="' . $fileName . '".xlsx');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            //header('Cache-Control: max-age=1');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
            $objWriter->save('php://output');

            //die(json_encode($response));
            exit;
        }
    }


    public function busco_proyectos_usuario()
    {
        if ($_POST["id_usuario"]) {
            $resultado = $this->BaseDatos->busco_proyectos_usuario($_POST["id_usuario"]);

            if($resultado)
            echo $resultado->id_proyecto;
            else echo -1;
        }
    }



}

?>
