<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("LoginModel");

    }

    public function _remap($metodo, $parametros = array())
    {
        switch ($metodo)
        {
            case 'acceso': $this->login();
                break;
            default: $this->index();
        }
    }

    public function index()
    {
        if($this->session->userdata('acceso_mapvd')) {

            redirect(base_url());
        } else {
            $this->load->view('loginView');
        }
    }

    public function login()
    {
        if($this->input->post())
        {
            $id = $this->LoginModel->acceso($_POST);
            if($id)
            {
                $usuario = $this->LoginModel->obtengo_datos_usuario($id);
                if($usuario) {
                    if($usuario->privilegios==1)$privilegio='Administrador';else if($usuario->privilegios==2)$privilegio="Cliente";else $privilegio='Promotor';

                    $usuarioData = array("id" => $usuario->id, "id_privilegio"=>$usuario->privilegios,"foto_perfil"=>$usuario->foto_perfil,
                        "privilegio"=>$privilegio, "usuario" => $usuario->usuario, "acceso_mapvd" => true);
                    $this->session->set_userdata($usuarioData);
                    redirect(base_url());
                }else {
                    $error_pri = true;
                    $this->load->view('loginView', compact("error_pri"));
                }
            }else {
                $error = true;
                $this->load->view('loginView', compact("error"));
            }
        }
    }
}
?>
