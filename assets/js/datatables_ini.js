var datatable_catalogos;
var columnas;
var  controlador='configuracion',metodo='',tabladatos,tabladatoshistorial;


    switch (g_titulo) {
        case 'Usuarios':

            metodo = '/usuarios';

            columnas=[
                {"data": "id"},
                {
                    "data": "foto_perfil",
                    'searchable': false,
                    'orderable': false,
                    'className': 'text-center',
                    "render": function (data, type, full, meta) {
                        var url=base_url+'fotos_perfil/'+full.id+'/'+data;
                        if(data=='')
                            url=base_url+'plantilla/assets/images/users/avatar.png';
                        return '<img width="100px" src="'+url+'">';
                    }
                },
                {"data":"usuario"},
                {"data":"correo"},
                {"data":"pass"},
                {"data":"telefono"},
                {"data":"direccion"},
                {"data":"tarjeta_dispersion"},
                {
                    "data": "fecha_alta",
                    "render": function (data, type, full, meta) {
                        var fecha_ddmmyyyy=mau.fechas.formato(data,"dd/mm/yyyy");
                        var fecha_yyyymmdd=mau.fechas.formato(data,"yyyymmdd");


                        return '<i class="hidden">'+fecha_yyyymmdd+'</i>'+fecha_ddmmyyyy;
                    }
                },
                {
                    "data": "sueldo",
                    "render": function (data, type, full, meta) {
                        return "$"+mau.numero.formato_moneda(data);
                    }
                },
                {"data": function (data,index) {
                        var rol=(data.privilegios==1)?'Administrador':(data.privilegios==2)?'Cliente':(data.privilegios==4)?'Promotor':(data.privilegios==5)?"Vendedor":"Promotor-Vendedor";
                        return rol;
                    }
                },
                {"data": function (data,index) {

                        var url=base_url+'fotos_perfil/'+data.id+'/'+data.foto_perfil;
                        if(data.foto_perfil=='')
                            url=base_url+'plantilla/assets/images/users/avatar.png';
                            var fecha=mau.fechas.formato(data.fecha_alta,"yyyy-mm-dd");

                        return '<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"' +
                            'data-id="'+data.id+'" data-foto="'+url+'" data-fecha_alta="'+fecha+'"  data-usuario="'+data.usuario+'"' +
                            'data-pass="'+data.pass+'" data-correo="'+data.correo+'" data-telefono="'+data.telefono+'" data-direccion="'+data.direccion+'" data-tarjeta_dispersion="'+data.tarjeta_dispersion+'" ' +
                            ' data-id_privilegio="'+data.privilegios+'" data-sueldo="'+data.sueldo+'"></i>'

                    },'className': 'text-center'

                }
            ];


            break;

        case 'Cadena':
            metodo = '/cadena';
            columnas=[{"data": "id"},
                {"data":"empresa"},
                {"data": function (data,index) {

                        return '<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-empresa="'+data.empresa+'" data-id="'+data.id+'"></i>';

                    }
                }
            ];
            break;

        case 'Sucursal':
            metodo = '/sucursal';
            columnas=[{"data": "id"},
                {"data":"empresa"},
                {"data":"sucursal"},
                {"data": function (data,index) {

                            var edit=(data.status_empresa==0)?"Cadena deshabilitada":'<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                                'data-id_empresa="'+data.id_empresa+'" data-sucursal="'+data.sucursal+'" data-id="'+data.id+'"></i>';
                        return edit;

                    }
                }
            ];
            break;
        case 'Cliente':
            metodo = '/cliente';
            columnas=[{"data": "id"},
                {"data":"usuario"},
                {"data":"cliente"},
                {"data": function (data,index) {



                        var edit= (data.status_usuario==0)?"Cliente deshabilitado":'<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-id_usuario="'+data.id_usuario+'" data-cliente="'+data.cliente+'" data-id="'+data.id+'"></i>';

                        return edit;



                    }
                }
            ];
            break;
        case 'ClientePromotor':
            metodo = '/cliente-promotor';
            columnas=[{"data": "id"},
                {"data":"usuario_cliente"},
                {"data":"cliente"},
                {"data": function (data,index) {
                        var rol=(data.id_rol==1)?'Administrador':(data.id_rol==2)?'Cliente':(data.id_rol==4)?'Promotor':(data.id_rol==5)?"Vendedor":"Promotor-Vendedor";
                        return rol;
                    }
                },
                {"data":"promotor"},
                {"data": function (data,index) {

                        var edit= (data.status_usuario==0)?"Cliente deshabilitado":'<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-id_promotor="'+data.id_promotor+'" data-id_rol="'+data.id_rol+'" data-id_usuario_cliente="'+data.id_usuario_cliente+'" data-id_cliente="'+data.id_cliente+'" data-id="'+data.id+'"></i>';
                        return edit;

                    }
                }
            ];
            break;


        case 'Estatus':
            metodo = '/estatus';
            columnas=[{"data": "id"},
                {"data":"estatus"},
                {"data": function (data,index) {

                        return '<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-estatus="'+data.estatus+'" data-id="'+data.id+'"></i>';

                    }
                }
            ];
            break;

        case 'Producto':
            metodo = '/producto';
            columnas=[{"data": "id"},
                {"data":"cliente"},
                {"data":"marca"},
                {"data":"producto"},
                {"data": function (data,index) {

                        var edit= (data.status_usuario==0)?"Cliente deshabilitado":'<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-id_cliente="'+data.id_cliente+'" data-id_marca="'+data.id_marca+'" data-producto="'+data.producto+'" data-id="'+data.id+'"></i>';

                        return edit;

                    }
                }
            ];
            break;

        case 'Lugar':
            metodo = '/lugar';
            columnas=[{"data": "id"},
                {"data":"lugar"},
                {"data": function (data,index) {

                        return '<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-lugar="'+data.lugar+'" data-id="'+data.id+'"></i>';

                    }
                }
            ];
            break;

        case 'Posición':
            metodo = '/posicion';
            columnas=[{"data": "id"},
                {"data":"lugar"},
                {"data":"posicion"},
                {"data": function (data,index) {

                        var edit= (data.status_lugar==0)?"Lugar deshabilitado":'<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-id_lugar="'+data.id_lugar+'" data-posicion="'+data.posicion+'" data-id="'+data.id+'"></i>';

                        return edit;

                    }
                }
            ];
            break;



        case 'Historial':
            controlador = 'historial';

            columnas=[{"data": "id"},
                {"data":"usuario"},
                {"data":"fecha"},
                {"data": function (data,index) {

                       var ubicacion={lat:parseFloat(data.lat),lng:parseFloat(data.lng)};

                        geocoder.geocode({'location': ubicacion}, function(results, status) {
                            if (status === 'OK') {
                                if (results[0]) {

                                    return results[0].formatted_address;

                                } else {
                                    window.alert('Resultados no encontrados');
                                }
                            } else {

                                if(status=="ZERO_RESULTS")
                                    window.alert("La ubicación no se puede mostrar porque los datos fueron capturados con el GPS apagado");
                                else
                                    window.alert('Error map: ' + status);
                            }
                        });

                    }
                },
                {"data": function (data,index) {

                        return '<i class="fa fa-pencil editar_estatus fa-2x" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-estatus="'+data.estatus+'" data-id="'+data.id+'"></i>';

                    }
                },
                {"data":"estatus"},
                {"data":"comentario"},
                {"data":"empresa"}
            ];


            break;
    }


    function asigno_attr_tr(row,data,index){

        switch (g_titulo){
            case "Sucursal":
                if(data.status_empresa==0)
                    $(row).css("color","#d8cbcb");
                break;
            case "Cliente":
                if(data.status_usuario==0)
                    $(row).css("color","#d8cbcb");
                break;
            case "ClientePromotor":
                if(data.status_usuario==0)
                    $(row).css("color","#d8cbcb");
                break;
            case "Producto":
                if(data.status_usuario==0)
                    $(row).css("color","#d8cbcb");
                break;
            case "Posición":
                if(data.status_lugar==0)
                    $(row).css("color","#d8cbcb");
                break;
        }


    }

//$(document).ready(function() {

    tabladatos=$('.data_table_configuracion').DataTable({
        "oLanguage": {
            "sUrl": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
        },
        "scrollY": "60vh",
        "scrollX": '100%',
        "scrollCollapse": true,
        "pageLength": 100,
        "order": [[ 0, "desc" ]],
        "ajax": base_url +controlador+metodo+'/cargo_datatable',

        columns:columnas,
        "createdRow": function ( row, data, index ) {
            if(data.status==0)
                $(row).addClass('tickets-eliminado');

            asigno_attr_tr(row,data,index);


        },
        "initComplete": function( settings, json ) {
        //   cargo_funciones();
        }
    });

    tabladatoshistorial=  $('.data_table_historial').DataTable({
        "oLanguage": {
            "sUrl": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
        },
        "pageLength": 100,
        "scrollY": "60vh",
        "scrollX": '100%',
        "scrollCollapse": true,
        "order": [[ 0, "asc" ]],
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: { orthogonal: 'export' }
            },
            {
                extend: 'excelHtml5',
                filename: function(){
                    var f = new Date();
                    var dia=(f.getDate()<10)?'0'+f.getDate():f.getDate();
                    var mes=((f.getMonth() +1)<10)?'0'+(f.getMonth() +1):(f.getMonth() +1);
                    var year=f.getFullYear();
                    var fecha =  dia+ "-" + mes + "-" +year;

                    return 'VDPromotor_' +$("#mapa_usuario option:selected").text()+fecha;
                },
                exportOptions: { orthogonal: 'export' }
            },
            {
                extend: 'pdfHtml5',
                filename: function(){
                    var f = new Date();
                    var dia=(f.getDate()<10)?'0'+f.getDate():f.getDate();
                    var mes=((f.getMonth() +1)<10)?'0'+(f.getMonth() +1):(f.getMonth() +1);
                    var year=f.getFullYear();
                    var fecha =  dia+ "-" + mes + "-" +year;

                    return 'VDPromotor_' +$("#mapa_usuario option:selected").text()+fecha;
                },
                exportOptions: { orthogonal: 'export' }
            }
        ]

    });

