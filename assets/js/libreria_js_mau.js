var mau=new Object();

mau.fechas=new Mau_fechas();
function Mau_fechas() {
    this.es_bisiesto=function (year) {
        if(new Date(year, 1, 29).getMonth() == 1)
            return true;
        else
            return false;
    }
    this.es_bisiesto_entre_fechas=function (fecha_ini,fecha_fin,orden) {
        var separo_fecha_ini,separo_fecha_fin,dia_ini,dia_fin,mes_ini,mes_fin,year_ini,year_fin,bisiesto=false;

        separo_fecha_ini=fecha_ini.split(/-|\//);
        separo_fecha_fin=fecha_fin.split(/-|\//);

        dia_ini=parseInt(separo_fecha_ini[orden[0]]);
        mes_ini=parseInt(separo_fecha_ini[orden[1]]);
        year_ini=parseInt(separo_fecha_ini[orden[2]]);

        dia_fin=parseInt(separo_fecha_fin[orden[0]]);
        mes_fin=parseInt(separo_fecha_fin[orden[1]]);
        year_fin=parseInt(separo_fecha_fin[orden[2]]);

        if(dia_ini== dia_fin && mes_ini==mes_fin && year_ini==(year_fin-1)){
            var diff = (new Date(fecha_fin).getTime()) - (new Date(fecha_ini).getTime());
            diff = (diff / (1000 * 60 * 60 * 24));

            if(diff==366)
                bisiesto=true;
        }

        return bisiesto;
    }

    this.fecha_hoy=function (formato) {
        var f = new Date();

        var mes=((f.getMonth() +1)<10)?"0"+(f.getMonth() +1):(f.getMonth() +1);

        var dia=(f.getDate()<10)?"0"+f.getDate():f.getDate();

        var year=f.getFullYear();

        if(formato=='dd/mm/yyyy'){
            return dia+ "/" +mes  + "/"+year;
        }else  if(formato=='yyyy/mm/dd'){
            return year+ "/" +mes  + "/"+dia;
        }
    }

    this.inc_dis_fecha=function(fecha,inc_dis,en) {

        var f = new Date(fecha),mas_menos,dia,mes;

        if(inc_dis=="+")
            mas_menos=1;
        else
            mas_menos=-1;

        if(en=="dia"){
            f.setDate(f.getDate()+mas_menos);
        }

        dia=(f.getDate()<10)?"0"+f.getDate():f.getDate();
        mes=((f.getMonth() + 1)<10)?"0"+(f.getMonth() + 1):(f.getMonth() + 1);

        var retorno=[];
        retorno["dmy"]=dia + '/' +mes + '/' + f.getFullYear();
        retorno["ymd"]=f.getFullYear() + '/' +mes + '/' + dia;

        return retorno;
    }

    this.formato=function (fecha,formato) {
            var fecha_div=fecha.split(/[^1234567890]+/i);

            var dia=(fecha_div[0].length==2)?fecha_div[0]:fecha_div[2];
            var mes=fecha_div[1];
            var year=(fecha_div[0].length==4)?fecha_div[0]:fecha_div[2];

        if(formato=='dd/mm/yyyy'){
            return dia+ "/" +mes  + "/"+year;
        }else  if(formato=='yyyy/mm/dd'){
            return year+ "/" +mes  + "/"+dia;
        }else  if(formato=='yyyy-mm-dd'){
            return year+ "-" +mes  + "-"+dia;
        }else  if(formato=='dd-mm-yyyy'){
            return dia+ "-" +mes  + "-"+year;
        }else  if(formato=='yyyymmdd'){
            return year+mes+dia;
        }
    }



}
mau.numero=new Mau_numero();
function Mau_numero() {

    this.formato_moneda=function(numero){
        if(!isNaN(numero) && numero!=='') {
            return new Intl.NumberFormat('en-US', {

                minimumFractionDigits: 2,
                maximumFractionDigits: 2
            }).format(numero);
        }else if(numero.indexOf(",")!=-1 || numero.indexOf("$")!=-1)
            return numero;
        else
            return 0;
    }

    this.formato_numero=function(numero){


        if(numero.indexOf(",")!=-1 || numero.indexOf("$")!=-1)
            while (numero.indexOf(",")!=-1 || numero.indexOf("$")!=-1 || numero.indexOf(" ")!=-1) {
                numero = numero.replace(",", "");
                numero = numero.replace("$", "");
                numero = numero.replace(" ", "");
            }


        if(!isNaN(numero) && numero!=='')
            return parseFloat(numero);
        else if(numero=='.' || numero=='-' || numero.indexOf("/")!=-1)
            return numero;
        else
            return '';

    }

}
mau.texto=new Mau_texto();
function Mau_texto() {
    this.filtro_palabra=function(palabra) {
        return palabra
            .normalize('NFD')
            .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,"$1")
            .normalize().toLowerCase().trim();
    }

}

mau.image = new Image();
mau.url=new Mau_url();
function Mau_url() {

    this.img_a_base64=function (url, callback, outputFormat){

        mau.image.crossOrigin = 'Anonymous';
        mau.image.src = url;
        mau.image.onload = function(){
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            canvas.height = this.height;
            canvas.width = this.width;
            ctx.drawImage(this,0,0);
            var dataURL = canvas.toDataURL();
            dataURL=//dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
            callback(dataURL);
            canvas = null;
            //return dataURL;
        };



    }

}


mau.input_file=new Mau_input_file();

function Mau_input_file() {

    this.base64m=function (selector) {
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            document.getElementById(selector.id_input_file).addEventListener('change',
                function(evt) {
                    var files = evt.target.files;
                    var file = files[0];

                    if (files && file && file.size <= 2000000) {
                        var reader = new FileReader();

                        reader.onload = function (readerEvt) {
                            var binaryString = readerEvt.target.result;
                            //  console.log(btoa(binaryString));
                            $(selector.id_imagen).val(binaryString);
                            $(selector.id_mostrar_imagen).prop("src", binaryString).removeClass(selector.remover_clase);
                        };

                        reader.readAsDataURL(file);
                    } else if (file.size > 2000000) {
                        $(selector.id_input_file).val("");
                        selector.alerta;
                    }


                }, false);
        } else {
            alert('The File APIs are not fully supported in this browser.');
        }

    }

    this.base64= function(file,selector) {

        if (file.size <= 2000000) {
            var reader = new FileReader();

            reader.onload = function (readerEvt) {
                var binaryString = readerEvt.target.result;
                //  console.log(btoa(binaryString));
                $(selector.id_imagen).val(binaryString);
                $(selector.id_mostrar_imagen).prop("src", binaryString);
                $(selector.id_mostrar_imagen).parent().removeClass(selector.remover_clase);
            };

            reader.readAsDataURL(file);
        } else if (file.size > 2000000) {
            $(selector.id_input_file).val("");
            $(selector.id_mostrar_imagen).prop("src", "");
            $(selector.id_mostrar_imagen).parent().addClass(selector.remover_clase);

            Swal.fire({
                title: '¡Tamaño imagen superado!',
                text: 'Ingresa una imagen menor o igual a 2MB.',
                type: 'warning',
                cancelButtonText: "Cerrar",
                confirmButtonColor:"#DB244B",
                focusConfirm: false,
                cancelButtonColor:"#9f909b"
            });

        }


    }

}

// valdiar curp

//Función para validar una CURP
function curpValida(curp) {
    var re = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/,
        validado = curp.match(re);

    if (!validado)  //Coincide con el formato general?
        return false;

    //Validar que coincida el dígito verificador
    function digitoVerificador(curp17) {
        //Fuente https://consultas.curp.gob.mx/CurpSP/
        var diccionario  = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ",
            lngSuma      = 0.0,
            lngDigito    = 0.0;
        for(var i=0; i<17; i++)
            lngSuma = lngSuma + diccionario.indexOf(curp17.charAt(i)) * (18 - i);
        lngDigito = 10 - lngSuma % 10;
        if (lngDigito == 10) return 0;
        return lngDigito;
    }

    if (validado[2] != digitoVerificador(validado[1]))
        return false;

    return true; //Validado
}

function rfcValido(rfc, aceptarGenerico = true) {
    const re       = /^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/;
    var   validado = rfc.match(re);

    if (!validado)  //Coincide con el formato general del regex?
        return false;

    //Separar el dígito verificador del resto del RFC
    const digitoVerificador = validado.pop(),
        rfcSinDigito      = validado.slice(1).join(''),
        len               = rfcSinDigito.length,

        //Obtener el digito esperado
        diccionario       = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ",
        indice            = len + 1;
    var   suma,
        digitoEsperado;

    if (len == 12) suma = 0
    else suma = 481; //Ajuste para persona moral

    for(var i=0; i<len; i++)
        suma += diccionario.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
    digitoEsperado = 11 - suma % 11;
    if (digitoEsperado == 11) digitoEsperado = 0;
    else if (digitoEsperado == 10) digitoEsperado = "A";

    //El dígito verificador coincide con el esperado?
    // o es un RFC Genérico (ventas a público general)?
    if ((digitoVerificador != digitoEsperado)
        && (!aceptarGenerico || rfcSinDigito + digitoVerificador != "XAXX010101000"))
        return false;
    else if (!aceptarGenerico && rfcSinDigito + digitoVerificador == "XEXX010101000")
        return false;
    return rfcSinDigito + digitoVerificador;
}

mau.validar=new Mau_validar();
function Mau_validar() {
        this.rfc=function(input) {
            var rfc = input.val().trim().toUpperCase();

            if(rfc!='') {
                var rfcCorrecto = rfcValido(rfc);

                if (rfcCorrecto) {
                    input.css("backgroundColor", "lightgreen");
                } else {
                    input.css("backgroundColor", "lightpink");
                }
            }else{
                input.css("backgroundColor", "transparent");
            }

        }
    this.curp=function(input) {
        var curp = input.val().trim().toUpperCase();

        if(curp!='') {
            if (curpValida(curp)) {
                input.css("backgroundColor", "lightgreen");
            } else {
                input.css("backgroundColor", "lightpink");
            }
        }else{
            input.css("backgroundColor", "transparent");
        }

    }

}




