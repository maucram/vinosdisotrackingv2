var g_direccion=[],g_inc=0,g_max;

function busco_direcciones_sin_procesar() {


    $.ajax({
        type: 'post',
        url: base_url + 'historial/'+g_metodo+'/busco_direcciones_sin_procesar',
        beforeSend: function () {

            // $("#status-general").html('<div class="alert alert-info" role="alert" align="center">Guardando datos...</div>');
        },
        dataType:"json",
        async: false,
        success: function (respuesta) {

            if(respuesta.length>0){

                //$("body").prepend('<div class="cargando-ubicaciones"><p><span class="fa fa-spinner fa-spin fa-2x"></span><h3> Cargando ubicaciones...</h3>Por favor espere esto podría demorar hasta minutos.</p></div>');


                g_max=respuesta.length;
                ciclo_direcciones(respuesta);
            }
            else console.log("No hay direcciones por procesar");

        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            // $("#status-general").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar agregar, recarga e intenta nuevamente</div>');
        }
    });

}

function ciclo_direcciones(dir) {


    var geocoder = new google.maps.Geocoder();
    var latlon={lat:parseFloat(dir[g_inc].lat),lng:parseFloat(dir[g_inc].lon)};
    var aux=new Object();



    geocoder.geocode({ 'location':latlon }, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {

            aux.id=dir[g_inc].id;
            aux.direccion=results[0].formatted_address;
            g_direccion.push(aux);

            g_inc++;

            if(g_inc==g_max) {
                guardo_direcciones();
                return false;
            }


            if(g_inc<=10){
                setTimeout(function () {
                    ciclo_direcciones(dir);
                },1300);
            }else
                guardo_direcciones();

        }
    });

}

function guardo_direcciones() {

    $.ajax({
        type: 'post',
        url: base_url + 'historial/'+g_metodo+'/guardo_direcciones',
        data: {direcciones: g_direccion},
        // dataType:"json",
        success: function (response) {

            if(g_inc==g_max) {
              //  $("body .cargando-ubicaciones").fadeOut(2000);
                if(controlador=='mapa')
                    $("#buscar_ruta").click();
                else
                    tabladatoshistorialtodos.ajax.reload(null, false);

            }else location.reload(true);

        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
        }
    });

}

function obtengo_direcciones_ini(latlonid) {

    var latlon,direccion;
    $.each(latlonid, function (i, item) {
        latlon={lat:parseFloat(item.lat),lng:parseFloat(item.lon)};
        console.log(JSON.stringify(latlon));
            geocoder.geocode({'location': latlon}, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {

                    direccion={"id":item.id,"direccion":results[0].formatted_address};
                   // console.log(JSON.stringify(direccion));
                    guardo_direcciones(direccion);

                } else {
               //     window.alert('Resultados no encontrados');
                }
            } else {
             //   window.alert('Error map: ' + status);
            }
        });

    });

}

function guardo_direcciones_back(direccion){

    console.log(JSON.stringify(direccion));
    $.ajax({
        type: 'post',
        url: base_url + 'historial/'+g_metodo+'/guardo_direcciones',
        data:direccion,
        beforeSend: function () {

            // $("#status-general").html('<div class="alert alert-info" role="alert" align="center">Guardando datos...</div>');
        },
        async: false,
        success: function (respuesta) {
            setTimeout(function () {
                if(controlador=='mapa')
                    $("#buscar_ruta").click();
                else {
                    busco_direcciones_sin_procesar();
                    tabladatoshistorialtodos.ajax.reload(null, false);
                }

            },700);
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            // $("#status-general").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar agregar, recarga e intenta nuevamente</div>');
        }
    });
}
