var g_agrego_una_vez=true;
var g_duplicado=false;
var g_tiempo;

$("#boton_guardar").click(function () {
   var tabla=$(".form_agregar").data("tabla");
    const formData = new FormData($(".form_agregar")[0]);
    var data_table=$(".data_table_configuracion .cuerpo_tabla_configuracion tr");
    var controlador;
    var llave_agregar=false;

    $("#status-general").html('');

    g_duplicado=false;

    switch (tabla) {
        case 'Usuarios':
            controlador = 'usuarios';
            var usuario_in=filtro_palabra($("input[name=nom_usuario]").val());

            if (usuario_in!=''  && $("select[name=id_proyecto]").val()!='' && $("input[name=pass]").val().length>=5 && $("input[name=pass]").val()!='' && $("select[name=id_privilegio]").val()!=''  && g_agrego_una_vez){

                llave_agregar=true;

            }

            break;

        case 'Estatus':
            controlador = 'estatus';
            var f_estatus_in=filtro_palabra($("input[name=estatus]").val());

            if (f_estatus_in!='' && g_agrego_una_vez){
                llave_agregar=true; 
                var f_estatus_td;
                $.each(data_table,function () {
                    f_estatus_td=filtro_palabra($(this).find("td").eq(1).text());
                    if(f_estatus_td==f_estatus_in){
                        g_duplicado=true;
                        return false;
                    }
                });

            }

            break;

        case 'Concepto':
            controlador = 'concepto';
            var concepto_in=filtro_palabra($("input[name=concepto]").val());
            if (concepto_in!=''  && g_agrego_una_vez){
                llave_agregar=true;
                var concepto_td;
                $.each(data_table,function () {
                    concepto_td=filtro_palabra($(this).find("td").eq(1).text());
                    if(concepto_td==concepto_in){
                        g_duplicado=true;
                        return false;
                    }
                });

            }
            break;

        case 'Proyecto':
            controlador = 'proyecto';

            var proyecto_in=filtro_palabra($("input[name=proyecto]").val());
            if (proyecto_in!=''  && g_agrego_una_vez){
                llave_agregar=true;
                var proyecto_td;
                $.each(data_table,function () {
                    proyecto_td=filtro_palabra($(this).find("td").eq(1).text());
                    if(proyecto_td==proyecto_in){
                        g_duplicado=true;
                        return false;
                    }
                });

            }

            break;

        case 'Menú':

            controlador = 'menu';

            var menu_in=filtro_palabra($("input[name=menu]").val());
            if (menu_in!='' && $("select[name=id_proyecto]").val()!='' && g_agrego_una_vez){
                llave_agregar=true;
                var menu_td;
                var proyecto_in=filtro_palabra($("select[name=id_proyecto]:first option:selected").text()),proyecto_td;
                $.each(data_table,function () {
                    proyecto_td=filtro_palabra($(this).find("td").eq(1).text());
                    menu_td=filtro_palabra($(this).find("td").eq(2).text());
                    console.log(menu_td+' '+menu_in+'--'+proyecto_in+' '+proyecto_td);
                    if(menu_td==menu_in && proyecto_in==proyecto_td){
                        g_duplicado=true;
                        return false;
                    }
                });

            }

            break;

        case 'Módulo':

            controlador = 'modulo';

            var modulo_in=filtro_palabra($("input[name=modulo]").val());
            if (modulo_in!=''  && $("select[name=id_proyecto]").val()!='' && $("select[name=id_menu]").val()!='' && g_agrego_una_vez){
                llave_agregar=true;
                var modulo_td,menu_td,proyecto_td;

                var proyecto_in=filtro_palabra($("#mod_proyecto option:selected").text());
                var menu_in=filtro_palabra($("#mod_menu option:selected").text());

                $.each(data_table,function () {
                    proyecto_td=filtro_palabra($(this).find("td").eq(1).text());
                    menu_td=filtro_palabra($(this).find("td").eq(2).text());
                    modulo_td=filtro_palabra($(this).find("td").eq(3).text());
                    if(menu_td==menu_in && proyecto_in==proyecto_td && modulo_in==modulo_td){
                        g_duplicado=true;
                        return false;
                    }
                });

            }
            break;

        case 'Tickets':
            controlador = 'home';

            if ($("input[name=id_usuario]").val()!='' && $("input[name=descripcion]").val()!='' && $("input[name=fecha]").val()!='' && $("select[name=id_menu]").val()!=''
                && $("select[name=id_modulo]").val()!='' && $("select[name=id_estatus]").val()!='' && $("select[name=id_concepto]").val()!=''  && g_agrego_una_vez){
                llave_agregar=true;
            }

            break;
    }

    if(llave_agregar){
        if(!g_duplicado){
            $("#status-general").css("height","100%");
            g_agrego_una_vez=false;
            $.ajax({
                type: 'post',
                url: base_url +controlador+'/agregar',
                data: formData,
                contentType: false,
                beforeSend: function () {

                    $("#status-general").html('<div class="alert alert-info" role="alert" align="center">Guardando datos...</div>');
                },
                cache: false,
                processData: false,
                success: function (response) {
                    $("#status-general").html('<div class="alert alert-success" role="alert" align="center">Datos guardatos correctamente</div>');
                    location.href = base_url + controlador;
                },
                error: function (response) {
                    console.log("error" + JSON.stringify(response));
                    $("#status-general").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar agregar, recarga e intenta nuevamente</div>');
                }
            });
        }else {
            $("#status-general").css("height","50px");
            $("#status-general").html('<div class="alert alert-warning" role="alert" align="center">Campo(s) duplicado(s)</div>');
        }
    }

});

$("#boton_guardar_e").click(function () {

    var tabla=$(".form_agregar").data("tabla");
    var data_table=$(".data_table_configuracion .cuerpo_tabla_configuracion tr");
    var controlador;
    var llave_editar=false;

    $("#status-general").html('');

    g_duplicado=false;

    switch (tabla) {
        case 'Usuarios':
            controlador = 'usuarios';
            var usuario_in=filtro_palabra($("input[name=nom_usuario]:last").val());

            if (usuario_in!=''  && $("select[name=id_proyecto]").val()!='' && $("input[name=pass]:last").val().length>=5 && $("input[name=pass]:last").val()!='' && $("select[name=id_privilegio]:last").val()!=''  && g_agrego_una_vez){

                var datos={"nom_usuario":$("input[name=nom_usuario]:last").val(),
                    "id":$("input[name=id]").val(),
                    "pass":$("input[name=pass]:last").val(),
                    "id_proyecto":$("select[name=id_proyecto]:last").val(),
                    "id_privilegio":$("select[name=id_privilegio]:last").val(),
                    "status":$("input[name=status]:checked").val(),
                };

                llave_editar=true;

            }

            break;

        case 'Estatus':
            controlador = 'estatus';
            var f_estatus_in=filtro_palabra($("input[name=estatus]:last").val());

            if (f_estatus_in!='' && g_agrego_una_vez){

                var datos={"estatus":$("input[name=estatus]:last").val(),
                    "id":$("input[name=id]").val(),
                    "status":$("input[name=status]:checked").val(),
                };

                llave_editar=true;
                var f_estatus_td,id;
                $.each(data_table,function () {
                    id=$(this).find("td").eq(0).text();
                    f_estatus_td=filtro_palabra($(this).find("td").eq(1).text());
                    if(f_estatus_td==f_estatus_in && $("input[name=id]").val()!=id){
                        g_duplicado=true;
                        return false;
                    }
                });

            }

            break;

        case 'Concepto':
            controlador = 'concepto';
            var concepto_in=filtro_palabra($("input[name=concepto]:last").val());
            if (concepto_in!=''  && g_agrego_una_vez){

                var datos={"concepto":$("input[name=concepto]:last").val(),
                    "id":$("input[name=id]").val(),
                    "status":$("input[name=status]:checked").val(),
                };

                llave_editar=true;
                var concepto_td,id;
                $.each(data_table,function () {
                    id=$(this).find("td").eq(0).text();
                    concepto_td=filtro_palabra($(this).find("td").eq(1).text());
                    if(concepto_td==concepto_in && $("input[name=id]").val()!=id){
                        g_duplicado=true;
                        return false;
                    }
                });

            }
            break;

        case 'Proyecto':
            controlador = 'proyecto';

            var proyecto_in=filtro_palabra($("input[name=proyecto]:last").val());
            if (proyecto_in!=''  && g_agrego_una_vez){

                var datos={"proyecto":$("input[name=proyecto]:last").val(),
                    "id":$("input[name=id]").val(),
                    "status":$("input[name=status]:checked").val(),
                };

                llave_editar=true;
                var proyecto_td,id;
                $.each(data_table,function () {
                    id=$(this).find("td").eq(0).text();
                    proyecto_td=filtro_palabra($(this).find("td").eq(1).text());
                    if(proyecto_td==proyecto_in && $("input[name=id]").val()!=id){
                        g_duplicado=true;
                        return false;
                    }
                });

            }

            break;

        case 'Menú':

            controlador = 'menu';

            var menu_in=filtro_palabra($("input[name=menu]:last").val());
            if (menu_in!='' && $("select[name=id_proyecto]:last").val()!='' && g_agrego_una_vez){

                var datos={"id_proyecto":$("select[name=id_proyecto]:last").val(),
                    "id":$("input[name=id]").val(),
                    "menu":$("input[name=menu]:last").val(),
                    "status":$("input[name=status]:checked").val(),
                };

                llave_editar=true;
                var menu_td,id;
                var proyecto_in=filtro_palabra($("select[name=id_proyecto]:last option:selected").text()),proyecto_td;
                $.each(data_table,function () {
                    id=$(this).find("td").eq(0).text();
                    proyecto_td=filtro_palabra($(this).find("td").eq(1).text());
                    menu_td=filtro_palabra($(this).find("td").eq(2).text());
                    if(menu_td==menu_in && proyecto_in==proyecto_td && $("input[name=id]").val()!=id){
                        g_duplicado=true;
                        return false;
                    }
                });

            }

            break;

        case 'Módulo':

            controlador = 'modulo';

            var modulo_in=filtro_palabra($("input[name=modulo]:last").val());
            if (modulo_in!=''  && $("select[name=id_proyecto]:last").val()!='' && $("select[name=id_menu]:last").val()!='' && g_agrego_una_vez){

                var datos={"id_proyecto":$("select[name=id_proyecto]:last").val(),
                    "id_menu":$("select[name=id_menu]:last").val(),
                    "id":$("input[name=id]").val(),
                    "modulo":$("input[name=modulo]:last").val(),
                    "status":$("input[name=status]:checked").val(),
                };

                llave_editar=true;
                var modulo_td,menu_td,proyecto_td,id;

                var proyecto_in=filtro_palabra($("#mod_proyecto_e option:selected").text());
                var menu_in=filtro_palabra($("#mod_menu_e option:selected").text());

                $.each(data_table,function () {
                    id=$(this).find("td").eq(0).text();
                    proyecto_td=filtro_palabra($(this).find("td").eq(1).text());
                    menu_td=filtro_palabra($(this).find("td").eq(2).text());
                    modulo_td=filtro_palabra($(this).find("td").eq(3).text());
                    if(menu_td==menu_in && proyecto_in==proyecto_td && modulo_in==modulo_td && $("input[name=id]").val()!=id){
                        g_duplicado=true;
                        return false;
                    }
                });

            }
            break;

    }

    if(llave_editar){
        if(!g_duplicado){
            $("#status-general_e").css("height","100%");
            g_agrego_una_vez=false;
            $.ajax({
                type: 'post',
                url: base_url +controlador+'/editar',
                data: datos,
                beforeSend: function () {

                    $("#status-general_e").html('<div class="alert alert-info" role="alert" align="center">Editando datos...</div>');
                },
                success: function (response) {
                    $("#status-general_e").html('<div class="alert alert-success" role="alert" align="center">Datos editados correctamente</div>');
                    location.href = base_url + controlador;
                },
                error: function (response) {
                    console.log("error" + JSON.stringify(response));
                    $("#status-general_e").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar editar, recarga e intenta nuevamente</div>');
                }
            });
        }else {
            $("#status-general_e").css("height","50px");
            $("#status-general_e").html('<div class="alert alert-warning" role="alert" align="center">Campo(s) duplicado(s)</div>');
        }
    }

});
// editar tablas //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion tr', function (e) {

    if($(this).hasClass("tickets-eliminado")){
        $("input[name=status][value='0']").prop("checked",true);
    }else
        $("input[name=status][value='1']").prop("checked",true);
    $("#status-general_e").html('');
    /*
    if($(this).hasClass("child")){
        if($(this).prev().hasClass("tickets-eliminado")){
            $("input[name=status][value='0']").prop("checked",true);
        }else
            $("input[name=status][value='1']").prop("checked",true);

    }
    */
});
/*********************************************************** usuarios ******************************************************************/


$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_usuarios', function (e) {

    $("#status-general_e").html("");

    var id = $(this).data('id'),
        usuario= $(this).data('usuario'),
        pass = $(this).data('pass'),
        id_proyecto = $(this).data('id_proyecto'),
        id_privilegio= $(this).data('id_privilegio');


  $("input[name=id]").val(id);
    $("input[name=nom_usuario]:last").val(usuario);
    $("input[name=pass]:last").val(pass);
    $("select[name=id_proyecto]:last").val(id_proyecto);
    $("select[name=id_privilegio]:last").val(id_privilegio);
    $("input[name=nom_usuario]:last").val(usuario);

    if(id_privilegio!=3){// difernte de cliente
        $("#proyecto_e").val(0);
        $("#proyecto_e").prop("disabled",true);
    } else{
        $("#proyecto_e").prop("disabled",false);
        $("#proyecto_e option[value=0]").prop("disabled",true);
    }

});

$("#usuarios_usuario,#usuarios_usuario_e").keyup(function () {

    var status_general='#status-general';
    var boton_guardar='#boton_guardar';
    if($("#usuarios_usuario_e").val().length>0){
        status_general+='_e';
        boton_guardar+='_e';
    }
    var nom_usuario=$(this).val();
    if(nom_usuario!='') {
        $.ajax({
            type: 'post',
            url: base_url + 'usuarios/verificar_usuario',
            data: {nom_usuario: nom_usuario},
            beforeSend: function () {
                $(status_general).html('<div class="alert alert-info" role="alert" align="center">Verificando usuario...</div>');
            },
            success: function (response) {
                if (response) {
                    $(status_general).html('<div class="alert alert-warning" role="alert" align="center">El usuario ya existe, intenta con otro</div>');
                    $(boton_guardar).hide();
                } else {
                    $(status_general).html('');
                    $(boton_guardar).show();
                }
            },
            error: function (response) {
                console.log("error" + JSON.stringify(response));
                $(status_general).html('<div class="alert alert-danger" role="alert" align="center">Error al comprobar usuario</div>');
                $(boton_guardar).hide();
            }
        });
    }
});

$("#usuarios_privilegio").change(function () {
    var id_privilegio=$(this).val();

    if(id_privilegio!=3){// difernte de cliente
        $("#proyecto").val(0);
        $("#proyecto").prop("disabled",true);
    } else{
        $("#proyecto").val("");
        $("#proyecto").prop("disabled",false);
        $("#proyecto option[value=0]").prop("disabled",true);
    }
});
$("#usuarios_privilegio_e").change(function () {
    var id_privilegio=$(this).val();

    if(id_privilegio!=3){// difernte de cliente
        $("#proyecto_e").val(0);
        $("#proyecto_e").prop("disabled",true);
    } else{
        $("#proyecto_e").val("");
        $("#proyecto_e").prop("disabled",false);
        $("#proyecto_e option[value=0]").prop("disabled",true);
    }
});
/*********************************************************** estatus ******************************************************************/


$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_estatus', function (e) {

    $("#status-general_e").html("");

    var id = $(this).data('id'),
        estatus= $(this).data('estatus');

    $("input[name=id]").val(id);
    $("input[name=estatus]:last").val(estatus);

});

/*********************************************************** concepto ******************************************************************/

$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_concepto', function (e) {

    $("#status-general_e").html("");

    var id = $(this).data('id'),
        concepto= $(this).data('concepto');

    $("input[name=id]").val(id);
    $("input[name=concepto]:last").val(concepto);

});
/*********************************************************** proyecto ******************************************************************/

$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_proyecto', function (e) {

    $("#status-general_e").html("");

    var id = $(this).data('id'),
        proyecto= $(this).data('proyecto');

    $("input[name=id]").val(id);
    $("input[name=proyecto]:last").val(proyecto);

});

/*********************************************************** menu ******************************************************************/

$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_menu', function (e) {

    $("#status-general_e").html("");

    var id = $(this).data('id'),
        id_proyecto= $(this).data('id_proyecto'),
        menu=$(this).data('menu');

    $("input[name=id]").val(id);
    $("select[name=id_proyecto]:last").val(id_proyecto);
    $("input[name=menu]:last").val(menu);

});

/*********************************************************** modulo ******************************************************************/

$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_modulo', function (e) {

    $("#status-general_e").html("");

    var id = $(this).data('id'),
        id_proyecto= $(this).data('id_proyecto'),
        id_menu= $(this).data('id_menu'),
        menu= $(this).data('menu'),
        modulo=$(this).data('modulo');
    var option_menu='<option value="'+id_menu+'">Cargando...</option>';

    $.ajax({
        type: 'post',
        url: base_url + 'modulo/obtengo_options_menu',
        data: {id_proyecto:id_proyecto},
        beforeSend: function () {

            $("select[name=id_menu]:last").html(option_menu);
        },
        success: function (options_me) {

            $("input[name=id]").val(id);
            $("#mod_proyecto_e").val(id_proyecto);
            $("select[name=id_menu]:last").html(options_me);
            $("select[name=id_menu]:last").val(id_menu);
            $("input[name=modulo]:last").val(modulo);
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            alert("Error al obtener opciones de menú");
        }
    });



});

$("#mod_proyecto").change(function () {
    var id_proyecto=$(this).val();
   obtengo_options_menu(id_proyecto);
});

function obtengo_options_menu(id_proyecto){
    $.ajax({
        type: 'post',
        url: base_url + 'modulo/obtengo_datos_menu',
        // async:false,
        data: {id_proyecto:id_proyecto},
        // contentType: false,
        beforeSend: function () {

            $("#mod_menu").prop("disabled",true);
        },
        //cache: false,
        //processData: false,
        //  dataType:'json',
        success: function (response) {
            $("#mod_menu").prop("disabled",false);

            $("#mod_menu").html(response);
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#mod_menu").html("Error al cargar");
        }
    });
}
$("#mod_proyecto_e").change(function () {
    var id_proyecto=$(this).val();
    $.ajax({
        type: 'post',
        url: base_url + 'modulo/obtengo_datos_menu',
        // async:false,
        data: {id_proyecto:id_proyecto},
        // contentType: false,
        beforeSend: function () {

            $("#mod_menu_e").prop("disabled",true);
        },
        //cache: false,
        //processData: false,
        //  dataType:'json',
        success: function (response) {
            $("#mod_menu_e").prop("disabled",false);

            $("#mod_menu_e").html(response);
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#mod_menu_e").html("Error al cargar");
        }
    });
});

/*********************************************************** tickets ******************************************************************/
/*
$('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_tickets', function (e) {

    $("#status-general_e").html("");

    var id = $(this).data('id'),
        id_proyecto= $(this).data('id_proyecto'),
        proyecto= $(this).data('proyecto'),
        id_menu= $(this).data('id_menu'),
        menu= $(this).data('menu'),
        fecha= $(this).data('fecha'),
        descripcion= $(this).data('descripcion'),
        id_concepto= $(this).data('id_concepto'),
        id_estatus= $(this).data('id_estatus'),
        id_modulo= $(this).data('id_modulo'),
        modulo=$(this).data('modulo');

    var option_menu='<option value="'+id_menu+'">'+menu+'</option>';
    var option_modulo='<option value="'+id_modulo+'">'+modulo+'</option>';
    var option_proyecto='<option value="'+id_proyecto+'">'+proyecto+'</option>';

    $("input[name=id]").val(id);
    $("select[name=id_proyecto]:last").html(option_proyecto);
    $("select[name=id_menu]:last").val(id_menu);
    $("select[name=id_modulo]:last").html(option_modulo);
    $("select[name=id_concepto]:last").val(id_concepto);
    $("select[name=id_estatus]:last").val(id_estatus);
    $("textarea[name=descripcion]:last").val(descripcion);
    $("input[name=fecha]").val(fecha);

});
*/
$('.data_table_configuracion .cuerpo_tabla_configuracion .t_estatus_tickets td').not(".eliminar_ticket").click(function (e) {

    if($("#Modal_agregar_estatus").length==0){

        var modal_estatus='<div class="modal fade" id="Modal_agregar_estatus">\n' +
            '    <div class="modal-dialog" style="width: 90%">\n' +
            '        <div class="modal-content">\n' +
            '\n' +
            '            <!-- Modal Header -->\n' +
            '            <div class="modal-header">\n' +
            '                <button type="button" class="close" data-dismiss="modal">&times;</button>\n' +
            '                <h4 class="modal-title">ESTATUS</h4>\n' +
            '            </div>\n' +
            '\n' +
            '\n' +
            '          <form onsubmit="return false" method="post"  enctype="multipart/form-data" class="agregar_estatus__" accept-charset="utf-8">\n' +
            '\n' +
            '            <div class="modal-body cuerpo_modal_estatus">\n' +
            '            </div>\n' +
            '            <div class="modal-close" style="padding: 30px;margin-top: 20px" align="center">\n' +
            '                <div class="row">\n' +
            '                    <div class="form-group col-xs-12">\n' +
            '                        <button type="submit" class="btn boton_ticket_modals"  id="boton_guardar_estatus">Agregar</button>\n' +
            '                        <button type="button" class="btn boton_ticket_modals" data-dismiss="modal">Cancelar</button><br>\n' +
            '                    </div>\n' +
            '                </div>\n' +
            '\n' +
            '            </div>\n' +
            '            <div id="t_status-general" align="center"></div>\n' +
            '          </form>\n' +
            '\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';

        $("body").append(modal_estatus);
    }

    $("#Modal_agregar_estatus").modal("show");



    var id_ticket=$(this).siblings("td").eq(0).text();

    $.ajax({
        type: 'post',
        url: base_url + 'home/obtengo_datos_estatus_ticket',
        // async:false,
        data: {id_ticket:id_ticket},
        beforeSend: function () {
            $("#t_status-general").html('<div class="alert alert-info" role="alert" align="center">Cargando estatus...</div>');
        },
        success: function (response) {
            $("#t_status-general").html('');
            var tabla = '<div class="table-responsive table-striped table-hover"><table class="table">';
            var id_privilegio=$("#id_p_u").val();
            var colspan='';


            var cuerpo_modal_estatus_cargados = '';

            if(response) {
                var estatus = JSON.parse(response);

                $.each(estatus,function (i,item) {
                    var aux_fecha=item.fecha.split("-");
                    var fecha=aux_fecha[2]+'/'+aux_fecha[1]+'/'+aux_fecha[0];

                    var archivo=(item.url_archivo==null)?'':'<a target="_blank" href="'+base_url+'carga_archivos/'+item.url_archivo+'"><i class="fa fa-2x fa-download" title="ver/descargar"></i></a>';

                    cuerpo_modal_estatus_cargados+= '<tr><td>'+fecha+'</td><td>'+item.estatus+'</td><td>'+item.nom_usuario+'</td><td>'+item.comentarios+'</td><td>'+archivo+'</td>';

                    //if(id_privilegio==1) //superusuario
                   //     cuerpo_modal_estatus_cargados+='<td><i class="fa fa-2x fa-trash-o eliminar_estatus_ticket" data-id="'+item.id+'""></i></td></tr>';
                    //else
                        cuerpo_modal_estatus_cargados+='</tr>';

                });
            }

            var cabeza_modal = '<tr><th>Fecha</th><th>Estatus</th><th>Usuario</th><th>Comentarios</th><th>Archivo</th>';

         //   if(id_privilegio==1 && cuerpo_modal_estatus_cargados!='') { //superusuario
           //     cabeza_modal += '<th>Eliminar</th></tr>';
             //   colspan='2';

            //}else {
                cabeza_modal += '</tr>';
                colspan='1';
            //}


            var fecha_hoy = new Date();
            var mes = fecha_hoy.getMonth() + 1;
            var dia = fecha_hoy.getDate();
            var ano = fecha_hoy.getFullYear();
            if (dia < 10)
                dia = '0' + dia;
            if (mes < 10)
                mes = '0' + mes
            var fecha_yyyymmdd = ano + "-" + mes + "-" + dia;

            var select_estatus = '<select class="form-control" required id="t_id_estatus" name="id_estatus">';

            $.each($("select[name=id_estatus]:first option"), function () {
                select_estatus += '<option value="' + $(this).val() + '">' + $(this).text() + '</option>';
            });

            select_estatus += '</select>';

            var usuario = $(".profile-data-name").text();


            var cuerpo_modal_estatus_por_agregar = '<tr><td><input type="hidden" name="id_ticket" value="'+id_ticket+'" ><input class="form-control" required type="date" name="fecha" value="' + fecha_yyyymmdd + '" readonly id="t_estatus_fecha"></td><td>' + select_estatus + '</td><td><input class="form-control" name="id_usuario" type="text" value="' + usuario + '" readonly id="t_id_usuario"></td><td><textarea  class="form-control" name="comentarios" id="t_comentario"></textarea></td><td colspan="'+colspan+'"><input type="file" class="form-control" name="url_archivo" id="t_archivo"></td></tr>';

            tabla += cabeza_modal + cuerpo_modal_estatus_cargados + cuerpo_modal_estatus_por_agregar + '</table></div>';

            $(".cuerpo_modal_estatus").html(tabla);
            $("#t_comentario").focus();

            /*
            $(".eliminar_estatus_ticket").click(function () {

                var id_estatus_ticket=$(this).data("id");
                var este_td=$(this).parent();

                var confirmar=confirm('¿Estás seguro?');
                if(confirmar){
                    $.ajax({
                        type: 'post',
                        url: base_url + 'home/eliminar_un_estatus_ticket',
                        // async:false,
                        data: {id_estatus_ticket:id_estatus_ticket},
                        beforeSend: function () {

                            este_td.html("Eliminando...").css("backgroundColor","lightblue");
                        },
                        success: function (response) {
                            este_td.html("Eliminado").css("backgroundColor","lightgreen");
                            location.href = base_url + 'home';
                        },
                        error: function (response) {
                            este_td.html("Error al intentar eliminar").css("backgroundColor","#E98C73");

                        }
                    });

                }

            });
            */

        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#t_status-general").html('<div class="alert alert-danger" role="alert" align="center">Error al cargar estatus...</div>');
        }
    });



    // guarrdar estado tickets =======================================================================

    $("#boton_guardar_estatus").click(function () {

        if ($("#t_id_estatus").val()!='' && g_agrego_una_vez){
            g_agrego_una_vez=false;

            const formData = new FormData($(".agregar_estatus__")[0]);
            $.ajax({
                type: 'post',
                url: base_url + 'home/agregar_estatus_ticket',
                // async:false,
                data: formData,
                contentType: false,
                beforeSend: function () {

                    $("#t_status-general").html('<div class="alert alert-info" role="alert" align="center">Agregando estatus...</div>');
                },
                cache: false,
                processData: false,
                //  dataType:'json',
                success: function (response) {
                    $("#t_status-general").html('<div class="alert alert-success" role="alert" align="center">Estatus agregado correctamente correctamente</div>');
                   location.href = base_url + 'home';
                },
                error: function (response) {
                    console.log("error" + JSON.stringify(response));
                    $("#t_status-general").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar agregar estatus</div>');
                }
            });
        }else{
            $("#t_status-general").html('<div class="alert alert-warning" role="alert" align="center">El estatus es obligatorio</div>');
        }
    });

});

function reset_tiempo(){
    g_agrego_una_vez=true;
    clearInterval(g_tiempo);
}








$("#mod_menu").change(function () {
    var id_modulo=$(this).val();
    $.ajax({
        type: 'post',
        url: base_url + 'home/obtengo_datos_modulo',
        // async:false,
        data: {id_modulo:id_modulo},
        // contentType: false,
        beforeSend: function () {

            $("#tick_modulo").prop("disabled",true);
        },
        //cache: false,
        //processData: false,
        //  dataType:'json',
        success: function (response) {
            $("#tick_modulo").prop("disabled",false);

            $("#tick_modulo").html(response);
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#tick_modulo").html("Error al cargar");
        }
    });
});

$("#mod_menu_e").change(function () {
    var id_modulo=$(this).val();
    $.ajax({
        type: 'post',
        url: base_url + 'home/obtengo_datos_modulo',
        // async:false,
        data: {id_modulo:id_modulo},
        // contentType: false,
        beforeSend: function () {

            $("#tick_modulo_e").prop("disabled",true);
        },
        //cache: false,
        //processData: false,
        //  dataType:'json',
        success: function (response) {
            $("#tick_modulo_e").prop("disabled",false);

            $("#tick_modulo_e").html(response);
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#tick_modulo_e").html("Error al cargar");
        }
    });
});


function filtro_palabra(palabra) {
    return palabra
        .normalize('NFD')
        .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,"$1")
        .normalize().toLowerCase().trim();
}



$("#id_usuario").change(function () {

    var id_usuario=$(this).val();

    if(id_usuario!='') {
        $.ajax({
            type: 'post',
            url: base_url + 'home/busco_proyectos_usuario',
            // async:false,
            data: {id_usuario: id_usuario},
            beforeSend: function () {
                $("#mod_proyecto").prop("disabled", true);
                $("#mod_proyecto").val('');
            },
            success: function (response) {

                if (response == 0) {
                    $("#mod_proyecto").prop("disabled", false);
                    $("#mod_menu").prop("disabled", true);
                    $("#mod_proyecto").val('');
                } else if (response != -1){
                    $("#mod_proyecto").val(response);
                    obtengo_options_menu(response)

                }
            },
            error: function (response) {
                alert("Error! "+JSON.stringify(response));

            }
        });
    }

});




// eliminar *********************************************************************************************************************************************************************************

$(".eliminar_ticket").click(function () {

    var id_ticket=$(this).siblings("td").eq(0).text();

    var este_td=$(this);

    var confirmar=confirm('¿Estás seguro de eliminar el Ticket con Id '+id_ticket+'?');
    if(confirmar){
        $.ajax({
            type: 'post',
            url: base_url + 'home/eliminar',
            // async:false,
            data: {id_ticket:id_ticket},
            beforeSend: function () {

               este_td.html("Eliminando...").css("backgroundColor","lightblue");
            },
            success: function (response) {
                este_td.html("Eliminado").css("backgroundColor","lightgreen");
                location.href = base_url + 'home';
            },
            error: function (response) {
                este_td.html("Error al intentar eliminar").css("backgroundColor","#E98C73");

            }
        });

    }
});


function llamo_modal_eliminar() {

    if($("#Modal_eliminar").length==0){

        var modal_estatus='<div class="modal fade" id="Modal_eliminar" style="background-color:rgba(255,0,0,.5);z-index: 99999999">\n' +
            '    <div class="modal-dialog" style="width: 10%">\n' +
            '        <div class="modal-content">\n' +
            '\n' +
            '            <!-- Modal Header -->\n' +
            '            <div class="modal-header">\n' +
            '                <button type="button" class="close" data-dismiss="modal">&times;</button>\n' +
            '                <h4 class="modal-title">Eliminar</h4>\n' +
            '            </div>\n' +
            '\n' +
            '\n' +

            '\n' +
            '            <div class="modal-body ">¿Estás seguro?\n' +
            '            </div>\n' +
            '            <div class="modal-close" align="center">\n' +
            '                        <button type="submit" class="btn btn-danger"  id="boton_guardar_estatus">Si</button>\n' +
            '                        <button type="button" class="btn btn-default" data-dismiss="modal">No</button><br>\n' +
            '\n' +
            '            </div>\n' +
            '\n' +
            '        </div>\n' +
            '    </div>\n' +
            '</div>';

        $("body").append(modal_estatus);
    }

    $("#Modal_eliminar").modal("show");
}

$("#boton_eliminar").click(function () {

    var confirmar=confirm('¿Estás seguro?');
    if(confirmar) {
        var tabla = $(".form_agregar").data("tabla"), id = $("input[name=id]").val(), controlador;
        switch (tabla) {
            case 'Usuarios':
                controlador = 'usuarios';
                break;

            case 'Estatus':
                controlador = 'estatus';
                break;

            case 'Concepto':
                controlador = 'concepto';
                break;

            case 'Proyecto':
                controlador = 'proyecto';
                break;

            case 'Menú':

                controlador = 'menu';
                break;

            case 'Módulo':

                controlador = 'modulo';
                break;

            case 'Tickets':
                controlador = 'home';
                break;
        }

        $.ajax({
            type: 'post',
            url: base_url + 'home/eliminar_t_datos',

            data: {id: id, tabla: controlador},

            beforeSend: function () {

                $("#status-general_e").html('<div class="alert alert-info" role="alert" align="center">Eliminando datos...</div>');
            },
            success: function (response) {
                $("#status-general_e").html('<div class="alert alert-success" role="alert" align="center">Datos eliminados correctamente</div>');
                location.href = base_url + controlador;
            },
            error: function (response) {
                console.log("error" + JSON.stringify(response));
                $("#status-general_e").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar eliminar</div>');
            }
        });
    }

});

// ***************************************************** descargar excel tickets

$(".descargar_excel_tickets").click(function () {
    var data_table=$(".data_table_configuracion .cuerpo_tabla_configuracion tr");
    var ids='';

    $.each(data_table,function (i) {
        ids+='/'+$(this).children().eq(0).text();
    });

    if(ids!='/No se encontraron resultados'){

        window.open(base_url + 'home/descargar_excel_tickets'+ids,"_parent");
    }
});