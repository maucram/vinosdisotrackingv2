
var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
var labelIndex = 0;
var g_metodo=$(".breadcrumb li:last").text().toLowerCase();

var neighborhoods = [
    {lat: 19.44, lng:  -99.14},
    {lat: 52.549, lng: 13.422},
    {lat: 52.497, lng: 13.396},
    {lat: 52.517, lng: 13.394}
];


var markers = [];
var map;
var geocoder;
var infowindow;
var g_ubicacion=[];
var controlador='mapa';
var g_datos_ubicaciones;
function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: {lat: 19.44, lng:-99.14}
    });
    geocoder = new google.maps.Geocoder;
    infowindow = new google.maps.InfoWindow;


}
var g_id=[];
function imprimo_datos(datos) {

    initMap();
    labelIndex = 0;
    g_id=[];
    $.each(datos,function (i,item) {
        g_id[i]=item.id;

          g_ubicacion[i]={lat:parseFloat(item.lat),lng:parseFloat(item.lng)};
//
//
//
        addMarkerWithTimeout(g_ubicacion[i], i * 200,item,i);
//
//         var text_ubicacion='';
//       //  var es=item.fecha.split(/ |\/|:/gi);
//        // var fecha_concat=es[2]+es[1]+es[0]+es[3]+es[4]+es[5];
//
        var ruta_foto=base_url+'carga_fotos/'+item.id_usuario+'/'+item.foto;
//
        var foto=(item.foto!='')?'<a target="_blank" href="'+ruta_foto+'"><i class="fa fa-eye fa-2x"></i></a>':'';
//
//         geocoder.geocode({'location': g_ubicacion[i]}, function(results, status) {
//             if (status === 'OK') {
//                 if (results[0]) {
//                     // map.setZoom(11);
//                     tabladatoshistorial.row.add([item.id,item.usuario,item.fecha,results[0].formatted_address,foto,item.estatus,item.comentario,item.empresa]).draw();
//                     //infowindow.setContent(results[0].formatted_address);
//                 } else {
//                     window.alert('Resultados no encontrados');
//                 }
//             } else {
//                 window.alert('Error map: ' + status);
//             }
//         });
// */

            var fila=(g_metodo=="promotor-vendedor")?[item.id,
                item.promotor_vendedor,
                item.fecha,
                item.direccion,
                foto,
                item.estatus,
                item.empresa,
                item.sucursal,
                item.cliente,
                item.marca,
                item.comentario]

                :

                [item.id,
                    item.promotor,
                    item.fecha,
                    item.direccion,
                    foto,
                    item.estatus,
                    item.empresa,
                    item.sucursal,
                    item.cliente,
                    item.marca,
                    item.producto,
                    item.lugar,
                    item.posicion,
                    item.cantidad,
                    item.comentario];
        tabladatoshistorial.row.add(fila).draw();
    });

}

function imprimo_datos_(datos){
    clearMarkers();
    labelIndex=0;
    for (var i = 0; i < neighborhoods.length; i++) {

        alert(JSON.stringify(neighborhoods[i]));
        addMarkerWithTimeout(neighborhoods[i], i * 200);

    }
}


function addMarkerWithTimeout(position, timeout,item,i) {
    window.setTimeout(function() {
        markers.push(new google.maps.Marker({
            position: position,
            map: map,
            label: (i+1).toString(),//labels[labelIndex++ % labels.length]+'-'+item.id+'-'+item.empresa,//labels[labelIndex++ % labels.length],
            animation: google.maps.Animation.DROP
        }));
       // infowindow.setContent();
        //infowindow.open(map, markers[i]);
    }, timeout);
}

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}

$("#buscar_ruta").click(function () {

    var id_persona=$("#mapa_usuario").val(),
        fecha=$("#mapa_fecha").val();
    if(id_persona!='' && fecha!='' && id_persona!==null){
        $.ajax({
            type:'post',
            data:{id_persona:id_persona,fecha:fecha},
            url: base_url +'mapa/'+g_metodo+'/cargo_datos_ubicaciones',
            beforeSend: function () {

                 $("#map_status").html('<div class="alert alert-info" role="alert" align="center">Cargando datos...</div>');
                clearMarkers();
                busco_direcciones_sin_procesar();

            },
            success: function (response) {
                if(response!=-1) {
                     g_datos_ubicaciones = JSON.parse(response);
                    $("#map_status").html('<div class="alert alert-success" role="alert" align="center">Datos cargados correctamente</div>');
                   var quito_status_mapa=setInterval(function () {
                        $("#map_status").html('');
                        clearInterval(quito_status_mapa);
                    },1000);
                    //imprimo_ubicaciones(datos.ubicaciones);
                    //imprimo_tabla(datos);
                    tabladatoshistorial.clear().draw();
                    imprimo_datos(g_datos_ubicaciones);
                    obtengo_grafica(id_persona,fecha);
                }else{
                    $("#map_status").html('<div class="alert alert-warning" role="alert" align="center">No se encontraron coincidencias</div>');
                }
            },
            error: function (response) {
                console.log("error" + JSON.stringify(response));
                $("#map_status").html('<div class="alert alert-danger" role="alert" align="center">Error al cargar, recarga e intenta nuevamente</div>');
            }
        });

    }

});

$('.data_table_historial tbody').on('click','tr td:not(:nth-child(5))',function () {

    var posicion = $("#foco_mapa").offset().top;
    $("html, body").animate({
        scrollTop: posicion
    }, 400);

    var id=$(this).parent("tr").children("td").eq(0).text();


    for (var i = 0; i < g_ubicacion.length; i++) {
        var id_mark=markers[i].label.split("-");

      //  $(".data_table_historial").DataTable().column(0).data().each(function (value,index) {
            if(g_id[i]==id){ //id_mark[1]
                var index=i;
                i=g_ubicacion.length;
            }
        //});

    }


    geocoder.geocode({'location': g_ubicacion[index]}, function(results, status) {
        if (status === 'OK') {
            if (results[0]) {
                map.setZoom(16);
                map.setCenter(g_ubicacion[index]);
                infowindow.setContent(results[0].formatted_address);

                infowindow.open(map, markers[index]);
            } else {
                window.alert('Resultados no encontrados');
            }
        } else {
            if(status=="ZERO_RESULTS")
                window.alert("La ubicación no se puede mostrar porque los datos fueron capturados con el GPS apagado");
            else
                window.alert('Error map: ' + status);
        }
    });


});

  // $.when(llado_ajax).done(function(data) {
//  var datos=JSON.parse(data);
/*



 */
//});


//$(document).ready(function () {

    $.ajax({
        type:'post',
        url: base_url +'configuracion/obtengo_promotores/'+g_metodo,
        beforeSend: function () {

            $("#mapa_usuario").html('<option value="">Cargando datos...</option>');
        },
        dataType:"json",
        success: function (response) {
           if(response.status==200){

               var opciones='';
               $.each(response.data,function (i,promotor) {
                   opciones+='<option value="'+promotor.id+'">'+promotor.promotor+'</option>';
               });
               $('#mapa_usuario').append(opciones);
               $('#mapa_usuario').select2({
                   "placeholder":"Elige un "+g_metodo+"..."
               });

           } else{
               $("#mapa_usuario").html('<option value="">No hay usuarios disponibles</option>');
           }
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#mapa_usuario").html('<option value="">Error al cargar</option>');
        }
    });

    /*  $('#mapa_usuario').select2({


          ajax: {
                url: base_url+'historial/obtengo_usuarios',
                type: 'post',
                data: function (params) {



                    var query = {
                        search: params.term
                    }

                    // Query parameters will be ?search=[term]&type=public
                    return query;
                }, processResults: function (data) {
                    // Tranforms the top-level key of the response object from 'items' to 'results'
                    var json_data=JSON.parse(data);

                    return {
                        results: json_data.results
                    }
                }
            }
    });*/
//});


// graficas ====================================

function cargo_grafica_vacia() {
    Highcharts.chart('grafica', {

        title: {
            text: "Promedio visitas"
        },
        chart: {
            type: 'spline',
            zoomType: 'x',
            panning: true,
            panKey: 'shift',
            scrollablePlotArea: {
                minWidth: 400
            }
        },
        tooltip: {
            valueSuffix: ''
        },

        subtitle: {
            text: ''
        },

        yAxis: {
            title: {
                text: 'Visitas'
            }
        },
        legend: {
            enabled: false
        },

        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        },
        xAxis: {
            categories: ['1',"2","3","4","5","6","7","8","9","10",'11',"12","13","14","15","16","17","18","19","20",'21',"22","23","24","25","26","27","28","29","30"]
            ,minRange: 15,
            title: {
                text: 'Últimos 30 días'
            }
        },
        colors: ['#DB244B'],

        series: [{
            name: '',
            data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
            marker: {
                enabled: false
            },
            threshold: null
        }],

        responsive: {
            rules: [{
                condition: {
                    maxWidth: 700
                },
                chartOptions: {
                    legend: {
                        layout: 'vertical',
                        align: 'center',
                        verticalAlign: 'bottom'
                    }
                }
            }]
        }

    });
}
cargo_grafica_vacia();
function obtengo_grafica(id_persona,fecha) {
    $.ajax({
        type:'post',
        url: base_url +'mapa/'+g_metodo+'/obtengo_grafica',
        beforeSend: function () {

            $("#grafica").html('<i class="fa fa-spin fa-spinner"></i> Cargando gráfica...').css("color","blue");
        },
        dataType:"json",
        data:{id_promotor:id_persona,fecha:fecha},
        success: function (respuesta) {

            if(respuesta.status==200){

                var datos=new Object(),min_fecha=[],cont=0, puerta=false,f_div=fecha.split("-"),
                fecha_elegida=f_div[2]+"/"+f_div[1]+"/"+f_div[0];
                fecha=f_div[0]+"/"+f_div[1]+"/"+f_div[2];
                datos.serie=[];
                datos.fechas=[];
                min_fecha["ymd"]=fecha;
                min_fecha["dmy"]=fecha_elegida;
                datos.prom=0;


                for (var i=0;i<30;i++){



                    $.each(respuesta.data,function (j,val) { console.debug(min_fecha["ymd"]+' '+val.fecha);

                        if(min_fecha["ymd"]==val.fecha){
                            puerta=true;
                            datos.serie.unshift(parseInt(val.visitas));

                            datos.prom+=parseInt(val.visitas);
                                return false;
                        }

                   });

                    if(!puerta){
                        datos.serie.unshift(0);
                        datos.prom+=0;

                    }
                    datos.fechas.unshift(min_fecha["dmy"]);
                    min_fecha=mau.fechas.inc_dis_fecha(min_fecha["ymd"],"-","dia");
                    puerta=false;
                }

                datos.prom=Math.round(datos.prom/30);



                //f_div=min_fecha.split("/");
                   // min_fecha=f_div[2]+"/"+f_div[1]+"/"+f_div[0];

                Highcharts.chart('grafica', {

                    title: {
                        text:'Promedio visitas '+datos.prom
                    },
                    chart: {
                        type: 'spline',
                        zoomType: 'x',
                        panning: true,
                        panKey: 'shift',
                        scrollablePlotArea: {
                            minWidth: 400
                        }
                    },
                    tooltip: {
                        valueSuffix:""
                    },

                    subtitle: {
                        text: ''
                    },

                    yAxis: {
                        title: {
                            text: 'Visitas'
                        }
                    },
                    legend: {
                        enabled: false
                    },

                    plotOptions: {
                        spline: {
                            marker: {
                                radius: 4,
                                lineColor: '#666666',
                                lineWidth: 1
                            }
                        }
                    },
                    xAxis: {
                        categories: datos.fechas,//['1',"2","3","4","5","6","7","8","9","10",'11',"12","13","14","15","16","17","18","19","20",'21',"22","23","24","25","26","27","28","29","30"],
                        title: {
                            text: 'Últimos 30 días'
                        }
                    },
                    colors: ['#DB244B'],

                    series: [{
                        name: '',
                        data: datos.serie,
                        marker: {
                            enabled: false
                        },
                        threshold: null
                    }],

                    responsive: {
                        rules: [{
                            condition: {
                                maxWidth: 700
                            },
                            chartOptions: {
                                legend: {
                                    layout: 'vertical',
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                }
                            }
                        }]
                    }

                });

            } else{
                $("#grafica").html('<i class="fa fa-exclamation-triangle"></i> Error al cargar').css("color","red");
            }
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#grafica").html('<i class="fa fa-exclamation-triangle"></i> Error en el servidor, recarga la página e intenta nuevamente').css("color","red");
        }
    });
}