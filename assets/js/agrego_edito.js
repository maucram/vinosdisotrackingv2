var g_agrego_una_vez=true;
var g_duplicado=false;


$("#form-agregar").validate({
    submitHandler: function (data) {

        var tabla=$(".form_agregar").data("tabla");
        var datos=$(data).serializeArray();


        var metodo;
        var llave_agregar=false;

        $("#status-general").html('');


        g_duplicado=false;


        switch (tabla) {
            case 'Usuarios':
                metodo= 'usuarios';
                var usuario_in=mau.texto.filtro_palabra($("#usuarios_usuario").val());

                if (usuario_in!='' && g_agrego_una_vez){

                    llave_agregar=true;

                    datos[8]["value"]=mau.numero.formato_numero(datos[8]["value"]);

                }

                break;

            case 'Cadena':
                metodo= 'cadena';
                var empresa_in=mau.texto.filtro_palabra($("#empresa").val());

                if (empresa_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var empresa_table;

                    tabladatos.rows().every(function (i) {

                        empresa_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());

                        if(empresa_table==empresa_in){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'Sucursal':
                metodo= 'sucursal';
                var sucursal_in=mau.texto.filtro_palabra($("#sucursal").val()),
                    empresa_in=mau.texto.filtro_palabra($("#id_empresa option:selected").text());

                if (sucursal_in!='' && empresa_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var empresa_table,sucursal_table;

                    tabladatos.rows().every(function (i) {

                        empresa_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        sucursal_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());

                        if(empresa_table==empresa_in && sucursal_in ==sucursal_table){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'Cliente':

                metodo= 'cliente';
                var cliente_in=mau.texto.filtro_palabra($("#cliente").val()),
                    usuario_in=mau.texto.filtro_palabra($("select[name=id_usuario]:first option:selected").text());



                if (usuario_in!='' && cliente_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var usuario_table,cliente_table;

                    tabladatos.rows().every(function (i) {

                        usuario_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());

                        if(usuario_in==usuario_table && cliente_in==cliente_table){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'ClientePromotor':

                metodo= 'cliente-promotor';
                var usuario_cliente_in=mau.texto.filtro_palabra($("#id_usuario_cliente option:selected").text()),
                    cliente_in=mau.texto.filtro_palabra($("#id_cliente option:selected").text()),
                    id_rol_in=mau.texto.filtro_palabra($("#id_rol option:selected").text()),
                    promotor_in=mau.texto.filtro_palabra($("#id_promotor option:selected").text());



                if (promotor_in!='' && cliente_in!='' && usuario_cliente_in!='' && id_rol_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var promotor_table,cliente_table,usuario_cliente_table,id_rol_table;

                    tabladatos.rows().every(function (i) {
                        usuario_cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());
                        id_rol_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(3).text());
                        promotor_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(4).text());


                        if(promotor_in==promotor_table && cliente_in==cliente_table && usuario_cliente_in==usuario_cliente_table && id_rol_in==id_rol_table){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'Estatus':
                metodo= 'estatus';
                var estatus_in=mau.texto.filtro_palabra($("#estatus").val());

                if (estatus_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var estatus_table;

                    tabladatos.rows().every(function (i) {

                        estatus_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());

                        if(estatus_table==estatus_in){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'Producto':
                metodo= 'producto';
                var producto_in=mau.texto.filtro_palabra($("#producto").val()),
                    cliente_in=mau.texto.filtro_palabra($("#id_cliente option:selected").text()),
                    marca_in=mau.texto.filtro_palabra($("#id_marca option:selected").text());

                if (producto_in!='' && cliente_in!='' && marca_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var cliente_table,producto_table,marca_table;

                    tabladatos.rows().every(function (i) {

                        cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        marca_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());
                        producto_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(3).text());

                        if(cliente_in==cliente_table && producto_table==producto_in && marca_in==marca_table){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'Lugar':
                metodo= 'lugar';
                var lugar_in=mau.texto.filtro_palabra($("#lugar").val());

                if (lugar_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var lugar_table;

                    tabladatos.rows().every(function (i) {

                        lugar_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());

                        if(lugar_table==lugar_in){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'Posición':
                metodo= 'posicion';
                var posicion_in=mau.texto.filtro_palabra($("#posicion").val()),
                    lugar_in=mau.texto.filtro_palabra($("#id_lugar option:selected").text());

                if (lugar_in!='' && posicion_in!='' && g_agrego_una_vez){
                    llave_agregar=true;
                    var lugar_table,posicion_table;

                    tabladatos.rows().every(function (i) {

                        lugar_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        posicion_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());

                        if(lugar_in==lugar_table && posicion_table==posicion_in){
                            g_duplicado=true;
                            return false;
                        }
                    });

                }

                break;

            case 'Historial':
                metodo = 'home';

                if (g_agrego_una_vez){
                    llave_agregar=true;
                }

                break;
        }


        if(llave_agregar){
            $("#status-general").css("height","50px");
            if(!g_duplicado){

                g_agrego_una_vez=false;
                $.ajax({
                    type: 'post',
                    url: base_url +"configuracion/"+metodo+'/agregar',
                    data: datos,
                    dataType:"json",
                    //contentType: false,
                    beforeSend: function () {

                        $("#status-general").html('<div class="alert alert-info" role="alert" align="center">Guardando datos...</div>');
                    },
                    //cache: false,
                    //processData: false,
                    success: function (id_new_row) {

                        // location.href = base_url + controlador;


                        if(id_new_row.status==200) {


                            $("#status-general").html('<div class="alert alert-success" role="alert" align="center">Datos guardatos correctamente</div>');
                            $("#status-general").css("height","100%");

                            switch (tabla) {
                                case 'Usuarios':

                                        var fecha_alta=mau.fechas.formato($("#fecha_alta").val(),"dd/mm/yyyy");
                                    tabladatos.rows.add([{
                                        "id": id_new_row.id,
                                        "foto_perfil": id_new_row.foto,
                                        "usuario": $("#usuarios_usuario").val(),
                                        "correo": $("#usuarios_correo").val(),
                                        "pass": $("#usuarios_pass").val(),
                                        "telefono": $("#usuarios_telefono").val(),
                                        "direccion": $("#usuarios_direccion").val(),
                                        "tarjeta_dispersion": $("#usuarios_tarjeta_dispersion").val(),
                                        "fecha_alta": fecha_alta,
                                        "sueldo": datos[8]["value"],
                                        "privilegios": $("#usuarios_privilegio").val()

                                    }]).order([0, 'desc']).draw();



                                    break;

                                case 'Cadena':

                                    tabladatos.rows.add([{
                                        "id": id_new_row.id,
                                        "empresa": $("#empresa").val()
                                    }]).order([0, 'desc']).draw();

                                    break;

                                case 'Estatus':
                                    tabladatos.rows.add([{
                                        "id": id_new_row.id,
                                        "estatus": $("#estatus").val()
                                    }]).order([0, 'desc']).draw();

                                    break;

                                case 'Lugar':

                                    tabladatos.rows.add([{
                                        "id": id_new_row.id,
                                        "lugar": $("#lugar").val()
                                    }]).order([0, 'desc']).draw();

                                    break;
                                default:
                                    tabladatos.ajax.reload();
                            }


                                    var espera = setInterval(function () {
                                        clearInterval(espera);
                                        $('#Modal_agregar').modal('hide');
                                        $(".fade").removeClass("modal-backdrop");
                                        $("html,body").css("overflow", "scroll");

                                        $("#form-agregar")[0].reset();
                                        $("#usuarios_muestro_foto img").prop("src", "");
                                        $("#usuarios_muestro_foto").addClass("hidden");
                                        g_agrego_una_vez = true;
                                        $("#status-general").html('');
                                    }, 1000);

                        }else{

                            Swal.fire({
                                title: '¡Error!',
                                text: 'Se produjo un error al intentar insertar, recarga la página e intenta nuevamente.',
                                type: 'error',
                                cancelButtonText: "Cerrar",
                                confirmButtonColor:"#DB244B",
                                focusConfirm: false,
                                cancelButtonColor:"#9f909b"
                            });

                        }


                    },
                    error: function (response) {
                        console.log("error" + JSON.stringify(response));
                        $("#status-general").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar agregar, recarga e intenta nuevamente</div>');
                    }
                });
            }else {
                $("#status-general").css("height","50px");
                $("#status-general").html('<div class="alert alert-warning" role="alert" align="center">Campo(s) duplicado(s)</div>');
            }
        }


        return false;
    },
    highlight: function(element) {
        $(element).css('background', '#ffdddd');
    },

    // Called when the element is valid:
    unhighlight: function(element) {
        $(element).css('background', '#ddffce');
    }
});
//var tabladatos=$('.data_table_configuracion').DataTable();
var g_tiempo;

$("#id_usuario_cliente").change(function () {
     $.ajax({
        type: 'post',
        url: base_url +"configuracion/cargo_combo/cliente",
        data: {"id_usuario_cliente":$(this).val()},
        dataType:"json",
        beforeSend: function () {

            $("#id_cliente").html('<option value="">Cargando...</option>');
        },
        success: function (response) {

            if(response.status==200){
                var opciones='<option value="">Elige una opción...</option>';
                $.each(response.data,function (i,cliente) {
                    opciones+='<option value="'+cliente.id+'">'+cliente.cliente+'</option>';
                });
                $("#id_cliente").html(opciones);
            }else{
                $("#id_cliente").html('<option value="">No se encontraron datos</option>').css("backgroundColor","lightred");
            }
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $("#id_cliente").html('<option value="">Error al cargar</option>').css("backgroundColor","lightred");
        }
    });
});

$("#form-editar").validate({
    submitHandler: function (data) {

        var tabla=$(".form_agregar").data("tabla");
        var datos=$(data).serializeArray();
            var id;

        var metodo;
        var llave_editar=false;

        $("#status-general_e").html('');

        console.log(JSON.stringify(datos));

        g_duplicado=false;


        switch (tabla) {
            case 'Usuarios':


                metodo= 'usuarios';

                var usuario_in=mau.texto.filtro_palabra($("#usuarios_usuario_e").val());


                if (usuario_in!=''){


                    tabladatos.rows().every(function (i) {


                        id=$(this.node()).find("td").eq(0).text();

                        if($("input[name=id]").val()==id){
                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }
                            if($("#usuarios_foto_e").val()!='')
                            $(this.node()).find("td").eq(1).find("img").prop("src",$("#usuarios_foto_e").val());

                            var fecha_alta=mau.fechas.formato($("#fecha_alta_e").val(),"dd/mm/yyyy");

                            $(this.node()).find("td").eq(2).html($("#usuarios_usuario_e").val());
                            $(this.node()).find("td").eq(3).html($("#usuarios_correo_e").val());
                            $(this.node()).find("td").eq(4).html($("#usuarios_pass_e").val());
                            $(this.node()).find("td").eq(5).html($("#usuarios_telefono_e").val());
                            $(this.node()).find("td").eq(6).html($("#usuarios_direccion_e").val());
                            $(this.node()).find("td").eq(7).html($("#usuarios_tarjeta_dispersion_e").val());
                            $(this.node()).find("td").eq(8).html(fecha_alta);
                            $(this.node()).find("td").eq(9).html($("#usuarios_sueldo_e").val());
                            $(this.node()).find("td").eq(10).html($("#usuarios_privilegio_e option:selected").text());


                           /* $(this.node()).find("td").eq(11).find("i").data("usuario",$("#usuarios_usuario_e").val()).data("pass",$("#usuarios_pass_e").val()).data("id",$("#usuarios_id").val())
                                .data("correo",$("#usuarios_correo_e").val()).data("telefono",$("#usuarios_telefono_e").val()).data("direccion",$("#usuarios_direccion_e").val())
                                .data("foto",$("#usuarios_foto_e").val()).data("tarjeta_dispersion",$("#usuarios_tarjeta_dispersion_e").val()).data("sueldo",datos[7]["value"]);*/

                            $(this.node()).find("td").eq(11).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"' +
                            'data-id="'+id+'" data-foto="'+$("#usuarios_foto_e").val()+'"  data-fecha_alta="'+$("#fecha_alta_e").val()+'" data-usuario="'+$("#usuarios_usuario_e").val()+'"' +
                            'data-pass="'+$("#usuarios_pass_e").val()+'" data-correo="'+$("#usuarios_correo_e").val()+'" data-telefono="'+$("#usuarios_telefono_e").val()+'" data-direccion="'+$("#usuarios_direccion_e").val()+'"' +
                            ' data-tarjeta_dispersion="'+$("#usuarios_tarjeta_dispersion_e").val()+'" ' +
                            ' data-id_privilegio="'+$("#usuarios_privilegio_e").val()+'" data-sueldo="'+datos[8]["value"]+'"></i>');

                            return false;
                        }
                    });
                    datos[8]["value"]=mau.numero.formato_numero(datos[8]["value"]);
                    llave_editar=true;

                }

                break;

            case 'Cadena':
                metodo= 'cadena';
                var empresa_in=mau.texto.filtro_palabra($("#empresa_e").val());

                if (empresa_in!=''){
                    var empresa_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();
                        empresa_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#empresa_e").val());

                            $(this.node()).find("td").eq(2).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                                'data-empresa="'+$("#empresa_e").val()+'" data-id="'+id+'"></i>');

                        }else if(empresa_table==empresa_in){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;


            case 'Sucursal':
                metodo= 'sucursal';
                var sucursal_in=mau.texto.filtro_palabra($("#sucursal_e").val()),
                    empresa_in=mau.texto.filtro_palabra($("#id_empresa_e option:selected").text());

                if (empresa_in!='' && sucursal_in!=''){
                    var empresa_table,sucursal_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();
                        empresa_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        sucursal_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#id_empresa_e option:selected").text());
                            $(this.node()).find("td").eq(2).html($("#sucursal_e").val());
                            $(this.node()).find("td").eq(3).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                            'data-id_empresa="'+$("#id_empresa_e").val()+'" data-sucursal="'+$("#sucursal_e").val()+'" data-id="'+id+'"></i>');

                        }else if(empresa_table==empresa_in && sucursal_in==sucursal_table){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;

            case 'Cliente':
                metodo= 'cliente';
                var cliente_in=mau.texto.filtro_palabra($("#cliente_e").val()),
                    usuario_in=mau.texto.filtro_palabra($("#id_usuario_e option:selected").text());

                if (usuario_in!='' && cliente_in!=''){
                    var usuario_table,cliente_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();
                        usuario_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#id_usuario_e option:selected").text());
                            $(this.node()).find("td").eq(2).html($("#cliente_e").val());
                            $(this.node()).find("td").eq(3).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                                'data-id_usuario="'+$("#id_usuario_e").val()+'" data-cliente="'+$("#cliente_e").val()+'" data-id="'+id+'"></i>');

                        }else   if(usuario_in==usuario_table && cliente_in==cliente_table){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;

            case 'ClientePromotor':
                metodo= 'cliente-promotor';
                var usuario_cliente_in=mau.texto.filtro_palabra($("#id_usuario_cliente_e option:selected").text()),
                cliente_in=mau.texto.filtro_palabra($("#id_cliente_e option:selected").text()),
                    id_rol_in=mau.texto.filtro_palabra($("#id_rol_e option:selected").text()),
                    promotor_in=mau.texto.filtro_palabra($("#id_promotor_e option:selected").text());

                if (promotor_in!='' && cliente_in!='' && usuario_cliente_in!=''){
                    var promotor_table,cliente_table,usuario_cliente_table,id_rol_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();

                        usuario_cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());
                        id_rol_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(3).text());
                        promotor_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(4).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#id_usuario_cliente_e option:selected").text());
                            $(this.node()).find("td").eq(2).html($("#id_cliente_e option:selected").text());
                            $(this.node()).find("td").eq(3).html($("#id_rol_e option:selected").text());
                            $(this.node()).find("td").eq(4).html($("#id_promotor_e option:selected").text());
                            $(this.node()).find("td").eq(5).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar" data-id_rol="'+$("#id_rol_e").val()+'"'+
                                'data-id_cliente="'+$("#id_cliente_e").val()+'" data-id_usuario_cliente="'+$("#id_usuario_cliente_e").val()+'" data-id_promotor="'+$("#id_promotor_e").val()+'" data-id="'+id+'"></i>');

                        }else   if(promotor_in==promotor_table && cliente_in==cliente_table && usuario_cliente_in==usuario_cliente_table){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;

            case 'Estatus':
                metodo='estatus';
                var estatus_in=mau.texto.filtro_palabra($("#estatus_e").val());

                if (estatus_in!=''){
                    var estatus_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();
                        estatus_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#estatus_e").val());
                            $(this.node()).find("td").eq(2).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                                'data-estatus="'+$("#estatus_e").val()+'" data-id="'+id+'"></i>');

                        }else if(estatus_table==estatus_in){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;

            case 'Producto':
                metodo= 'producto';
                var producto_in=mau.texto.filtro_palabra($("#producto_e").val()),
                    marca_in=mau.texto.filtro_palabra($("#id_marca_e option:selected").text()),
                    cliente_in=mau.texto.filtro_palabra($("#id_cliente_e option:selected").text());

                if (cliente_in!='' && producto_in!='' && marca_in!=''){
                    var producto_table,cliente_table,marca_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();
                        cliente_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        marca_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());
                        producto_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(3).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#id_cliente_e option:selected").text());
                            $(this.node()).find("td").eq(2).html($("#id_marca_e option:selected").text());
                            $(this.node()).find("td").eq(3).html($("#producto_e").val());
                            $(this.node()).find("td").eq(4).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                                'data-id_cliente="'+$("#id_cliente_e").val()+'" data-id_marca="'+$("#id_marca_e").val()+'" data-producto="'+$("#producto_e").val()+'" data-id="'+id+'"></i>');

                        }else if(cliente_in==cliente_table && producto_in==producto_table){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;

            case 'Lugar':
                metodo= 'lugar';
                var lugar_in=mau.texto.filtro_palabra($("#lugar_e").val());

                if (lugar_in!=''){
                    var lugar_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();
                        lugar_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#lugar_e").val());

                            $(this.node()).find("td").eq(2).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                                'data-empresa="'+$("#lugar_e").val()+'" data-id="'+id+'"></i>');

                        }else if(lugar_table==lugar_in){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;

            case 'Posición':
                metodo= 'posicion';
                var posicion_in=mau.texto.filtro_palabra($("#posicion_e").val()),
                    lugar_in=mau.texto.filtro_palabra($("#id_lugar_e option:selected").text());

                if (lugar_in!='' && posicion_in!=''){
                    var lugar_table,posicion_table;
                    llave_editar=true;
                    tabladatos.rows().every(function (i) {

                        id=$(this.node()).find("td").eq(0).text();
                        lugar_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(1).text());
                        posicion_table=mau.texto.filtro_palabra($(this.node()).find("td").eq(2).text());

                        if($("input[name=id]").val()==id){

                            if($("input[name=status]:checked").val()==1){
                                $(this.node()).removeClass('tickets-eliminado');
                            }else{
                                $(this.node()).addClass('tickets-eliminado');
                            }

                            $(this.node()).find("td").eq(1).html($("#id_lugar_e option:selected").text());
                            $(this.node()).find("td").eq(2).html($("#posicion_e").val());
                            $(this.node()).find("td").eq(3).html('<i class="fa fa-pencil editar" data-toggle="modal" data-target="#Modal_editar"'+
                                'data-id_cliente="'+$("#id_lugar_e").val()+'" data-producto="'+$("#posicion_e").val()+'" data-id="'+id+'"></i>');

                        }else if(lugar_in==lugar_table && posicion_in==posicion_table){
                            g_duplicado=true;
                            return false;
                        }


                    });

                }

                break;

            case 'Historial':
                metodo = 'home';

                if (g_agrego_una_vez){
                    llave_editar=true;
                }

                break;
        }

        if(llave_editar){
            $("#status-general_e").css("height","50px");
            if(!g_duplicado){
                console.log(JSON.stringify(datos));
                $.ajax({
                    type: 'post',
                    url: base_url +"configuracion/"+metodo+'/editar',
                    data: datos,
                    beforeSend: function () {

                        $("#status-general_e").html('<div class="alert alert-info" role="alert" align="center">Editando datos...</div>');
                    },
                    success: function (response) {
                        $("#status-general_e").html('<div class="alert alert-success" role="alert" align="center">Datos editados correctamente</div>');
                        $("#status-general_e").css("height","100%");


                        var espera=setInterval(function () {

                            clearInterval(espera);
                            $('#Modal_editar').modal('hide');
                            $(".fade").removeClass("modal-backdrop");
                            $("html,body").css("overflow","scroll");
                            $("#status-general_e").html('');

                            $("#form-editar")[0].reset();
                            $("#usuarios_muestro_foto_e img").prop("src", "");
                            $("#usuarios_foto_e").val("");
                            $("#usuarios_muestro_foto_e").addClass("hidden");
                        },800);


                        // location.href = base_url + controlador;
                    },
                    error: function (response) {
                        console.log("error" + JSON.stringify(response));
                        $("#status-general_e").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar editar, recarga e intenta nuevamente</div>');
                    }
                });
            }else {

                $("#status-general_e").html('<div class="alert alert-warning" role="alert" align="center">Campo(s) duplicado(s)</div>');
            }
        }


        return false;
    },
    highlight: function(element) {
        $(element).css('background', '#ffdddd');
    },

    // Called when the element is valid:
    unhighlight: function(element) {
        $(element).css('background', '#ddffce');
    }
});

var g_ajax_usuario;
$("#id_rol,#id_rol_e").change(function () {
    var id_rol=$(this).val();

    var promotor='#id_promotor';
    if($(this).data("edito")){
        promotor+='_e';
    }

    g_ajax_usuario= $.ajax({
        type: 'post',
        url: base_url +"configuracion/cliente-promotor/obtengo_usuarios_rol_seleccionado",
        data: {"id_rol":id_rol},
        dataType:"json",
        beforeSend: function () {

            $(promotor).html('<option value="">Cargando...</option>');
        },
        success: function (response) {

            if(response.status==200){
                var opciones='<option value="">Elige una opción...</option>';
                $.each(response.data,function (i,usuario) {
                    opciones+='<option value="'+usuario.id+'">'+usuario.usuario+'</option>';
                });
                $(promotor).html(opciones);
            }else{
                $(promotor).html('<option value="">No se encontraron datos</option>').css("backgroundColor","lightred");
            }
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $(promotor).html('<option value="">Error al cargar</option>').css("backgroundColor","lightred");
        }
    });

});

var g_ajax_marca;
$("#id_cliente,#id_cliente_e").change(function () {
    var id_cliente=$(this).val();

    var promotor='#id_marca';
    if($(this).data("edito")){
        promotor+='_e';
    }

    g_ajax_marca= $.ajax({
        type: 'post',
        url: base_url +"configuracion/producto/obtengo_marcas",
        data: {"id_cliente":id_cliente},
        dataType:"json",
        beforeSend: function () {

            $(promotor).html('<option value="">Cargando...</option>');
        },
        success: function (response) {

            if(response.status==200){
                var opciones='<option value="">Elige una opción...</option>';
                $.each(response.data,function (i,usuario) {
                    opciones+='<option value="'+usuario.id+'">'+usuario.marca+'</option>';
                });
                $(promotor).html(opciones);
            }else{
                $(promotor).html('<option value="">No se encontraron datos</option>').css("backgroundColor","lightred");
            }
        },
        error: function (response) {
            console.log("error" + JSON.stringify(response));
            $(promotor).html('<option value="">Error al cargar</option>').css("backgroundColor","lightred");
        }
    });

});

   $('.data_table_configuracion').on('click', '.cuerpo_tabla_configuracion td:last-child .editar', function (e) {

        var tabla = $(".form_agregar").data("tabla");
        var id = $(this).data('id');

        switch (tabla) {
            case 'Usuarios':
                var
                    foto = $(this).data("foto"),
                    usuario = $(this).data('usuario'),
                    correo = $(this).data("correo"),
                    pass = $(this).data('pass'),
                    telefono = $(this).data("telefono"),
                    fecha_alta=$(this).data("fecha_alta"),
                    direccion = $(this).data("direccion"),
                    tarjeta_dispersion = $(this).data("tarjeta_dispersion"),
                    sueldo = mau.numero.formato_moneda($(this).data("sueldo")),
                    id_privilegio = $(this).data('id_privilegio');


                $("#usuarios_muestro_foto_e").removeClass("hidden");


                $("#usuarios_muestro_foto_e img").prop("src", foto);
                $("#usuarios_usuario_e").val(usuario);
                $("#usuarios_correo_e").val(correo);
                $("#usuarios_pass_e").val(pass);
                $("#usuarios_telefono_e").val(telefono);
                $("#usuarios_direccion_e").val(direccion);
                $("#usuarios_tarjeta_dispersion_e").val(tarjeta_dispersion);
                $("#fecha_alta_e").val(fecha_alta);
                $("#usuarios_sueldo_e").val(sueldo);
                $("#usuarios_privilegio_e").val(id_privilegio);

                break;


            case 'Cadena':

                var empresa = $(this).data('empresa');
                $("#empresa_e").val(empresa);
                break;

            case 'Sucursal':

                var id_empresa =$(this).data('id_empresa'),
                    sucursal = $(this).data('sucursal');
                $("#id_empresa_e").val(id_empresa);
                //$("#id_empresa_e option:contains("+empresa+")").prop("selected",true);
                $("#sucursal_e").val(sucursal);
                break;

            case 'Cliente':

                var id_usuario =$(this).data('id_usuario'),
                    cliente = $(this).data('cliente');
                $("#id_usuario_e").val(id_usuario);
                //$("#id_empresa_e option:contains("+empresa+")").prop("selected",true);
                $("#cliente_e").val(cliente);
                break;

            case 'ClientePromotor':
                var id_promotor =$(this).data('id_promotor'),
                    id_cliente = $(this).data('id_cliente'),
                    id_rol = $(this).data('id_rol'),
                    id_usuario_cliente = $(this).data('id_usuario_cliente');

               $.ajax({
                    type: 'post',
                    url: base_url +"configuracion/cargo_combo/cliente",
                    data: {"id_usuario_cliente":id_usuario_cliente},
                    dataType:"json",
                    beforeSend: function () {

                        $("#id_cliente_e").html('<option value="">Cargando...</option>');
                    },
                    success: function (response) {

                        if(response.status==200){
                            var opciones='<option value="">Elige una opción...</option>';
                            $.each(response.data,function (i,cliente) {
                                opciones+='<option value="'+cliente.id+'">'+cliente.cliente+'</option>';
                            });
                            $("#id_cliente_e").html(opciones);
                            $("#id_cliente_e").val(id_cliente);
                        }else{
                            $("#id_cliente_e").html('<option value="">No se encontraron datos</option>').css("backgroundColor","lightred");
                        }
                    },
                    error: function (response) {
                        console.log("error" + JSON.stringify(response));
                        $("#id_cliente_e").html('<option value="">Error al cargar</option>').css("backgroundColor","lightred");
                    }
                });

                $("#id_rol_e").val(id_rol);
                $("#id_rol_e").change();
                $.when(g_ajax_usuario).done(function () {

                    $("#id_promotor_e").val(id_promotor);
                });

                $("#id_usuario_cliente_e").val(id_usuario_cliente);
                //$("#id_empresa_e option:contains("+empresa+")").prop("selected",true);

                break;

            case 'Estatus':

                var estatus = $(this).data('estatus');
                $("#estatus_e").val(estatus);
                break;

            case 'Producto':

                var id_cliente =$(this).data('id_cliente'),
                    id_marca =$(this).data('id_marca'),
                    producto = $(this).data('producto');
                $("#id_cliente_e").val(id_cliente);
                $("#id_cliente_e").change();
                $.when(g_ajax_marca).done(function () {

                    $("#id_marca_e").val(id_marca);
                });
                $("#producto_e").val(producto);
                break;

            case 'Lugar':

                var lugar = $(this).data('lugar');
                $("#lugar_e").val(lugar);
                break;

            case 'Posición':

                var id_lugar =$(this).data('id_lugar'),
                    posicion = $(this).data('posicion');
                $("#id_lugar_e").val(id_lugar);
                //$("#id_empresa_e option:contains("+empresa+")").prop("selected",true);
                $("#posicion_e").val(posicion);
                break;
        }

        $("input[name=id]").val(id);
        $("#status-general_e").html("");

    });



// eliminar *********************************************************************************************************************************************************************************



$("#boton_eliminar").click(function () {

    Swal.fire({
        title: '¿Estás seguro?',
        text: "Se eliminarán todos los datos del ID "+$("input[name=id]").val(),
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DB244B',
        confirmButtonText: 'Aceptar',
        showCancelButtonText: "cancelar",
    }).then((result) => {

        if (result.value) {


            var tabla = $(".form_agregar").data("tabla"), id = $("input[name=id]").val(), metodo;
            switch (tabla) {
                case 'Usuarios':
                    metodo = 'usuarios';
                    break;

                case 'Cadena':
                    metodo = 'cadena';
                    break;

                case 'Sucursal':
                    metodo = 'sucursal';
                    break;
                case 'Cliente':
                    metodo = 'cliente';
                    break;
                case 'ClientePromotor':
                    metodo = 'cliente-promotor';
                    break;
                case 'Estatus':
                    metodo = 'estatus';
                    break;
                case 'Producto':
                    metodo = 'producto';
                    break;
                case 'Lugar':
                    metodo = 'lugar';
                    break;
                case 'Posición':
                    metodo = 'posicion';
                    break;

            }
            $.ajax({
                type: 'post',
                url: base_url + 'configuracion/' + metodo + '/eliminar',

                data: {id: id, tabla: metodo},

                beforeSend: function () {
                    $("#status-general_e").css("height", "50px");
                    $("#status-general_e").html('<div class="alert alert-info" role="alert" align="center">Eliminando datos...</div>');
                },
                success: function (response) {
                    g_row.remove().draw();
                    $("#status-general_e").html('<div class="alert alert-success" role="alert" align="center">Datos eliminados correctamente</div>');
                    var espera = setInterval(function () {
                        clearInterval(espera);
                        $('#Modal_editar').modal('hide');
                        $(".fade").removeClass("modal-backdrop");
                        $("html,body").css("overflow", "scroll");
                        $("#status-general_e").html('');
                        $("#status-general_e").css("height", "100%");
                    }, 700);

                    // location.href = base_url + controlador;
                },
                error: function (response) {
                    console.log("error" + JSON.stringify(response));
                    $("#status-general_e").html('<div class="alert alert-danger" role="alert" align="center">Error al intentar eliminar</div>');
                }
            });
        }
    });


});




    var g_row;
    // editar tablas //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    $('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion tr', function (e) {

        g_row = tabladatos.row($(this));

        if($(this).hasClass("tickets-eliminado")){
            $("input[name=status][value='0']").prop("checked",true);
        }else
            $("input[name=status][value='1']").prop("checked",true);
        $("#status-general_e").html('');
        /*
        if($(this).hasClass("child")){
            if($(this).prev().hasClass("tickets-eliminado")){
                $("input[name=status][value='0']").prop("checked",true);
            }else
                $("input[name=status][value='1']").prop("checked",true);

        }
        */
    });
    /*********************************************************** usuarios ******************************************************************/


    $("#usuarios_foto_input").change(function () {

        var selector=new Object();

        selector.id_input_file='#usuarios_foto_input';
        selector.id_imagen='#usuarios_foto';
        selector.id_mostrar_imagen='#usuarios_muestro_foto img';
        selector.remover_clase="hidden";
        var file =document.getElementById("usuarios_foto_input").files[0];

        mau.input_file.base64(file,selector);

    });



    $("#usuarios_sueldo").on("change blur mouseout",function () {
        $(this).val(mau.numero.formato_moneda($(this).val()));
    });



$("#usuarios_foto_input_e").change(function () {

    var selector=new Object();

    selector.id_input_file='#usuarios_foto_input_e';
    selector.id_imagen='#usuarios_foto_e';
    selector.id_mostrar_imagen='#usuarios_muestro_foto_e img';
    selector.remover_clase="hidden";
    var file =document.getElementById("usuarios_foto_input_e").files[0];

    mau.input_file.base64(file,selector);

});

$("#usuarios_sueldo_e").on("change blur mouseout",function () {
    $(this).val(mau.numero.formato_moneda($(this).val()));
});



    $("#usuarios_usuario,#usuarios_usuario_e").keyup(function () {

        var status_general='#status-general';
        var boton_guardar='#boton_guardar';
        if($(this).data("edito")){
            status_general+='_e';
            boton_guardar+='_e';
        }else{

        }
        var usuario=$(this).val();
        if(usuario!='') {
            $(status_general).css("height","50px");
            $.ajax({
                type: 'post',
                url: base_url + 'configuracion/verificar_usuario',
                data: {usuario: usuario},
                beforeSend: function () {
                    $(status_general).html('<div class="alert alert-info" role="alert" align="center">Verificando usuario...</div>');
                },
                success: function (response) {
                    if (response) {
                        $(status_general).html('<div class="alert alert-warning" role="alert" align="center">El usuario ya existe, intenta con otro</div>');
                        $(boton_guardar).hide();
                    } else {
                        $(status_general).html('');
                        $(status_general).css("height","100%");
                        $(boton_guardar).show();
                    }
                },
                error: function (response) {
                    console.log("error" + JSON.stringify(response));
                    $(status_general).html('<div class="alert alert-danger" role="alert" align="center">Error al comprobar usuario</div>');
                    $(boton_guardar).hide();
                }
            });
        }
    });



    /*********************************************************** estatus ******************************************************************/


    $('.data_table_configuracion').on( 'click', '.cuerpo_tabla_configuracion td:last-child .editar_estatus', function (e) {

        $("#status-general_e").html("");

        var id = $(this).data('id'),
            estatus= $(this).data('estatus');

        $("input[name=id]").val(id);
        $("input[name=estatus]:last").val(estatus);

    });





    // ***************************************************** descargar excel tickets

    $(".descargar_excel_tickets").click(function () {
        var data_table=$(".data_table_configuracion .cuerpo_tabla_configuracion tr");
        var ids='';

        $.each(data_table,function (i) {
            ids+='/'+$(this).children().eq(0).text();
        });

        if(ids!='/No se encontraron resultados'){

            window.open(base_url + 'home/descargar_excel_tickets'+ids,"_parent");
        }
    });




function filtro_palabra(palabra) {
    return palabra
        .normalize('NFD')
        .replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi,"$1")
        .normalize().toLowerCase().trim();
}

