//$('.data_table_historial').DataTable();

//$(document).ready(function() {

var g_metodo=$(".breadcrumb li:last").text().toLowerCase();


var map_historial;
var geocoder;
var infowindow;
var puerta=true;
var puerta2=true;

var columnas=(g_metodo=="promotor" || g_metodo=="vendedor")?
    [{"data": "id"},
        {"data":"promotor"},
        {
            "data": "fecha",
            "render": function (data, type, full, meta) {
                var fe_div=data.split(/\/|\s|:/);
                var fecha_yyyymmdd=fe_div[2]+fe_div[1]+fe_div[0]+fe_div[3]+fe_div[4]+fe_div[5];

                return '<i class="hidden">'+fecha_yyyymmdd+'</i>'+data;
            }
        },
        {"data": "latlng",
            "render":function (data, type, row) {

                if(puerta) {

                    return data;

                    puerta=false;
                }

            }
        },
        {"data": function (data,index) {

                if (puerta2) {
                    // var es=data.fecha.split(/ |\/|:/gi);
                    //  var fecha_concat=es[2]+es[1]+es[0]+es[3]+es[4]+es[5];
                    var ruta_foto = base_url + 'carga_fotos/' + data.id_usuario + '/'+ data.foto;

                    var foto = (data.foto != '') ? '<a target="_blank" href="' + ruta_foto + '"><i class="fa fa-eye fa-2x"></i></a>' : '';
                    return foto;
                    puerta2=false;
                }
            }
        },
        {"data":"estatus"},
        {"data":"empresa"},
        {"data":"sucursal"},
        {"data":"cliente"},
        {"data":"marca"},
        {"data":"producto"},
        {"data":"lugar"},
        {"data":"posicion"},
        {"data":"cantidad"},
        {"data":"comentario"}

    ]:


    [{"data": "id"},
        {"data":"promotor_vendedor"},
        {
            "data": "fecha",
            "render": function (data, type, full, meta) {
                var fe_div=data.split(/\/|\s|:/);
                var fecha_yyyymmdd=fe_div[2]+fe_div[1]+fe_div[0]+fe_div[3]+fe_div[4]+fe_div[5];

                return '<i class="hidden">'+fecha_yyyymmdd+'</i>'+data;
            }
        },
        {"data": "latlng",
            "render":function (data, type, row) {

                if(puerta) {

                    return data;

                    puerta=false;
                }

            }
        },
        {"data": function (data,index) {

                if (puerta2) {
                    // var es=data.fecha.split(/ |\/|:/gi);
                    //  var fecha_concat=es[2]+es[1]+es[0]+es[3]+es[4]+es[5];
                    var ruta_foto = base_url + 'carga_fotos/' + data.id_usuario + '/'+ data.foto;

                    var foto = (data.foto != '') ? '<a target="_blank" href="' + ruta_foto + '"><i class="fa fa-eye fa-2x"></i></a>' : '';
                    return foto;
                    puerta2=false;
                }
            }
        },
        {"data":"estatus"},
        {"data":"empresa"},
        {"data":"sucursal"},
        {"data":"cliente"},
        {"data":"marca"},
        {"data":"comentario"}

    ]

        ;

var controlador='historial';

function initMap() {

    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: {lat: 19.44, lng:-99.14}
    });

    geocoder = new google.maps.Geocoder;
    infowindow = new google.maps.InfoWindow;
}


    $(".historial_buscar_ruta").click(function () {

    });
//});


var tabladatoshistorialtodos=  $('.data_table_historial_todos').DataTable({
    "oLanguage": {
        "sUrl": "//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"
    },
    "pageLength": 100,
    "scrollY": "60vh",
    "scrollX": '100%',
    "scrollCollapse": true,
    "order": [[ 0, "desc" ]],
    "ajax": base_url +'historial/'+g_metodo+'/cargo_datatable',
    "initComplete":function( settings, json){
       // ini_dir();

        busco_direcciones_sin_procesar();
       $("#map").hide();
        // call your function here
    },
    columns:columnas,
    dom: 'Bfrtip',
    buttons: [
        {
            extend: 'copyHtml5',
            exportOptions: { orthogonal: 'export' }
        },
        {
            extend: 'excelHtml5',
            filename: function(){
                var f = new Date();
                var dia=(f.getDate()<10)?'0'+f.getDate():f.getDate();
                var mes=((f.getMonth() +1)<10)?'0'+(f.getMonth() +1):(f.getMonth() +1);
                var year=f.getFullYear();
                var fecha =  dia+ "-" + mes + "-" +year;

                return 'VDPromotoresHistorial_' +fecha;
            },
            exportOptions: { orthogonal: 'export' }
        },
        {
            extend: 'pdfHtml5',
            filename: function(){
                var f = new Date();
                var dia=(f.getDate()<10)?'0'+f.getDate():f.getDate();
                var mes=((f.getMonth() +1)<10)?'0'+(f.getMonth() +1):(f.getMonth() +1);
                var year=f.getFullYear();
                var fecha =  dia+ "-" + mes + "-" +year;

                return 'VDPromotoresHistorial_' +fecha;
            },
            exportOptions: { orthogonal: 'export' }
        }
    ]

});
